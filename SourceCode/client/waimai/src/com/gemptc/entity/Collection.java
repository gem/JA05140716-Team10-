package com.gemptc.entity;



public class Collection {
	private int coid;
	private int rid;
	private String jpgurl;
	private String rname;
	private String info;
	private long telephone;
	private long uphone;
	private String rsite;
	private String sname;
	public int getCoid() {
		return coid;
	}
	public void setCoid(int coid) {
		this.coid = coid;
	}
	public String getJpgurl() {
		return jpgurl;
	}
	public void setJpgurl(String jpgurl) {
		this.jpgurl = jpgurl;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public long getTelephone() {
		return telephone;
	}
	public void setTelephone(long telephone) {
		this.telephone = telephone;
	}
	public long getUphone() {
		return uphone;
	}
	public void setUphone(long uphone) {
		this.uphone = uphone;
	}
	public Collection(int coid,int rid, String jpgurl, String rname, String info,
			long telephone, long uphone,String rsite,String sname) {
		super();
		this.coid = coid;
		this.rid=rid;
		this.jpgurl = jpgurl;
		this.rname = rname;
		this.info = info;
		this.telephone = telephone;
		this.uphone = uphone;
		this.rsite=rsite;
		this.setSname(sname);
	}
	public Collection(int rid,String jpgurl, String rname, String info, long telephone,
			long uphone,String rsite,String sname) {
		super();
		this.rid=rid;
		this.jpgurl = jpgurl;
		this.rname = rname;
		this.info = info;
		this.telephone = telephone;
		this.uphone = uphone;
		this.rsite=rsite;
		this.setSname(sname);
	}
	public Collection() {
		super();
	}
	public String getRsite() {
		return rsite;
	}
	public void setRsite(String rsite) {
		this.rsite = rsite;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
}
