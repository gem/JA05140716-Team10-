package com.gemptc.entity;

public class Food {
	private int fid;
	private int rid;
	private String ftype;
	private String fname;
	private double fprice;
	private int number;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getFtype() {
		return ftype;
	}

	public void setFtype(String ftype) {
		this.ftype = ftype;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public double getFprice() {
		return fprice;
	}

	public void setFprice(double fprice) {
		this.fprice = fprice;
	}

	public Food(int fid, int rid, String fname, String ftype, double fprice) {
		super();
		this.fid = fid;
		this.rid = rid;
		this.fname = fname;
		this.ftype = ftype;
		this.fprice = fprice;
		number = 0;
	}

	public Food(String fname, double fprice, int number) {
		super();
		this.fname = fname;
		this.fprice = fprice;
		this.number = number;
	}

	public Food() {
		super();
	}

}
