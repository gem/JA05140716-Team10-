package com.gemptc.entity;

public class ShowPwd {
	private String showresult;
	private String password;
	public String getShowresult() {
		return showresult;
	}
	public void setShowresult(String showresult) {
		this.showresult = showresult;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ShowPwd(String showresult, String password) {
		super();
		this.showresult = showresult;
		this.password = password;
	}
}
