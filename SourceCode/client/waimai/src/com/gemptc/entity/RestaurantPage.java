package com.gemptc.entity;

import java.util.List;



public class RestaurantPage {
	private List<Restaurant> data;
	private int currentPage;
	private int totalPage;
	public List<Restaurant> getData() {
		return data;
	}
	public void setData(List<Restaurant> data) {
		this.data = data;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public RestaurantPage(List<Restaurant> data, int currentPage, int totalPage) {
		super();
		this.data = data;
		this.currentPage = currentPage;
		this.totalPage = totalPage;
	}
	public RestaurantPage() {
		super();
	}
}
