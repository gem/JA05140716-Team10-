package com.gemptc.entity;

public class Advice {
	/*Aid int
	 * Aphone varchar(20) not null,
	     Ainfo varchar(200) not null*/
	private int Aid; 
	private String aphone;
	private String Ainfo;
	public int getAid() {
		return Aid;
	}
	public void setAid(int aid) {
		Aid = aid;
	}
	public String getAphone() {
		return aphone;
	}
	public void setAphone(String aphone) {
		this.aphone = aphone;
	}
	public String getAinfo() {
		return Ainfo;
	}
	public void setAinfo(String ainfo) {
		Ainfo = ainfo;
	}
	public Advice(int aid, String aphone, String ainfo) {
		super();
		Aid = aid;
		this.aphone = aphone;
		Ainfo = ainfo;
	}
	public Advice() {
		super();
	}
}
