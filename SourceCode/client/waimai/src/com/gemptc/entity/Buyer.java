package com.gemptc.entity;

public class Buyer {
	private int bid;
	private String bname;
	private String bpwd;
	public Buyer() {
		super();
	}
	public Buyer(int bid, String bname, String bpwd) {
		super();
		this.bid = bid;
		this.bname = bname;
		this.bpwd = bpwd;
	}
	public int getId() {
		return bid;
	}
	public void setId(int bid) {
		this.bid = bid;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getBpwd() {
		return bpwd;
	}
	public void setBpwd(String bpwd) {
		this.bpwd = bpwd;
	}
}
