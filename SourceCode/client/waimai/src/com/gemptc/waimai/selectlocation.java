package com.gemptc.waimai;

import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.gemptc.waimai.LocationApplication;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class selectlocation extends Activity implements OnClickListener {
	private ArrayAdapter<String> adapter;
	
	private LocationClient mLocationClient;// 用来发起定位,添加取消监听
	private TextView LocationResult;// ,ModeInfor
	private String locatinResultstr;
	private LocationMode tempMode = LocationMode.Hight_Accuracy;// 定位模式，？基站+gps+wifi：gps
	private String tempcoor = "gcj02";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去标题
		setContentView(R.layout.activity_location);
		ListView lvlocation = (ListView) findViewById(R.id.lv_location);
		Button btnreturn=(Button) findViewById(R.id.btn_back);
		btnreturn.setOnClickListener(this);
		Button btnreselest=(Button) findViewById(R.id.btn_locationauto);
		btnreselest.setOnClickListener(this);
		final String data[] = { "平江区", "工业园区", "沧浪区", "吴中区", "金阊区", "相城区",
				"虎丘区" };
		adapter = new ArrayAdapter<String>(this,
				R.layout.activity_listitem_location, R.id.tv_cityname, data);
		lvlocation.setAdapter(adapter);

		// 监听list条目
		lvlocation.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent();
				intent.putExtra("position", position);
				intent.setClass(selectlocation.this, selectstress0.class);
				startActivity(intent);
				
			}
		});
		
	}

	@Override
	public void onClick(View v) {
		int id=v.getId();
		mLocationClient = ((LocationApplication) getApplication()).mLocationClient;
		//***找控件一定到指定view去找，否则默认到当前acitvity——layout去找***
		LocationResult = (TextView) findViewById(R.id.textView1);//非findViewById(R.id.main_tab_weixin_textView1);
		((LocationApplication) getApplication()).mLocationResult = LocationResult;
		locatinResultstr=((LocationApplication)getApplication()).loactionResultstr;
		InitLocation();
		mLocationClient.start();
		if(id==R.id.btn_back){
			startActivity(new Intent(this,ShowRestaurantActivity.class));
		}
//		Toast.makeText(this, locatinResultstr, Toast.LENGTH_LONG).show();
	}

	private void InitLocation() {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(tempMode);// 设置定位模式
		option.setCoorType(tempcoor);// 返回的定位结果是百度经纬度，默认值gcj02，属性中已设置
		int span = 1000;
		// try {
		// span = Integer.valueOf(frequence.getText().toString());
		// } catch (Exception e) {
		// // TODO: handle exception
		// }
		option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms

		// option.setIsNeedAddress(checkGeoLocation.isChecked());
		option.setIsNeedAddress(true);// 设置需要地理信息
		mLocationClient.setLocOption(option);
	}
}
