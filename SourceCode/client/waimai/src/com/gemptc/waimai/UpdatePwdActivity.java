package com.gemptc.waimai;


import com.gemptc.waimai.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdatePwdActivity extends Activity implements OnClickListener {
private EditText etpad;
private EditText etrepad;
private Button button2;
private Button button1;
private SharedPreferences sp;
private String phone;

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_updatepwd);
	etpad=(EditText) findViewById(R.id.et_pad);
	etrepad=(EditText) findViewById(R.id.et_repad);
	button1=(Button) findViewById(R.id.btn_go);
	button2=(Button) findViewById(R.id.btn_getpadsubmit);
	button1.setOnClickListener(this);
	button2.setOnClickListener(this);
	sp=getSharedPreferences("login", MODE_PRIVATE);
	phone=sp.getString("phone", "");
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_getpadsubmit){
		String pad=etpad.getText().toString();
		String repad=etrepad.getText().toString();
		if(pad.equals("")||pad==null){
			Toast.makeText(this, "密码不能为空！", Toast.LENGTH_LONG).show();
			return;
		}if(repad.equals("")||repad==null){
			Toast.makeText(this, "确认密码不能为空！", Toast.LENGTH_LONG).show();
			return;
		}
		if(!repad.equals(pad)){
			Toast.makeText(this, "确认密码错误！", Toast.LENGTH_LONG).show();
			return ;
		}
		if(CommonUtils.isFastDoubleClick()){
			Toast.makeText(this, "密码已修改，请不要重复提交！",Toast.LENGTH_LONG).show();
	        return;  
		}else{
			 Utils.uploadPwdByHttpClientPost(this,phone,pad);
		}
		
		
	}else if(id==R.id.btn_go){
		Intent intent=new Intent(this,LoginActivity.class);
		startActivity(intent);
	}
	
}
}
