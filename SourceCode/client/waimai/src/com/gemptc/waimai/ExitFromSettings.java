package com.gemptc.waimai;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ExitFromSettings extends Activity implements OnClickListener {
private LinearLayout layout;
private SharedPreferences sp;


@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_exitsign);
	layout=(LinearLayout)findViewById(R.id.exit_layout2);
	layout.setOnClickListener(this);
	sp=getSharedPreferences("login",MODE_PRIVATE);
}

@Override
public void onClick(View v) {
	Toast.makeText(getApplicationContext(), "提示：点击窗口外部关闭窗口！", 
			Toast.LENGTH_SHORT).show();	
}
@Override
public boolean onTouchEvent(MotionEvent event){
	finish();
	return true;
}

public void exitbutton1(View v) {  //取消
	this.finish();    	
  }  
public void exitbutton0(View v) {  //退出登录
	this.finish();
	sp.edit().remove("phone").remove("password").remove("isCheck").commit();
	Intent intent=new Intent(this,LoginActivity.class);
	startActivity(intent);
  }  
}
