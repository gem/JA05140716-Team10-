package com.gemptc.waimai;


import com.gemptc.waimai.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ModifyNameActivity extends Activity implements OnClickListener {
private EditText etname;
private SharedPreferences sp;
private String phone;

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_shortname);
	etname=(EditText) findViewById(R.id.et_name);
	Button button1=(Button) findViewById(R.id.btn_register);
	Button button2=(Button) findViewById(R.id.btn_getpadsubmit);
	button1.setOnClickListener(this);
	button2.setOnClickListener(this);
	sp=getSharedPreferences("login", MODE_PRIVATE);
	phone=sp.getString("phone", "");
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_register){
		Intent intent=new Intent(this,PersonSettingActivity.class);
		startActivity(intent);
	}else if(id==R.id.btn_getpadsubmit){
		String name=etname.getText().toString();
		if(name.equals("")||name==null){
			Toast.makeText(this, "昵称不能为空！", Toast.LENGTH_LONG).show();
			return;
		}
		if(CommonUtils.isFastDoubleClick()){
			Toast.makeText(this, "昵称已修改，请不要重复提交！",Toast.LENGTH_LONG).show();
	        return;  
		}
		else{
			Utils.uploadNameByHttpClientPost(this,name,phone);
		}
		
	}
	
}
}
