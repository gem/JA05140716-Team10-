package com.gemptc.waimai;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GetPhotoActivity extends Activity implements OnClickListener {
	public static final int SELECT_PIC = 11;
	public static final int TAKE_PHOTO = 12;
	public static final int CROP_PHOTO = 13;
	private Uri imageUri;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_getphoto);
		Button button1 = (Button) findViewById(R.id.exitBtn1);
		Button button2 = (Button) findViewById(R.id.exitBtn2);
		Button button3 = (Button) findViewById(R.id.exitBtn3);
		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		button3.setOnClickListener(this);
		SharedPreferences sp=getSharedPreferences("login", MODE_PRIVATE);
		String phone=sp.getString("phone", "");
		File file = new File(Environment.getExternalStorageDirectory(),
				System.currentTimeMillis()+".jpg");
		imageUri = Uri.fromFile(file);
	
	
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.exitBtn1) {// 拍照上传
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
			startActivityForResult(intent, TAKE_PHOTO);
		} else if (id == R.id.exitBtn2) {// 本地图库中选择上传
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_PICK);
			intent.setType("image/*");
			// 裁剪
			intent.putExtra("crop", "true");
			// 宽高比例
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			// 定义宽和高
			intent.putExtra("outputX", 300);
			intent.putExtra("outputY", 300);
			// 图片是否缩放
			intent.putExtra("scale", true);
			// 是否要返回值
			intent.putExtra("return-data", false);
			// 把图片存放到imageUri
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
			// 图片输出格式
			intent.putExtra("outputFormat",
			Bitmap.CompressFormat.JPEG.toString());
			intent.putExtra("noFaceDetection", true);
			startActivityForResult(intent, SELECT_PIC);
		} else if (id == R.id.exitBtn3) {// 取消
			this.finish();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode != RESULT_OK) {
			return;
		}
		if (requestCode == SELECT_PIC) {
			Intent intent = new Intent();
			intent.putExtra("url", imageUri.toString());
			setResult(RESULT_OK, intent);
			finish();
		} else if (requestCode == GetPhotoActivity.TAKE_PHOTO) {
			Intent intent = new Intent();
			intent.putExtra("url", imageUri.toString());
			setResult(RESULT_OK, intent);
			finish();
		}

	}
}
