package com.gemptc.waimai;

import java.security.PublicKey;
import java.util.ArrayList;

import android.R.integer;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private ViewPager viewPager;
	private ArrayList<View> pageViews;
	private ImageView imageView;
	private ImageView[] imageViews;
	private int num = 300;
	// 包裹滑动图片LinearLayout
	private ViewGroup main;
	// 包裹小圆点的LinearLayout
	private ViewGroup group;
	private ImageView imgnear;
	private ImageView imgfeast;
	private ImageView imgorder;
	private ImageView imgme;
	private SharedPreferences sp;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_main);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);
		LayoutInflater inflater = getLayoutInflater();
		pageViews = new ArrayList<View>();
		pageViews.add(inflater.inflate(R.layout.view1, null));
		pageViews.add(inflater.inflate(R.layout.view2, null));
		pageViews.add(inflater.inflate(R.layout.view3, null));
		imageViews = new ImageView[pageViews.size()];
		main = (ViewGroup) inflater.inflate(R.layout.activity_main, null);
		group = (ViewGroup) main.findViewById(R.id.checkview);
		viewPager = (ViewPager) main.findViewById(R.id.guidePages);
		
//		LinearLayout ll1=(LinearLayout) findViewById(R.id.ll1);
//		LinearLayout ll2=(LinearLayout) findViewById(R.id.ll2);
//		LinearLayout ll3=(LinearLayout) findViewById(R.id.ll3);
//		LinearLayout ll4=(LinearLayout) findViewById(R.id.ll4);
//		ll1.setOnClickListener(this);
//		ll2.setOnClickListener(this);
//		ll3.setOnClickListener(this);
//		ll4.setOnClickListener(this);
		
//		
//		ll1.setOnClickListener(new  LinearLayout.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				startActivity(new Intent(MainActivity.this,ShowRestaurantActivity.class));	
//			}
//		});
		for (int i = 0; i < pageViews.size(); i++) {
			imageView = new ImageView(MainActivity.this);
			imageView.setLayoutParams(new LayoutParams(13, 13));
			imageView.setPadding(13, 0, 13, 0);
			imageViews[i] = imageView;

			if (i == 0) {
				// 默认选中第一张图片
				imageViews[i]
						.setBackgroundResource(R.drawable.page_indicator_focused);
			} else {
				imageViews[i].setBackgroundResource(R.drawable.page_indicator);
			}

			group.addView(imageView);
		}

		setContentView(main);

		viewPager.setAdapter(new GuidePageAdapter());
		viewPager.setOnPageChangeListener(new GuidePageChangeListener());
		viewPager.setCurrentItem(300);
		mHandler.postDelayed(mRunnable, 5000);
		imgnear= (ImageView) findViewById(R.id.img_near);
		imgfeast = (ImageView) findViewById(R.id.img_feast);
		imgorder=(ImageView) findViewById(R.id.img_order);
		imgme=(ImageView) findViewById(R.id.img_me);
		imgnear.setOnClickListener(this);
		imgfeast.setOnClickListener(this);
		imgorder.setOnClickListener(this);
		imgme.setOnClickListener(this);
		sp=getSharedPreferences("login", MODE_PRIVATE);
	}

	private Handler mHandler = new Handler();
	private Runnable mRunnable = new Runnable() {
		public void run() {
			mHandler.postDelayed(mRunnable, 1000 * 5);
			num++;
			viewHandler.sendEmptyMessage(num);
		}

	};
//
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		switch (keyCode) {
//		case KeyEvent.KEYCODE_BACK:
//			this.finish();
//			mHandler.removeCallbacks(mRunnable);
//		default:
//			break;
//		}
//
//		return super.onKeyDown(keyCode, event);
//	}

	private final Handler viewHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			viewPager.setCurrentItem(msg.what);
			super.handleMessage(msg);
		}

	};
	private long mExitTime;

	// 指引页面数据适配器
	class GuidePageAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return Integer.MAX_VALUE;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO Auto-generated method stub
			return super.getItemPosition(object);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			// TODO Auto-generated method stub
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			// TODO Auto-generated method stub
			try {
				((ViewPager) arg0).addView(
						pageViews.get(arg1 % pageViews.size()), 0);
			} catch (Exception e) {

			}
			return pageViews.get(arg1 % pageViews.size());
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public Parcelable saveState() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void finishUpdate(View arg0) {
			// TODO Auto-generated method stub

		}
	}

	// 指引页面更改事件监听器
	class GuidePageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int arg0) {
			num = arg0;
				arg0 = arg0 % pageViews.size();

			for (int i = 0; i < imageViews.length; i++) {
				imageViews[arg0]
						.setBackgroundResource(R.drawable.page_indicator_focused);

				if (arg0 != i) {
					imageViews[i]
							.setBackgroundResource(R.drawable.page_indicator);
				}
			}
		}

	}
	
	@Override
	public void onClick(View v) {
		int id=v.getId();
		if(id==R.id.img_near){
			startActivity(new Intent(this,ShowRestaurantActivity.class));
		}else if(id==R.id.img_order){
			startActivity(new Intent(this,orderlistviewpageActivity.class));
		}else if(id==R.id.img_feast){
			startActivity(new Intent(this,ShowTypeRestaurantActivity.class));
		}else if(id==R.id.img_me){
			String dname=sp.getString("phone", "");
			//boolean flag=sp.getBoolean("isCheck", false);
			if(dname!=null&&!dname.equals("")){
				startActivity(new Intent(this,PersonCenterActivity.class));
			}else{
				startActivity(new Intent(this,LoginActivity.class));
			}
		}
//		if(id==R.id.ll1){
//			startActivity(new Intent(this,ShowRestaurantActivity.class));
//		}else if(id==R.id.ll2){
//			startActivity(new Intent(this,orderlistviewpageActivity.class));
//		}else if(id==R.id.ll3){
//			startActivity(new Intent(this,ShowTypeRestaurantActivity.class));
//		}else if(id==R.id.ll4){
//			String dname=sp.getString("phone", "");
//			//boolean flag=sp.getBoolean("isCheck", false);
//			if(dname!=null&&!dname.equals("")){
//				startActivity(new Intent(this,PersonCenterActivity.class));
//			}else{
//				startActivity(new Intent(this,LoginActivity.class));
//			}
//		}
	}  
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
		if ((System.currentTimeMillis() - mExitTime) > 5000) {// 如果两次按键时间间隔大于5000毫秒，则不退出
		Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
		mExitTime = System.currentTimeMillis();// 更新mExitTime
		} else {
		System.exit(0);// 否则退出程序
		}
		return true;
		}
		return super.onKeyDown(keyCode, event);
		}

}