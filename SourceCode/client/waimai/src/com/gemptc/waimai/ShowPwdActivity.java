package com.gemptc.waimai;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ShowPwdActivity extends Activity implements OnClickListener {
private EditText etpad;

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_showpad);
	Button btnreturn=(Button) findViewById(R.id.btn_return);
	Button btngo=(Button) findViewById(R.id.btn_go);
	Button btnsub=(Button) findViewById(R.id.btn_submit);
	etpad=(EditText) findViewById(R.id.et_pad);
	Intent intent=getIntent();
	String pwd=intent.getStringExtra("pwd");
	etpad.setText(pwd);
	btnreturn.setOnClickListener(this);
	btngo.setOnClickListener(this);
	
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_return){
		Intent intent=new Intent(this,UpdatePwdActivity.class);
		startActivity(intent);
	}else if(id==R.id.btn_go){
		Intent intent=new Intent(this,LoginActivity.class);
		startActivity(intent);
	}
	
}
}
