package com.gemptc.waimai;

//import java.io.IOException;
//import java.io.InputStream;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//
//import com.gemptc.util.Utils;
//
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.os.IBinder;
//import android.widget.Toast;
//
//public class MessageService extends Service {
//	public static final String PATH15 = "http://10.203.1.48:8080/waimai/AndroidSendMessageToServiceServlet";
//    //点击查看
//    private Intent messageIntent = null;
//    private PendingIntent messagePendingIntent = null;
// 
//    //通知栏消息
//    private int messageNotificationID = 1000;
//    private Notification messageNotification = null;
//    private NotificationManager messageNotificatioManager = null;
//	@Override
//	public IBinder onBind(Intent intent) {
//		return null;
//	}
//	 @Override
//		public void onCreate() {
//		 super.onCreate();
//	    	 //初始化
//	        messageNotification = new Notification();
//	        messageNotification.icon = R.drawable.icon;
//	        messageNotification.tickerText = "新消息";
//     messageNotification.defaults = Notification.DEFAULT_SOUND;
//	        messageNotificatioManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//	        //点击跳转的activity
//	        messageIntent = new Intent(this, UpdateRestaurantActivity.class);
//	        messagePendingIntent = PendingIntent.getActivity(this,0,messageIntent,0);
//		}
//		/**
//	     * 从服务器端获取消息
//	     *
//  */
//	    
//		@Override
//			public void onDestroy() {
//			       System.exit(0);
//			            //或者，二选一，推荐使用System.exit(0)，这样进程退出的更干净
//			            //messageThread.isRunning = false;
//			            //super.onDestroy();
//			}
// /**
// * @return 返回服务器要推送的消息，否则如果为空的话，不推送
//  */
//		//创建一个新的线程
//		class MyTask extends AsyncTask<Void, Void,String>{
//			@Override
//			protected String doInBackground(Void... params) {
//				HttpClient client=new DefaultHttpClient();
//				HttpPost post=new HttpPost(PATH15);
//				try {
//					HttpResponse response=client.execute(post);
//					if(response.getStatusLine().getStatusCode()==200){		
//						InputStream is=response.getEntity().getContent();
//						byte[] data=Utils.convertInputStream2ByteArray(is);
//						String back=new String(data);
//						return back;
//					}
//				} catch (ClientProtocolException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				return null;
//			}
//			@Override
//			protected void onPostExecute(String result) {
//			super.onPostExecute(result);
//			System.out.println(result);
//			if(result!=null){
//			
//			                        //更新通知栏
//			                        messageNotification.setLatestEventInfo(MessageService.this,"新消息","有新的商家入驻，点击查看新餐厅哦^-^",messagePendingIntent);
//			                        messageNotificatioManager.notify(messageNotificationID, messageNotification);
//		                     //每次通知完，通知ID递增一下，避免消息覆盖掉
//		                    messageNotificationID++;
//			    }
//			}
//			}
//		}
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class MessageService extends Service {	 
	    //获取消息线程
	    private MessageThread messageThread = null;
	 
	    //点击查看
	    private Intent messageIntent = null;
	    private PendingIntent messagePendingIntent = null;
	 
	    //通知栏消息
	    private int messageNotificationID = 1000;
	    private Notification messageNotification = null;
	    private NotificationManager messageNotificatioManager = null;
	 
	    public IBinder onBind(Intent intent) {
	        return null;
	    }
	     
	    @Override
		public void onCreate() {
	    	super.onCreate();
	    	//初始化
	        messageNotification = new Notification();
	        messageNotification.icon = R.drawable.icon;
	        messageNotification.tickerText = "新消息";
        messageNotification.defaults = Notification.DEFAULT_SOUND;
	        messageNotificatioManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
	        //点击跳转的activity
	        messageIntent = new Intent(this, UpdateRestaurantActivity.class);
	        messagePendingIntent = PendingIntent.getActivity(this,0,messageIntent,0);
	     
	        //开启线程
	        messageThread = new MessageThread();
	        messageThread.isRunning = true;
	        messageThread.start();
	// Toast.makeText(MessageService.this, "aaaa", Toast.LENGTH_LONG).show();
			
		}

		/**
	     * 从服务器端获取消息
	     *
     */
	    class MessageThread extends Thread{
	        //运行状态，下一步骤有大用
	        public boolean isRunning = true;
	        public void run() {
	            while(isRunning){
	                try {
	                    //休息10分钟
	                    Thread.sleep(5000);
	                    //获取服务器消息
	                    String serverMessage = getServerMessage();
	                 
	                    if(serverMessage!=null&&!"".equals(serverMessage)){
	                        //更新通知栏
	                        messageNotification.setLatestEventInfo(MessageService.this,"新消息","您中奖了，1个亿!"+serverMessage,messagePendingIntent);
	                        messageNotificatioManager.notify(messageNotificationID, messageNotification);
                        //每次通知完，通知ID递增一下，避免消息覆盖掉
                      //messageNotificationID++;
                    }
               } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
	    }
		@Override
			public void onDestroy() {
			  super.onDestroy();
			           System.exit(0);
			            //或者，二选一，推荐使用System.exit(0)，这样进程退出的更干净
			           //messageThread.isRunning = false;
			          
			}
    /**
     * 这里以此方法为服务器Demo，仅作示例
    * @return 返回服务器要推送的消息，否则如果为空的话，不推送
     */
    public String getServerMessage(){
        return "YES!";
    }
}
