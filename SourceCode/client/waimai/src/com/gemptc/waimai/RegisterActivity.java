package com.gemptc.waimai;

import java.net.URLEncoder;

import com.gemptc.entity.UserPeople;
import com.gemptc.waimai.util.Utils;

import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RegisterActivity extends Activity implements OnClickListener {
private String progrem;
private int pdid;
private Button button1;
private Button button2;
private EditText etphone;
private EditText etpad;
private EditText etname;
private EditText etrepad;
private EditText etanswer;
private Spinner spspinner;
private ArrayAdapter adapter;

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_register);
	button1=(Button) findViewById(R.id.btn_register);
	button2=(Button) findViewById(R.id.btn_getpadsubmit);
	etphone=(EditText) findViewById(R.id.et_phone);
	etpad=(EditText) findViewById(R.id.et_pad);
	etrepad=(EditText) findViewById(R.id.et_repad);
	etname=(EditText) findViewById(R.id.et_name);
	etanswer=(EditText) findViewById(R.id.et_answer);
	spspinner = (Spinner) findViewById(R.id.sp_spinner);
	 //将可选内容与ArrayAdapter连接起来
    adapter = ArrayAdapter.createFromResource(this, R.array.plantes, android.R.layout.simple_spinner_item);
    //设置下拉列表的风格 
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    //将adapter 添加到spinner中
     spspinner.setAdapter(adapter);
     spspinner.setOnItemSelectedListener(new SpinnerXMLSelectedListener());
     spspinner.setVisibility(View.VISIBLE);
	button1.setOnClickListener(this);
	button2.setOnClickListener(this);
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_register){
		Intent intent=new Intent(this,LoginActivity.class);
		startActivity(intent);
	}else if(id==R.id.btn_getpadsubmit){
		String phone=etphone.getText().toString();
		String pad=etpad.getText().toString();
		String repad=etrepad.getText().toString();
		String name=etname.getText().toString();
		String answer=etanswer.getText().toString();
		String progrems=progrem;
		int pid=pdid;
		if(phone.equals("")||phone==null){
			Toast.makeText(this, "手机号不能为空！", Toast.LENGTH_LONG).show();
			return;
		} if(!Utils.isPhone(phone)){
			  Toast.makeText(this, "手机号格式不正确，请重新输入！", Toast.LENGTH_LONG).show();
			  return;
		}
		if(pad.equals("")||pad==null){
			Toast.makeText(this, "密码不能为空！", Toast.LENGTH_LONG).show();
			return;
		}if(repad.equals("")||repad==null){
			Toast.makeText(this, "确认密码不能为空！", Toast.LENGTH_LONG).show();
			return;
		}
		if(!repad.equals(pad)){
			Toast.makeText(this, "确认密码错误！", Toast.LENGTH_LONG).show();
			return ;
		}
		if(name.equals("")||name==null){
			Toast.makeText(this, "昵称不能为空！", Toast.LENGTH_LONG).show();
			return;
		}if(answer.equals("")||answer==null){
			Toast.makeText(this, "请输入您所选择的问题答案，以方便找回密码！", Toast.LENGTH_LONG).show();
			return;
		}
		UserPeople up=new UserPeople(name, pad, Long.valueOf(phone), pid, answer);
		Utils.uploadDataByHttpClientPost(this, up);
		
//		Intent data=new Intent();
//		data.putExtra("phone", phone);
//		data.putExtra("pad", pad);
//		setResult(RESULT_OK, data);
//		finish();
	}
}
//使用XML形式操作
class SpinnerXMLSelectedListener implements OnItemSelectedListener
{
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
            long arg3) {
	    pdid=arg2;
		progrem=(String) adapter.getItem(arg2);
    }
    public void onNothingSelected(AdapterView<?> arg0) {
         
    }
}
}
