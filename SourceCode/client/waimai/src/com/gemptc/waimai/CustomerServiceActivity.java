package com.gemptc.waimai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class CustomerServiceActivity extends Activity implements OnClickListener {
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_customerservice);
	RelativeLayout complientphone=(RelativeLayout) findViewById(R.id.rela_updatename);
	RelativeLayout serviceonline=(RelativeLayout) findViewById(R.id.sendmessage);
	Button btnreturn=(Button) findViewById(R.id.btn_register);
	complientphone.setOnClickListener(this);
	serviceonline.setOnClickListener(this);
	btnreturn.setOnClickListener(this);
	
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.rela_updatename){
		Intent intent = new Intent (this,ShowPhoneActivity.class);			
		startActivity(intent);	
	}else if(id==R.id.sendmessage){
		Intent intent = new Intent (this,ServiceOnlineActivity.class);			
		startActivity(intent);	
	}else if(id==R.id.btn_register){
		Intent intent=new Intent(this,PersonSettingActivity.class);
		startActivity(intent);
	}

	
}
}
