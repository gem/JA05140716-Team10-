package com.gemptc.waimai;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import net.tsz.afinal.FinalBitmap;

import com.gemptc.entity.OrderList;
import com.gemptc.waimai.util.NetUtil;
import com.gemptc.waimai.util.JListView.OnRefreshListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.net.VpnService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class orderlistviewpageActivity extends Activity implements
		OnRefreshListener, OnClickListener {
	private int one;
	private int pre;
	private ImageView ivTitle;

	private ProgressDialog dialog;

	// private ArrayAdapter<String> adapter;
	private OrderListAdapter1 adapter1;
	private OrderListAdapter2 adapter2;
	private OrderListAdapter3 adapter3;
	private FinalBitmap fb;
	private List<OrderList> data1 = new ArrayList<OrderList>();
	private List<OrderList> data2 = new ArrayList<OrderList>();
	private List<OrderList> data3 = new ArrayList<OrderList>();
	private int currentpage1 = 1;// 当前页
	private int currentpage2 = 1;// 当前页
	private int currentpage3 = 1;// 当前页
	private String phone = "";

	private boolean is_divpage;// 是否需要分页
	public String Path1 = "";
	public String Path2 = "";
	public String Path3 = "";
	private int state1 = 0;
	private int state2 = 1;
	private ListView lv1;
	private ListView lv2;
	private ListView lv3;
	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	private ViewPager vp;

	public List<OrderList> getData1() {
		return data1;
	}

	public void setData(List<OrderList> data1) {
		this.data1 = data1;
	}

	public List<OrderList> getData2() {
		return data2;
	}

	public void setData2(List<OrderList> data2) {
		this.data2 = data2;
	}

	public List<OrderList> getData3() {
		return data3;
	}

	public void setData3(List<OrderList> data3) {
		this.data3 = data3;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去标题
		setContentView(R.layout.activity_orderlistviewpage);
		Button btnreturn = (Button) findViewById(R.id.btn_back);
		btnreturn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int id = v.getId();
				if (id == R.id.btn_back) {
					startActivity(new Intent(orderlistviewpageActivity.this,
							MainActivity.class));
				}
			}
		});
		// 初始化ImageView
		tv1 = (TextView) findViewById(R.id.tv_title1);
		tv2 = (TextView) findViewById(R.id.tv_title2);
		tv3 = (TextView) findViewById(R.id.tv_title3);
		tv1.setOnClickListener(new myonclick(0));
		tv2.setOnClickListener(new myonclick(1));
		tv3.setOnClickListener(new myonclick(2));
		ivTitle = (ImageView) findViewById(R.id.iv_title);
		// 获得屏幕宽度
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics outMetrics = new DisplayMetrics();
		display.getMetrics(outMetrics);
		int displayWidth = outMetrics.widthPixels;// 屏幕的宽度
		// 图片的宽度
		Bitmap bm = BitmapFactory.decodeResource(getResources(),
				R.drawable.hoz_line);
		int imgWidth = bm.getWidth();
		int dx = (displayWidth / 3 - imgWidth) / 2;// 默认位置
		Matrix matrix = new Matrix();
		matrix.postTranslate(dx, 0);
		ivTitle.setImageMatrix(matrix);
		// 标题1所在位置
		one = displayWidth / 3;
		// 定义ViewPager显示的页面
		SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
		phone = sp.getString("phone", "");
		Path1 = "http://10.203.1.44:8080/waimai/OrderListByPhone?phone="
				+ phone + "&currentPage=";
		Path3 = "http://10.203.1.44:8080/waimai/OrderListByphoneAndState?phone="
				+ phone + "&state=0" + "&currentPage=";
		Path2 = "http://10.203.1.44:8080/waimai/OrderListByphoneAndState?phone="
				+ phone + "&state=1" + "&currentPage=";
		dialog = new ProgressDialog(this);
		dialog.setTitle("温馨提示");
		dialog.setMessage("Loading...");
		fb = FinalBitmap.create(this);
		adapter1 = new OrderListAdapter1();
		adapter2 = new OrderListAdapter2();
		adapter3 = new OrderListAdapter3();
		List<View> views = new ArrayList<View>();
		LayoutInflater inflater = LayoutInflater.from(this);

		// 第一个页面
		View view1 = inflater.inflate(R.layout.activity_orderlist1, null);
		views.add(view1);
		lv1 = (ListView) view1.findViewById(R.id.lv_order_list1);
		new MyTask1().execute(Path1 + currentpage1);
		// 监听滚动条
		lv1.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// 判断是否需要分页或者滚动条是否到了最下面
				if (is_divpage
						&& scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
					new MyTask1().execute(Path1 + currentpage1);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// 判断出现在屏幕内的第一行加上最后一行是否等于屏幕能显示的总行数
				is_divpage = (firstVisibleItem + visibleItemCount == totalItemCount);
			}
		});

		// 监听listview条目
		lv1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
			}
		});

		// 第二个页面
		View view2 = inflater.inflate(R.layout.activity_orderlist2, null);
		views.add(view2);
		lv2 = (ListView) view2.findViewById(R.id.lv_order_list2);
		new MyTask2().execute(Path2 + currentpage2);
		// 监听滚动条
		lv2.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// 判断是否需要分页或者滚动条是否到了最下面
				if (is_divpage
						&& scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
					new MyTask2().execute(Path2 + currentpage2);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// 判断出现在屏幕内的第一行加上最后一行是否等于屏幕能显示的总行数
				is_divpage = (firstVisibleItem + visibleItemCount == totalItemCount);
			}
		});

		// 监听listview条目
		lv2.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
			}
		});

		// 第三个页面
		View view3 = inflater.inflate(R.layout.activity_orderlist3, null);
		views.add(view3);
		lv3 = (ListView) view3.findViewById(R.id.lv_order_list3);

		new MyTask3().execute(Path3 + currentpage3);
		// 监听滚动条
		lv3.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// 判断是否需要分页或者滚动条是否到了最下面
				if (is_divpage
						&& scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
					new MyTask3().execute(Path3 + currentpage3);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// 判断出现在屏幕内的第一行加上最后一行是否等于屏幕能显示的总行数
				is_divpage = (firstVisibleItem + visibleItemCount == totalItemCount);
			}
		});

		// 监听listview条目
		lv3.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
			}
		});

		// 设置ViewPager adapter
		vp = (ViewPager) findViewById(R.id.vp_type);
		vp.setCurrentItem(0);
		PagerAdapterType adapter = new PagerAdapterType(views);
		vp.setAdapter(adapter);
		// 监听ViewPager事件
		vp.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
				// 位置变化动画
				// RotateAnimation ScaleAnimation AlphaAnimation
				Animation anim = new TranslateAnimation(one * pre, one
						* position, 0, 0);
				pre = position;
				anim.setDuration(500);
				anim.setFillAfter(true);// 保持动画最终状态
				ivTitle.startAnimation(anim);
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});
	}

	// 创建第一个新的线程
	class MyTask1 extends AsyncTask<String, Void, List<OrderList>> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog.show();
		}

		@Override
		protected List<OrderList> doInBackground(String... params) {
			// 请求数据
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(Path1 + currentpage1);
			try {
				HttpResponse response = client.execute(get);
				if (response.getStatusLine().getStatusCode() == 200) {
					InputStream is = response.getEntity().getContent();
					byte[] data = convertInputStream2ByteArray(is);
					String json = new String(data);// json字符串
					Gson g = new Gson();
					List<OrderList> listnew = g.fromJson(json,
							new TypeToken<List<OrderList>>() {
							}.getType());
					return listnew;
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<OrderList> result) {
			if (result != null) {
				data1.addAll(result);
				adapter1.setData(data1);
				if (currentpage1 == 1) {
					lv1.setAdapter(adapter1);
				}
				adapter1.notifyDataSetChanged();
				currentpage1++;
				dialog.dismiss();
			} else {
				dialog.setTitle("提示");
				dialog.setMessage("没有数据了");
				dialog.dismiss();
			}
		}
	}

	// 创第二个新的线程
	class MyTask2 extends AsyncTask<String, Void, List<OrderList>> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog.show();
		}

		@Override
		protected List<OrderList> doInBackground(String... params) {
			// 请求数据
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(Path2 + currentpage2);
			try {
				HttpResponse response = client.execute(get);
				if (response.getStatusLine().getStatusCode() == 200) {
					InputStream is = response.getEntity().getContent();
					byte[] data = convertInputStream2ByteArray(is);
					String json = new String(data);// json字符串
					Gson g = new Gson();
					List<OrderList> listnew = g.fromJson(json,
							new TypeToken<List<OrderList>>() {
							}.getType());
					return listnew;
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<OrderList> result) {
			if (result != null) {
				for (OrderList o : result) {
					System.out.println(o.getFoodinfo());
				}
				data2.addAll(result);
				adapter2.setData(data2);
				if (currentpage2 == 1) {
					lv2.setAdapter(adapter2);
				}
				adapter2.notifyDataSetChanged();
				currentpage2++;
				dialog.dismiss();
			} else {
				dialog.setTitle("提示");
				dialog.setMessage("没有数据了");
				dialog.dismiss();
			}
		}
	}

	// 创建第三个新的线程
	class MyTask3 extends AsyncTask<String, Void, List<OrderList>> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog.show();
		}

		@Override
		protected List<OrderList> doInBackground(String... params) {
			// 请求数据
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(Path3 + currentpage3);
			try {
				HttpResponse response = client.execute(get);
				if (response.getStatusLine().getStatusCode() == 200) {
					InputStream is = response.getEntity().getContent();
					byte[] data = convertInputStream2ByteArray(is);
					String json = new String(data);// json字符串
					Gson g = new Gson();
					List<OrderList> listnew = g.fromJson(json,
							new TypeToken<List<OrderList>>() {
							}.getType());
					return listnew;
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<OrderList> result) {
			if (result != null) {
				data3.addAll(result);
				adapter3.setData(data3);
				if (currentpage3 == 1) {
					lv3.setAdapter(adapter3);
				}
				adapter3.notifyDataSetChanged();
				currentpage3++;
				dialog.dismiss();
			} else {
				dialog.setTitle("提示");
				dialog.setMessage("没有数据了");
				dialog.dismiss();
			}
		}
	}

	// 读取字符
	public static byte[] convertInputStream2ByteArray(InputStream is) {
		byte[] buffer = new byte[1024];
		int len;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			while ((len = is.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}

	// 自定义一个新的Adapter
	public class OrderListAdapter1 extends BaseAdapter {

		private List<OrderList> adapterData;

		public List<OrderList> getData() {
			return adapterData;
		}

		public void setData(List<OrderList> data) {
			this.adapterData = data;
		}

		public OrderListAdapter1() {
			super();
		}

		public OrderListAdapter1(List<OrderList> data) {
			this.adapterData = data;
		}

		// 说明Listview有多少个条目
		@Override
		public int getCount() {
			return adapterData.size();
		}

		// 说明position指定的条目关联的数据对象
		@Override
		public Object getItem(int position) {
			return adapterData.get(position);
		}

		// 条目的ID
		@Override
		public long getItemId(int position) {
			return position;
		}

		// 说明每个条目的布局
		// convertView:缓存的条目
		// parent:Listview
		// 返回值作为Listview的一个条目
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			OrderList OrderList = adapterData.get(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(orderlistviewpageActivity.this);
				convertView = inflater.inflate(R.layout.listview_item_order,
						null);
			}
			TextView tvFoodname = (TextView) convertView
					.findViewById(R.id.tv_foodname);
			// TextView tvFoodsum = (TextView)
			// convertView.findViewById(R.id.tv_foodsum);
			TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
			TextView tvTotalprice = (TextView) convertView
					.findViewById(R.id.tv_totalprice);

			tvFoodname.setText(OrderList.getFoodinfo());
			// tvFoodsum.setText(ol.getFoodinfo());
			tvDate.setText(OrderList.getOtime().toString());
			tvTotalprice.setText("总计:" + OrderList.getTotalprice());
			return convertView;
		}

	}

	// 自定义第二个adapter

	public class OrderListAdapter2 extends BaseAdapter {

		private List<OrderList> adapterData;

		public List<OrderList> getData() {
			return adapterData;
		}

		public void setData(List<OrderList> data) {
			this.adapterData = data;
		}

		public OrderListAdapter2() {
			super();
		}

		public OrderListAdapter2(List<OrderList> data) {
			this.adapterData = data;
		}

		// 说明Listview有多少个条目
		@Override
		public int getCount() {
			return adapterData.size();
		}

		// 说明position指定的条目关联的数据对象
		@Override
		public Object getItem(int position) {
			return adapterData.get(position);
		}

		// 条目的ID
		@Override
		public long getItemId(int position) {
			return position;
		}

		// 说明每个条目的布局
		// convertView:缓存的条目
		// parent:Listview
		// 返回值作为Listview的一个条目
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			OrderList OrderList = adapterData.get(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(orderlistviewpageActivity.this);
				convertView = inflater.inflate(R.layout.listview_item_order,
						null);
			}
			TextView tvFoodname = (TextView) convertView
					.findViewById(R.id.tv_foodname);
			// TextView tvFoodsum = (TextView)
			// convertView.findViewById(R.id.tv_foodsum);
			TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
			TextView tvTotalprice = (TextView) convertView
					.findViewById(R.id.tv_totalprice);

			tvFoodname.setText(OrderList.getFoodinfo());
			// tvFoodsum.setText(ol.getFoodinfo());
			tvDate.setText(OrderList.getOtime().toString());
			tvTotalprice.setText("总计:" + OrderList.getTotalprice());
			return convertView;
		}

	}

	// 自定义第三个adapter

	public class OrderListAdapter3 extends BaseAdapter {

		private List<OrderList> adapterData;

		public List<OrderList> getData() {
			return adapterData;
		}

		public void setData(List<OrderList> data) {
			this.adapterData = data;
		}

		public OrderListAdapter3() {
			super();
		}

		public OrderListAdapter3(List<OrderList> data) {
			this.adapterData = data;
		}

		// 说明Listview有多少个条目
		@Override
		public int getCount() {
			return adapterData.size();
		}

		// 说明position指定的条目关联的数据对象
		@Override
		public Object getItem(int position) {
			return adapterData.get(position);
		}

		// 条目的ID
		@Override
		public long getItemId(int position) {
			return position;
		}

		// 说明每个条目的布局
		// convertView:缓存的条目
		// parent:Listview
		// 返回值作为Listview的一个条目
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			OrderList OrderList = adapterData.get(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(orderlistviewpageActivity.this);
				convertView = inflater.inflate(R.layout.listview_item_order,
						null);
			}
			TextView tvFoodname = (TextView) convertView
					.findViewById(R.id.tv_foodname);
			// TextView tvFoodsum = (TextView)
			// convertView.findViewById(R.id.tv_foodsum);
			TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
			TextView tvTotalprice = (TextView) convertView
					.findViewById(R.id.tv_totalprice);

			tvFoodname.setText(OrderList.getFoodinfo());
			// tvFoodsum.setText(ol.getFoodinfo());
			tvDate.setText(OrderList.getOtime().toString());
			tvTotalprice.setText("总计:" + OrderList.getTotalprice());
			return convertView;
		}

	}

	@Override
	public void onDownPullRefresh() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadingMore() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
	}

	class PagerAdapterType extends PagerAdapter {
		private List<View> views;

		public PagerAdapterType(List<View> views) {
			this.views = views;
		}

		@Override
		public int getCount() {
			return views.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View view = views.get(position);
			container.addView(view);
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			View view = views.get(position);
			container.removeView(view);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	class myonclick implements OnClickListener {
		private int index = 0;

		public myonclick(int i) {
			index = i;
		}

		@Override
		public void onClick(View v) {
			vp.setCurrentItem(index);
		}

	}

}