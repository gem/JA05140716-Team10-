package com.gemptc.waimai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

public class TestingActivity extends Activity {
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_testing);
	new Handler().postDelayed(new Runnable(){
		@Override
		public void run(){		
			finish();
			Toast.makeText(getApplicationContext(), "恭喜您，您的已是最新版本！", Toast.LENGTH_LONG).show();
		}
	}, 3000);
}
}
