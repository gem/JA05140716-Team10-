package com.gemptc.waimai;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.GeofenceClient;
import com.baidu.location.LocationClient;
import android.app.Application;
import android.app.Service;
import android.os.Vibrator;
import android.util.Log;
import android.widget.TextView;

/**
 * 主Application
 */
public class LocationApplication extends Application {
	public LocationClient mLocationClient;//用来发起定位,添加取消监听
	public GeofenceClient mGeofenceClient;//地理围栏SDK核心类
	public MyLocationListener mMyLocationListener;//实现两个方法:定位请求回调函数+poi请求回调函数
	
	public String loactionResultstr;
	public TextView mLocationResult,logMsg;
	public TextView trigger,exit;
	public Vibrator mVibrator;//取得震动服务的句柄
	
	@Override
	public void onCreate() {
		super.onCreate();
		mLocationClient = new LocationClient(this.getApplicationContext());
		mMyLocationListener = new MyLocationListener();
		mLocationClient.registerLocationListener(mMyLocationListener);
		mGeofenceClient = new GeofenceClient(getApplicationContext());
		
		
		mVibrator =(Vibrator)getApplicationContext().getSystemService(Service.VIBRATOR_SERVICE);
	}

	
	/**
	 * 实现实位回调监听
	 * BDLocationListener接口有2个方法需要实现：
1.接收异步返回的定位结果，参数是BDLocation类型参数。
2.接收异步返回的POI查询结果，参数是BDLocation类型参数。
	 */
	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			//Receive Location 
			StringBuffer sb = new StringBuffer(256);
//			sb.append("time : ");
//			sb.append(location.getTime());
//			sb.append("\nerror code : ");
//			sb.append(location.getLocType());
//			sb.append("\nlatitude : ");
//			sb.append(location.getLatitude());
//			sb.append("\nlontitude : ");
//			sb.append(location.getLongitude());
//			sb.append("\nradius : ");//默认2000m
//			sb.append(location.getRadius());
			
//			sb.append("\nAddrStr: ");
//			sb.append(location.getAddrStr());
//			sb.append("\nProvince : ");
//			sb.append(location.getProvince());//获取省份信息
//			sb.append("\nCity : ");
			sb.append(location.getCity());//获取城市信息
			loactionResultstr=location.getStreet();
//			sb.append("\nDistrict : ");
			sb.append(location.getDistrict());//获取区县信息
//			sb.append("\nStreet : ");
//			sb.append(location.getStreet());
//			sb.append("\nStreetNumber(): ");
//			sb.append(location.getStreetNumber());
//			sb.append("\nFloor : ");
//			sb.append(location.getFloor());
			
//			if (location.getLocType() == BDLocation.TypeGpsLocation){
//				sb.append("\nspeed : ");
//				sb.append(location.getSpeed());
//				sb.append("\nsatellite : ");
//				sb.append(location.getSatelliteNumber());
//				sb.append("\ndirection : ");
//				sb.append("\naddr : ");
//				sb.append(location.getAddrStr());
//				sb.append(location.getDirection());
//			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation){
//				sb.append("\naddr : ");
//				sb.append(location.getAddrStr());
//				//运营商信息
//				sb.append("\noperationers : ");
//				sb.append(location.getOperators());
//			}
			logMsg(sb.toString());
//			Log.i("BaiduLocationApiDem", sb.toString());
		}


	}
	
	
	/**
	 * 显示请求字符串
	 * @param str
	 */
	public void logMsg(String str) {
		try {
			
//			str.substring(0,str.length()-2);
			
			if (mLocationResult != null)//****此处加断点，输出看值调试***
				mLocationResult.setText(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 高精度地理围栏回调
	 * @author jpren
	 *
	 */
	
}
