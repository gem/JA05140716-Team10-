package com.gemptc.waimai.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.http.RetryHandler;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import com.gemptc.entity.Restaurant;
import com.gemptc.waimai.ShowRestaurantActivity;
import com.gemptc.waimai.ShowRestaurantActivity.RestaurantAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.Dialog;
import android.os.AsyncTask;
import android.provider.ContactsContract.Contacts.Data;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;

public class NetUtil {
	public static final String Path="http://10.203.1.44:8080/waimai/RestaurantByPageServlet?currentPage=";
	
	public static void getDataFromServer(final ListView lv,final RestaurantAdapter adapter,final ShowRestaurantActivity activity,final int currentpage,final List<Restaurant> data) {
		AsyncTask<String, Void,List<Restaurant>> aTask=new AsyncTask<String, Void, List<Restaurant>>(){
			
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}
			
			@Override
			protected List<Restaurant> doInBackground(String... params) {
				/*
				BufferedInputStream bis = null;
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				try {
					URL urlObj = new URL(params[0]);
					HttpURLConnection httpConn = (HttpURLConnection) urlObj
							.openConnection();
					httpConn.setDoInput(true);
					httpConn.setDoOutput(false);
					httpConn.setRequestMethod("GET");
					httpConn.connect();
					if (httpConn.getResponseCode() == 200) {
						bis = new BufferedInputStream(httpConn.getInputStream());
						int c = 0;
						byte[] buffer = new byte[8 * 1024];
						while ((c = bis.read(buffer)) != -1) {
							baos.write(buffer, 0, c);
						}
						return baos.toByteArray();
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						if (bis != null) {
							bis.close();
						}
						if (baos != null) {
							baos.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				return null;
				*/
				
				
				HttpClient client=new DefaultHttpClient();
				HttpGet get=new HttpGet(Path+currentpage);
				try {
					HttpResponse response=client.execute(get);
					if(response.getStatusLine().getStatusCode()==200){		
						InputStream is=response.getEntity().getContent();
						byte[] data=convertInputStream2ByteArray(is);
						String json=new String(data);//json�ַ���
						Gson g=new Gson();
						List<Restaurant> listnew=g.fromJson(json, new TypeToken<List<Restaurant>>(){}.getType());
						return listnew;
					}
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
			@Override
			protected void onPostExecute(List<Restaurant> result) {
				if(result!=null){
					activity.setData(result);	
//					activity.getData().addAll(result);
					data.addAll(result);
					adapter.setData(data);
					lv.setAdapter(adapter);
					adapter.notifyDataSetChanged();
				}
			}
			
		};
		aTask.execute();
	}
	
	public static byte[] convertInputStream2ByteArray(InputStream is) {
		byte[] buffer = new byte[1024];
		int len;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			while ((len = is.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}
	
}
