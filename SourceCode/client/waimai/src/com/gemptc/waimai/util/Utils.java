package com.gemptc.waimai.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes.Name;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.tsz.afinal.FinalBitmap;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.MultipartPostMethod;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.nfc.tech.IsoDep;
import android.os.AsyncTask;
import android.renderscript.Type;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gemptc.entity.Collection;
import com.gemptc.entity.Restaurant;
import com.gemptc.entity.ShowPwd;
import com.gemptc.entity.UserPeople;
import com.gemptc.entity.showHeadPhoto;
import com.gemptc.waimai.AdviceActivity;
import com.gemptc.waimai.CustumerOrder;
import com.gemptc.waimai.GetPadActivity;
import com.gemptc.waimai.LoginActivity;
import com.gemptc.waimai.MainActivity;
import com.gemptc.waimai.ModifyNameActivity;
import com.gemptc.waimai.PersonCenterActivity;
import com.gemptc.waimai.PersonCenterActivity.MyAdapter;
import com.gemptc.waimai.RegisterActivity;
import com.gemptc.waimai.ShakeRestaurantActivity;
import com.gemptc.waimai.ShakeRestaurantActivity.RestaurantAdapter;
import com.gemptc.waimai.ShowPwdActivity;
import com.gemptc.waimai.UpdatePwdActivity;
import com.gemptc.waimai.UpdateRestaurantActivity;
import com.gemptc.waimai.UpdateRestaurantActivity.MyRestaurantAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Utils {
	public static final String PATH = "http://10.203.1.44:8080/waimai/AndroidUserLoginServlet";
	public static final String PATH1 = "http://10.203.1.44:8080/waimai/AndroidUsersRegisterServlet";
	public static final String PATH2 = "http://10.203.1.44:8080/waimai/AndroidUserFindpasswordServlet";
	public static final String PATH3 = "http://10.203.1.44:8080/waimai/AndroidUpdatePwdServlet";
	public static final String PATH4 = "http://10.203.1.44:8080/waimai/AndroidUpdateNameServlet";
	public static final String PATH5 = "http://10.203.1.44:8080/waimai/AndroidAdviceServlet";
	public static final String PATH7 = "http://10.203.1.44:8080/waimai/AndroidPersonCenteterActivity";
	public static final String PATH8 = "http://10.203.1.44:8080/waimai/AndroidUserCollectionCountServlet";
	public static final String PATH9 = "http://10.203.1.44:8080/waimai/AndroidUserContentCountServlet";
	public static final String PATH10 = "http://10.203.1.44:8080/waimai/AndroidShowUserNameServlet";
	public static final String PATH11 = "http://10.203.1.44:8080/waimai/AndroidUploadFileServlet";
	public static final String PATH12 = "http://10.203.1.44:8080/waimai/ShakeRestaurantServlet";
	public static final String PATH13 = "http://10.203.1.44:8080/waimai/AndroidMessageSend";
	public static final String PATH14 = "http://10.203.1.44:8080/waimai/AndroidAddSeat";
	public static final String PATH15 = "http://10.203.1.44:8080/waimai/ShakeRestaurantServlet";

	public static boolean isPhone(String s) {
		Pattern p = Pattern
				.compile("^[1]([3][0-9]{1}|[5][0-9]{1}|[8][0-9]{1}|[4][7])[0-9]{8}$");
		Matcher m = p.matcher(s);
		return m.matches();
	}

	public static void uploadDataByHttpClientPost(
			final LoginActivity loginActivity, final String phone,
			final String pad) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					parameters.add(new BasicNameValuePair("password",pad));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					Gson g=new Gson();
					showHeadPhoto shp=g.fromJson(result, showHeadPhoto.class);
					String jg=shp.getResult();
					String path=shp.getPhpath();
					if (jg.equals("true")) {
						Intent intent = new Intent(loginActivity,
								PersonCenterActivity.class);
						intent.putExtra("path", path);
						loginActivity.startActivity(intent);

						
					}else if (jg.equals("false")) {
						Toast.makeText(loginActivity, "密码错误，请再次输入密码！",
								Toast.LENGTH_LONG).show();
					} else if (jg.equals("no")) {
						Toast.makeText(loginActivity, "该用户尚未注册，请注册！",
								Toast.LENGTH_LONG).show();
					}
				}
			}
		};
		at.execute();

	}


	public static byte[] convertInputStream2ByteArray(InputStream is) {
		byte[] buffer = new byte[1024];
		int len = 0;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			while ((len = is.read(buffer)) > 0) {
				os.write(buffer, 0, len);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return os.toByteArray();
	}

	public static void uploadDataByHttpClientPost(
			final RegisterActivity registerActivity, final UserPeople up) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH1);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", String
							.valueOf(up.getPhone())));
					parameters.add(new BasicNameValuePair("password",
							URLEncoder.encode(up.getPassword(), "utf-8")));
					parameters.add(new BasicNameValuePair("name", URLEncoder
							.encode(up.getName(), "utf-8")));
					parameters.add(new BasicNameValuePair("pid", String
							.valueOf(up.getPid())));
					parameters.add(new BasicNameValuePair("answer", URLEncoder
							.encode(up.getAnswer(), "utf-8")));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					if ("true".equals(result)) {
						Toast.makeText(registerActivity, "注册成功！",
								Toast.LENGTH_LONG).show();
						Intent intent=new Intent(registerActivity,LoginActivity.class);
						registerActivity.startActivity(intent);
					} else if ("false".equals(result)) {
						Toast.makeText(registerActivity, "注册失败,该手机号已注册！",
								Toast.LENGTH_LONG).show();
					}
				}
			}
		};
		at.execute();

	}

	public static void uploadDataByHttpClientPost(
			final GetPadActivity getPadActivity, final String phone,
			final int pid, final String answer) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH2);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					parameters.add(new BasicNameValuePair("pid", String
							.valueOf(pid)));
					parameters.add(new BasicNameValuePair("answer", URLEncoder
							.encode(answer, "utf-8")));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					Gson g = new Gson();
					ShowPwd sp = g.fromJson(result, ShowPwd.class);
					String showresult = sp.getShowresult();
					String pwd = sp.getPassword();
					if ("true".equals(showresult)) {
						Intent intent = new Intent(getPadActivity,
								ShowPwdActivity.class);
						intent.putExtra("pwd", pwd);
						getPadActivity.startActivity(intent);
					} else if ("false".equals(showresult)) {
						Toast.makeText(getPadActivity, "对不起，您的问题或答案错误，密码无法找回！",
								Toast.LENGTH_LONG).show();

					} else if ("no".equals(showresult)) {
						Toast.makeText(getPadActivity, "该用户尚未注册，请注册！",
								Toast.LENGTH_LONG).show();
					}
				}
			}
		};
		at.execute();

	}

	public static void uploadPwdByHttpClientPost(
			final UpdatePwdActivity updatePwdActivity, final String phone,
			final String pad) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH3);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					parameters.add(new BasicNameValuePair("password",
							URLEncoder.encode(pad, "utf-8")));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					if ("true".equals(result)) {
						Toast.makeText(updatePwdActivity, "密码修改成功，请登录！",
								Toast.LENGTH_LONG).show();
					}
				}
			}
		};
		at.execute();
	}

	public static void uploadNameByHttpClientPost(
			final ModifyNameActivity modifyNameActivity, final String name,
			final String phone) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH4);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					parameters.add(new BasicNameValuePair("name", URLEncoder
							.encode(name, "utf-8")));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					if ("true".equals(result)) {
						Toast.makeText(modifyNameActivity, "昵称修改成功！",
								Toast.LENGTH_LONG).show();
					}
				}
			}
		};
		at.execute();
	}

	public static void uploadAdviceByHttpClientPost(
			final AdviceActivity adviceActivity, final String phone,
			final String advice) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH5);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					parameters.add(new BasicNameValuePair("advice", URLEncoder
							.encode(advice, "utf-8")));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					if ("true".equals(result)) {

						Toast.makeText(adviceActivity, "恭喜你，意见发表成功！",
								Toast.LENGTH_LONG).show();

					}
				}
			}
		};
		at.execute();
	}

	public static void uploadPhoneByHttpColient(
			final PersonCenterActivity personCenterActivity,
			final String phone, final ListView lv, final MyAdapter adapter,
			final TextView tvtips) {
		AsyncTask<Void, Void, List<Collection>> at = new AsyncTask<Void, Void, List<Collection>>() {

			@Override
			protected List<Collection> doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH7);
				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String json = new String(buffer);
						Gson g = new Gson();
						List<Collection> data = g.fromJson(json,
								new TypeToken<List<Collection>>() {
								}.getType());
						return data;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(List<Collection> result) {
				super.onPostExecute(result);
				if (result != null) {
					tvtips.setText("我的收藏");
					adapter.setData(result);
					lv.setAdapter(adapter);
				}

			}
		};
		at.execute();
	}

	public static void getCollectionCountByName(
			final PersonCenterActivity personCenterActivity,
			final String phone, final TextView tvcollect) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH8);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					tvcollect.setText(result);
				}
			}
		};
		at.execute();
	}

	public static void getContentCountByPhone(
			final PersonCenterActivity personCenterActivity,
			final String phone, final TextView tvcriticle) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH9);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					tvcriticle.setText(result);
				}
			}
		};
		at.execute();
	}

	public static void showUserNameByPhoneAndPassword(
			final PersonCenterActivity personCenterActivity,
			final String phone, final String password, final TextView tvname) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH10);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("phone", phone));
					parameters.add(new BasicNameValuePair("password",
							URLEncoder.encode(password, "utf-8")));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					tvname.setText(result);
				}
			}
		};
		at.execute();

	}

	public static void uploadFileByHttpClient(final Activity activity,
			final String phone, final File file) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
				try {
					MultipartPostMethod method = new MultipartPostMethod(PATH11);
					method.addParameter("phone", phone);
					method.addParameter("file", file);
					client.executeMethod(method);
					if (method.getStatusLine().getStatusCode() == 200) {

						return method.getResponseBodyAsString();
					}
				} catch (HttpException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(String result) {
				if (result != null) {
					System.out.println("111");
				} else {
					System.out.println("222");
				}
			}
		};
		at.execute();
	}

	public static void StartShareApp(Context context,
			final String szChooserTitle, final String title, final String msg) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, title);
		intent.putExtra(Intent.EXTRA_TEXT, msg);
		context.startActivity(Intent.createChooser(intent, szChooserTitle));
	}

	public static void getDataByHttpClient(
			final ShakeRestaurantActivity shakeRestaurantActivity,
			final ImageView iv, final TextView tvname, final TextView tvdesc,
			final TextView tvphone, final FinalBitmap fb) {
		AsyncTask<Void, Void, Restaurant> at = new AsyncTask<Void, Void, Restaurant>() {

			@Override
			protected Restaurant doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH12);
				try {
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String json = new String(buffer);
						Gson g = new Gson();
						Restaurant rt = g.fromJson(json, Restaurant.class);
						return rt;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(Restaurant result) {
				super.onPostExecute(result);
				if (result != null) {
					String name = result.getRname();
					String jpgurl = result.getJpgurl();
					long phone = result.getTelephone();
					String desc = result.getInfo();
					tvname.setText("店名：" + name);
					tvdesc.setText("简介：" + desc);
					tvphone.setText("电话：" + phone);
					fb.display(iv, jpgurl);
				}
			}

		};
		at.execute();
	}

	public static void getRestaurantDataByHttpClient(
			final UpdateRestaurantActivity updateRestaurantActivity,
			final MyRestaurantAdapter adapter, final ListView lv) {
		AsyncTask<Void, Void, List<Restaurant>> at = new AsyncTask<Void, Void, List<Restaurant>>() {

			@Override
			protected List<Restaurant> doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH13);
				try {
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String json = new String(buffer);
						Gson g = new Gson();
						List<Restaurant> data = g.fromJson(json,
								new TypeToken<List<Restaurant>>() {
								}.getType());
						return data;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(List<Restaurant> result) {
				super.onPostExecute(result);
				if (result != null) {
					adapter.setData(result);
					lv.setAdapter(adapter);
				}
			}
		};
		at.execute();
	}
	
	public static void uploadDataByHttpClientPost(
			final CustumerOrder custumerOrder, final int rid,
			final String seat, final int isbj,final String time,final String uphone) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH14);

				UrlEncodedFormEntity entity;
				try {
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("uphone",String.valueOf(uphone)));
					parameters.add(new BasicNameValuePair("rid", String
							.valueOf(rid)));
					parameters.add(new BasicNameValuePair("seat", String
							.valueOf(seat)));
					parameters.add(new BasicNameValuePair("isbj", String
							.valueOf(isbj)));
					parameters.add(new BasicNameValuePair("time", URLEncoder
							.encode(time, "utf-8")));
					entity = new UrlEncodedFormEntity(parameters);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String back = new String(buffer);
						return back;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result != null) {
					Toast.makeText(custumerOrder, "恭喜您预定成功！", Toast.LENGTH_LONG).show();
					custumerOrder.startActivity(new Intent(custumerOrder,MainActivity.class));
				}else{
					Toast.makeText(custumerOrder, "提交失败！", Toast.LENGTH_LONG).show();
				}
			}
		};
		at.execute();

	}
	
	public static void getDataByHttpClient(
			final ShakeRestaurantActivity shakeRestaurantActivity,
			final RestaurantAdapter adapter,final FinalBitmap fb, final ListView lv) {
		AsyncTask<Void, Void, List<Restaurant>> at = new AsyncTask<Void, Void, List<Restaurant>>() {

			@Override
			protected List<Restaurant> doInBackground(Void... params) {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(PATH15);
				try {
					
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte buffer[] = convertInputStream2ByteArray(is);
						String json = new String(buffer);
						Gson g = new Gson();
						List<Restaurant> data = g.fromJson(json,
								new TypeToken<List<Restaurant>>() {
								}.getType());
						return data;

					}
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(List<Restaurant> result) {
				super.onPostExecute(result);
				if (result != null) {
					adapter.setData(result);
					lv.setAdapter(adapter);
				}

			}
		};
		at.execute();
	}
}
