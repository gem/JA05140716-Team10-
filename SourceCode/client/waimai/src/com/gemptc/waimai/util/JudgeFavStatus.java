package com.gemptc.waimai.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.gemptc.waimai.Net;
import com.gemptc.waimai.R;
import com.gemptc.waimai.ShowFoodActivity;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

public class JudgeFavStatus extends AsyncTask<Void, Void, String> {
	private android.os.Message msgMessage;
	private String rname;
	private String usephone;
	private String rid;
	public static String favYesOrNo = "http://10.203.1.44:8080/waimai/SelectCollectionServlet";
	
	private ImageView ivTab3;
	private ShowFoodActivity aFoodActivity;

	public JudgeFavStatus(Message msgMessage, String rname, String usephone,
			String rid) {
		super();
		this.msgMessage = msgMessage;
		this.rname = rname;
		this.usephone = usephone;
		this.rid = rid;
	}

	@Override
	protected String doInBackground(Void... params) {

		HttpClient hClient = new DefaultHttpClient();
		try {
			// HttpPost post = new HttpPost((favYesOrNo + "?rname="
			// + URLEncoder.encode(String.valueOf(rname), "utf-8")
			// + "&usephone=" + URLEncoder.encode(
			// String.valueOf(usephone), "utf-8")+"&rid="+rid));
			HttpPost post = new HttpPost(favYesOrNo);
			List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
			// 6.设置发送的数据
			reqParams.add(new BasicNameValuePair("rname", URLEncoder.encode(
					rname, "utf-8")));// post编码
			reqParams.add(new BasicNameValuePair("usephone", URLEncoder.encode(
					usephone, "utf-8")));// post编码
			reqParams.add(new BasicNameValuePair("rid", rid));// post编码
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(reqParams);
			// 4.设置发送的实体
			post.setEntity(entity);
			HttpResponse response = hClient.execute(post);
			if (response.getStatusLine().getStatusCode() == 200) {
				InputStream is = response.getEntity().getContent();
				byte[] data = NetUtil.convertInputStream2ByteArray(is);
				String back = new String(data);
				return back;
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (result != null) {
			char c = result.charAt(0);
			if (c == 't') {
				msgMessage.obj = "true";
				msgMessage.what = 1;
				msgMessage.sendToTarget();
			} else if (c == 'f') {
				msgMessage.obj = "false";
				msgMessage.what = 1;
				msgMessage.sendToTarget();
			}
		}
//		if(result != null){
//			if (result.contains("false")) {
//				// if("".equals(usephone)){
//				// Toast.makeText(ShowFoodActivity.this, "对不起请登陆",
//				// Toast.LENGTH_SHORT).show();
//				// }else{
//				ivTab3.setBackgroundResource(R.drawable.heart_red_48);
//				Toast.makeText(aFoodActivity, "收藏成功", Toast.LENGTH_SHORT)
//				.show();
//				Net.uploadAddCollectDataByHttpClientPost(aFoodActivity.JPGURI, rname,aFoodActivity.rinfo,
//						aFoodActivity.rphone, aFoodActivity.usephone, aFoodActivity.rsite, aFoodActivity.rid, aFoodActivity.sname);
//				aFoodActivity.flag = "true";
//				// }
//				// ImageView ivTab3 = (ImageView)tabIndicator3.getChildAt(0);
//			} else {
//				// ImageView ivTab3 = (ImageView)tabIndicator3.getChildAt(0);
//				ivTab3.setBackgroundResource(R.drawable.heart_48);
//				Toast.makeText(aFoodActivity, "取消收藏", Toast.LENGTH_SHORT)
//				.show();
//				Net.uploadRemoveCollectDataByHttpClientPost(usephone, rid);
//				aFoodActivity.flag = "false";
//			}
//		}
	}

	// @Override
	// protected void onPostExecute(List<Restaurant> result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// if (result != null) {
	// msgMessage.obj = result;
	// msgMessage.what = 1;
	// msgMessage.sendToTarget();
	// }
	// }

}
