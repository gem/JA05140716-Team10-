package com.gemptc.waimai;

import java.util.List;

import net.tsz.afinal.FinalBitmap;

import com.gemptc.entity.Collection;
import com.gemptc.entity.Restaurant;
import com.gemptc.waimai.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class UpdateRestaurantActivity extends Activity implements OnClickListener {
private ListView lv;
private FinalBitmap fb;
private MyRestaurantAdapter adapter;

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_updaterestaurant);
	lv=(ListView) findViewById(R.id.listView1);
	Button btnreturn=(Button) findViewById(R.id.btn_register);
	btnreturn.setOnClickListener(this);
	lv.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			
		}
	});
	fb = FinalBitmap.create(this);
	adapter=new MyRestaurantAdapter();
	Utils.getRestaurantDataByHttpClient(this,adapter,lv);
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_register){
		Intent intent=new Intent(this,MainActivity.class);
		startActivity(intent);
	}
	
}
public class MyRestaurantAdapter extends BaseAdapter{
	private List<Restaurant> data;

	public List<Restaurant> getData() {
	return data;
}

public void setData(List<Restaurant> data) {
	this.data = data;
}

	public MyRestaurantAdapter(List<Restaurant> data) {
	super();
	this.data = data;
}

	public MyRestaurantAdapter() {
		super();
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Restaurant restaurant=data.get(position);
		if(convertView == null){
			LayoutInflater inflater  = LayoutInflater.from(UpdateRestaurantActivity.this);
			convertView = inflater.inflate(R.layout.activity_shoplist, null);
		}  
		ImageView ivshop=(ImageView) convertView.findViewById(R.id.iv_shoppic);
		TextView tvname = (TextView) convertView.findViewById(R.id.tv1_tvname);
		TextView tvdesc = (TextView) convertView.findViewById(R.id.tv_tvdesc);
		TextView tvphone = (TextView) convertView.findViewById(R.id.tv_tvphone);
		String URL=restaurant.getJpgurl();
		fb.display(ivshop, URL);// 根据URL设置图片
		tvname.setText("店名："+restaurant.getRname());
		tvdesc.setText("简介："+restaurant.getInfo());
		tvphone.setText("电话："+restaurant.getTelephone());
		return convertView;
	}
	
}
}
