package com.gemptc.waimai;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.gemptc.waimai.util.Utils;
import com.google.gson.Gson;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WriteMessageActivity extends Activity {
	ArrayList<ShopCar> orderlist = new ArrayList<ShopCar>();
	private EditText etbuyername;
	private EditText etphone;
	private EditText etaddress;
	private String rid;
	private String sum;
	private String usephone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_writemessage);

		rid = getIntent().getStringExtra("rid");
		sum = String.valueOf(getIntent().getDoubleExtra("sum", 0));
		orderlist = this.getIntent().getParcelableArrayListExtra("orderlist");
		SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
		usephone = sp.getString("phone", "");

		Gson g = new Gson();
		final String gson = g.toJson(orderlist);

		etbuyername = (EditText) findViewById(R.id.ed_buyername);// 收货人姓名
		etphone = (EditText) findViewById(R.id.ed_phone);// 收货人的电话
		etaddress = (EditText) findViewById(R.id.ed_address);// 收货人的地址
		Button btnsubmit = (Button) findViewById(R.id.btn_submit);// 获得提交按钮

		// 提交按钮的监听
		btnsubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String buyname = etbuyername.getText().toString();
				String phone = etphone.getText().toString();
				String address = etaddress.getText().toString();

				if (!buyname.equals("")) {// 判断收货人姓名是否为空
					if (!phone.equals("")&&Utils.isPhone(phone)) {// 判断收货人电话是否为空
						if (!address.equals("")) {// 判断收货人地址是否为空
							uploadOrderDataByHttpClientPost(
									WriteMessageActivity.this, gson, sum,
									buyname, phone, address, rid, usephone);
							Toast.makeText(WriteMessageActivity.this, "提交成功",
									Toast.LENGTH_SHORT).show();
							Intent intent = new Intent(
									WriteMessageActivity.this,
									ShowRestaurantActivity.class);
							startActivity(intent);
						} else {
							Toast.makeText(WriteMessageActivity.this,
									"收货地址不能为空", Toast.LENGTH_SHORT).show();
							return;
						}
					} else {
						Toast.makeText(WriteMessageActivity.this, "输入的手机号码为空或格式不正确",
								Toast.LENGTH_SHORT).show();
						return;
					}
				} else {
					Toast.makeText(WriteMessageActivity.this, "收货人姓名不能为空",
							Toast.LENGTH_SHORT).show();
					return;
				}
			}
		});
	}

	public static void uploadOrderDataByHttpClientPost(
			final WriteMessageActivity writeMessageActivity, final String gson,
			final String sum, final String buyname, final String phone,
			final String address, final String rid, final String usephone) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				// 1.创建浏览器
				HttpClient client = new DefaultHttpClient();
				try {
					// 3.说明发送的请求类型
					HttpPost post = new HttpPost(
							"http://10.203.1.44:8080/waimai/GetOrderServlet");
					List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
					// 6.设置发送的数据
					SimpleDateFormat df = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");// 设置日期格式
					String date = df.format(new Date());// new Date()为获取当前系统时间

					reqParams.add(new BasicNameValuePair("gson", URLEncoder
							.encode(gson, "utf-8")));
					reqParams.add(new BasicNameValuePair("sum", sum));
					reqParams.add(new BasicNameValuePair("buyname", URLEncoder
							.encode(buyname, "utf-8")));// post编码
					reqParams.add(new BasicNameValuePair("phone", phone));
					reqParams.add(new BasicNameValuePair("address", URLEncoder
							.encode(address, "utf-8")));// post编码
					reqParams.add(new BasicNameValuePair("date", date));
					reqParams.add(new BasicNameValuePair("rid", rid));
					reqParams.add(new BasicNameValuePair("usephone", usephone));
					// 5.表单
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
							reqParams);
					// 4.设置发送的实体
					post.setEntity(entity);
					// 2.发送请求到服务器
					HttpResponse response = client.execute(post);
					if (response.getStatusLine().getStatusCode() == 200) {
						InputStream is = response.getEntity().getContent();
						byte[] data = convertInputStream2ByteArray(is);
						String back = new String(data);
						return back;
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
			}
		};
		at.execute();
	}

	public static byte[] convertInputStream2ByteArray(InputStream is) {
		byte[] buffer = new byte[1024];
		int len;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			while ((len = is.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}

}
