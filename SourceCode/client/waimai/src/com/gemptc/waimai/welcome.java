package com.gemptc.waimai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class welcome extends Activity {
	private ImageView welcomeimg;
	private TranslateAnimation translateAnimation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.welcome);
		welcomeimg = (ImageView) findViewById(R.id.welcome1);
		translateAnimation = new TranslateAnimation(0, 0, 40, 350);
		translateAnimation.setDuration(1800);
		translateAnimation.setFillAfter(true);
		welcomeimg.setAnimation(translateAnimation);
		translateAnimation.setAnimationListener(new animationimpl());
	}

	private class animationimpl implements AnimationListener {

		@Override
		public void onAnimationEnd(Animation animation) {
			skip();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub

		}

	}

	private void skip() {
		Intent intent=new Intent(welcome.this,MainActivity.class);
		startActivity(intent);
		finish();
	}
}
