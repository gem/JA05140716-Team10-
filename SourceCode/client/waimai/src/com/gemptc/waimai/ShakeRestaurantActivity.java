package com.gemptc.waimai;

import java.util.List;

import com.gemptc.entity.Restaurant;
import com.gemptc.waimai.util.Utils;

import net.tsz.afinal.FinalBitmap;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ShakeRestaurantActivity extends Activity implements OnClickListener {


private FinalBitmap fb;
private ListView lv;
private RestaurantAdapter adapter;

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_shakeresturantresult);
	Button butreturn=(Button) findViewById(R.id.btn_register);
	lv=(ListView) findViewById(R.id.lv_list4);
	adapter=new RestaurantAdapter();
	fb=FinalBitmap.create(this);
	Utils.getDataByHttpClient(this,adapter,fb,lv);
	butreturn.setOnClickListener(this);
	lv.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Restaurant restaurant=(Restaurant) adapter.getItem(position);
			Intent intent=new Intent();
			intent.putExtra("RID", String.valueOf(restaurant.getRid()));
			intent.putExtra("RNAME", restaurant.getRname());
			intent.putExtra("TELEPHONE", String.valueOf(restaurant.getTelephone()));
			intent.putExtra("RSITE", restaurant.getRsite());
			intent.putExtra("PIC", restaurant.getJpgurl());
			intent.putExtra("RINFO", restaurant.getInfo());
			intent.putExtra("sname", restaurant.getSname());
			intent.setClass(ShakeRestaurantActivity.this, ShowFoodActivity.class);
			startActivity(intent);		
			
		}
	});

	
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_register){
		Intent intent=new Intent(this,ShakeActivity.class);
		startActivity(intent);
	}
}
//自定义一个新的Adapter
	public class RestaurantAdapter extends BaseAdapter {

		private List<Restaurant> adapterData;


		public List<Restaurant> getData() {
			return adapterData;
		}

		public void setData(List<Restaurant> data) {
			this.adapterData = data;
		}

		public RestaurantAdapter() {
			super();
		}

		public RestaurantAdapter(List<Restaurant> data) {
			this.adapterData = data;
		}

		// 说明Listview有多少个条目
		@Override
		public int getCount() {
			return adapterData.size();
		}

		// 说明position指定的条目关联的数据对象
		@Override
		public Object getItem(int position) {
			return adapterData.get(position);
		}

		// 条目的ID
		@Override
		public long getItemId(int position) {
			return position;
		}

		// 说明每个条目的布局
		// convertView:缓存的条目
		// parent:Listview
		// 返回值作为Listview的一个条目
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Restaurant Restaurant = adapterData.get(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(ShakeRestaurantActivity.this);
				convertView = inflater.inflate(
						R.layout.activity_listitem_restaurant, null);
			}
			TextView tvTitle = (TextView) convertView
					.findViewById(R.id.tv_resname);
			TextView tvDesc = (TextView) convertView
					.findViewById(R.id.tv_resinfo);
			TextView tvContent = (TextView) convertView
					.findViewById(R.id.textView3);
			ImageView ivPic = (ImageView) convertView
					.findViewById(R.id.imageView1);
			// 设置控件的数据
			String url="http://10.203.1.44:8080/SellerImages/"+Restaurant.getSname()+".jpg";
			fb.display(ivPic, url);
			tvTitle.setText("店名：" + Restaurant.getRname());
			tvDesc.setText("简介：" + Restaurant.getInfo());
			tvContent.setText("手机号码：" + Restaurant.getTelephone());
			return convertView;
		}

	}
}
