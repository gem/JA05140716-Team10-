package com.gemptc.waimai;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ShowPhoneActivity extends Activity implements OnClickListener {
	private Button btnphone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_complientphone);
		btnphone=(Button) findViewById(R.id.exitBtn1);
		Button btncancel=(Button) findViewById(R.id.exitBtn2);
		btnphone.setOnClickListener(this);
		btncancel.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.exitBtn1){
		String number  = btnphone.getText().toString();
		Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);//把电话号码显示在界面上，不打出去
		//intent.setAction(Intent.ACTION_CALL);//直接拨打电话
		Uri uri = Uri.parse("tel:"+number);
		intent.setData(uri);
		startActivity(intent);
	}else if(id==R.id.exitBtn2){
		this.finish(); 
	}

		
	}
	}

