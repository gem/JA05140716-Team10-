package com.gemptc.waimai;


import com.gemptc.waimai.util.Utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.app.PendingIntent;
public class PersonSettingActivity extends Activity implements OnClickListener {
private SharedPreferences sp;
//private final static int SCANNIN_GREQUEST_CODE = 1;
public static PersonSettingActivity instance = null;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_settings);
	   //启动activity时不自动弹出软键盘
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
    instance = this;
	RelativeLayout rlupname=(RelativeLayout) findViewById(R.id.rela_updatename);
	RelativeLayout servicecenter = (RelativeLayout) findViewById(R.id.sendmessage);
	RelativeLayout yaoyiyao=(RelativeLayout) findViewById(R.id.bindweibo);
	RelativeLayout saoyisao=(RelativeLayout) findViewById(R.id.saoyisao);
	RelativeLayout share=(RelativeLayout) findViewById(R.id.share);
	RelativeLayout rladvice=(RelativeLayout) findViewById(R.id.rela_addadvice);
	RelativeLayout introduce=(RelativeLayout) findViewById(R.id.thisintroduce);
	RelativeLayout newwedition=(RelativeLayout) findViewById(R.id.newedition);
	Button button2=(Button) findViewById(R.id.btn_register);
	Button button=(Button) findViewById(R.id.exit);

	rlupname.setOnClickListener(this);
	yaoyiyao.setOnClickListener(this);
	saoyisao.setOnClickListener(this);
	rladvice.setOnClickListener(this);
	introduce.setOnClickListener(this);
	share.setOnClickListener(this);
	newwedition.setOnClickListener(this);
	button.setOnClickListener(this);
	button2.setOnClickListener(this);
	servicecenter.setOnClickListener(this);
	sp=getSharedPreferences("login", MODE_PRIVATE);
}


@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.rela_updatename){//修改昵称
		Intent intent=new Intent(this,ModifyNameActivity.class);
		startActivity(intent);
	}else if(id==R.id.bindweibo){//摇一摇
		Intent intent = new Intent (this,ShakeActivity.class);			
		startActivity(intent);	
	}else if(id==R.id.saoyisao){//扫一下
		Intent intent = new Intent();
		intent.setClass(this, MipcaActivityCapture.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		//startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
	}else if(id==R.id.rela_addadvice){//提建议
		Intent intent=new Intent(this,AdviceActivity.class);
		startActivity(intent);
	}else if(id==R.id.thisintroduce){//软件介绍
		Intent intent=new Intent(this,SoftWareIntroduce.class);
		startActivity(intent);
	}else if(id==R.id.share){//分享
		Utils.StartShareApp(this, "分享到", "分享到",
				"我发现了一款很好的软件,很不错!赶快来试试哦！\n http://10.203.1.44:8080/waimai/user/UserloginShow.jsp");
	}else if(id==R.id.newedition){
		Intent intent=new Intent(this,TestingActivity.class);
		startActivityForResult(intent, 4);
	}else if(id==R.id.exit){
		Intent intent = new Intent (this,ExitFromSettings.class);			
		startActivity(intent);	
	}else if(id==R.id.btn_register){
		Intent intent=new Intent(this,PersonCenterActivity.class);
		startActivity(intent);
	}else if (id == R.id.sendmessage) {// 客服中心
		Intent intent = new Intent(this, CustomerServiceActivity.class);
		startActivity(intent);
	} 

	
}

}

