package com.gemptc.waimai;

import com.gemptc.waimai.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class GetPadActivity extends Activity implements OnClickListener {
private Button butsub;
private Button butreturn;
private EditText et;
private String progrem;
private int pdid;
private EditText etanswer;
private Spinner spspinner;
private ArrayAdapter adapter;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_getpad);
	butsub=(Button) findViewById(R.id.btn_getpadsubmit);
	butreturn=(Button) findViewById(R.id.btn_getpad);
	et=(EditText) findViewById(R.id.et_phone);
	etanswer=(EditText) findViewById(R.id.et_answer);
	spspinner = (Spinner) findViewById(R.id.sp_spinner);
	 //将可选内容与ArrayAdapter连接起来
    adapter = ArrayAdapter.createFromResource(this, R.array.plantes, android.R.layout.simple_spinner_item);
    //设置下拉列表的风格 
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    //将adapter 添加到spinner中
     spspinner.setAdapter(adapter);
     spspinner.setOnItemSelectedListener(new SpinnerXMLSelectedListener());
     spspinner.setVisibility(View.VISIBLE);
	butreturn.setOnClickListener(this);
	butsub.setOnClickListener(this);
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_getpadsubmit){
		String phone=et.getText().toString();
		String answer=etanswer.getText().toString();
		String progrems=progrem;
		int pid=pdid;
		if(phone.equals("")||phone==null){
			Toast.makeText(this, "手机号不能为空！", Toast.LENGTH_LONG).show();
			return;
		} if(!Utils.isPhone(phone)){
			  Toast.makeText(this, "手机号格式不正确，请重新输入！", Toast.LENGTH_LONG).show();
			  return;
		  }if(answer.equals("")||answer==null){
				Toast.makeText(this, "请输入您所选择的问题答案，以方便找回密码！", Toast.LENGTH_LONG).show();
				return;
			}
		  Utils.uploadDataByHttpClientPost(this,phone,pid,answer);
		
	}else if(id==R.id.btn_getpad){
		Intent intent=new Intent(this,LoginActivity.class);
		startActivity(intent);
	}
	
}
//使用XML形式操作
class SpinnerXMLSelectedListener implements OnItemSelectedListener
{
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
          long arg3) {
	    pdid=arg2;
		progrem=(String) adapter.getItem(arg2);
  }
  public void onNothingSelected(AdapterView<?> arg0) {
       
  }
}
}
