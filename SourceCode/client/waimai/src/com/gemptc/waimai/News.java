package com.gemptc.waimai;


public class News {
	private String title;//新闻标题
	private String desc;//新闻描述
	private int count;//评论数
	//private int resId;//图片资源化的Id
	private String imgURL;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
//	public int getResId() {
//		return resId;
//	}
//	public void setResId(int resId) {
//		this.resId = resId;
//	}
	public News(String title, String desc, int count, String imgURL) {
		super();
		this.title = title;
		this.desc = desc;
		this.count = count;
		this.imgURL=imgURL;
		//this.resId = resId;
	}
	public String getImgURL() {
		return imgURL;
	}
	public void setImgURL(String imgURL) {
		this.imgURL = imgURL;
	}
	
}
