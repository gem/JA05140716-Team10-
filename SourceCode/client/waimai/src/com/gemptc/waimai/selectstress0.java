package com.gemptc.waimai;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.Sampler.Value;
import android.text.StaticLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class selectstress0 extends Activity  implements OnClickListener{
	final String data[] = { "观前街道", "平江路街道", "苏锦街道", "娄门街道", "城北街道", "桃花坞" };
	final String data1[] = { "唯亭街道", "斜塘街道", "娄葑街道", "胜浦街道", "独墅湖科教创新区" };
	final String data2[] = { "公园街道", "府前街道", "南门街道", "吴门桥街道", "葑门街道", "双塔街",
			"友新街道", "胥江街道" };
	final String data3[] = { "长桥镇", "胥口镇", "木渎镇", "横泾镇", "浦庄镇", "渡村镇", "东山镇",
			"西山镇", "藏书镇", "光福镇", "镇湖镇", "东渚镇", "角直镇", "车坊镇", "郭巷镇" };
	final String data4[] = { "太平街道", "北桥街道", "元和街道 ", "黄桥街道" };
	final String data5[] = { "石路街道", "彩香街道", "留园街道", "虎丘街道", "白洋湾街道" };
	final String data6[] = { "横塘街道", "狮山街道", "枫桥街道", "镇湖街道" };
	private ArrayAdapter<String> adapter;
	private int posit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去标题
		setContentView(R.layout.activity_stress0);
		Button btnreturn=(Button) findViewById(R.id.btn_back);
		btnreturn.setOnClickListener(this);
		ListView lvlocation = (ListView) findViewById(R.id.lv_location);
		Intent intent = getIntent();
		posit = intent.getIntExtra("position", -1);
		switch (posit) {
		case 0:
			adapter = new ArrayAdapter<String>(this,
					R.layout.activity_listitem_location, R.id.tv_cityname, data);
			break;
		case 1:
			adapter = new ArrayAdapter<String>(this,
					R.layout.activity_listitem_location, R.id.tv_cityname,
					data1);
			break;
		case 2:
			adapter = new ArrayAdapter<String>(this,
					R.layout.activity_listitem_location, R.id.tv_cityname,
					data2);
			break;
		case 3:
			adapter = new ArrayAdapter<String>(this,
					R.layout.activity_listitem_location, R.id.tv_cityname,
					data3);
			break;
		case 4:
			adapter = new ArrayAdapter<String>(this,
					R.layout.activity_listitem_location, R.id.tv_cityname,
					data4);
			break;
		case 5:
			adapter = new ArrayAdapter<String>(this,
					R.layout.activity_listitem_location, R.id.tv_cityname,
					data5);
			break;
		case 6:
			adapter = new ArrayAdapter<String>(this,
					R.layout.activity_listitem_location, R.id.tv_cityname,
					data6);
			break;
		default:
			break;
		}
		lvlocation.setAdapter(adapter);

		// 监听list条目
		lvlocation.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String area = "";
				switch (posit) {
				case 0:
					area = data[position];
					break;
				case 1:
					area = data1[position];
					break;
				case 2:
					area = data2[position];
					break;
				case 3:
					area = data3[position];
					break;
				case 4:
					area = data4[position];
					break;
				case 5:
					area = data5[position];
					break;
				case 6:
					area = data6[position];
					break;
				default:
					break;
				}
				Intent intent=new Intent();
				intent.putExtra("stress", area);
				Log.i("TAG",area);
				intent.setClass(selectstress0.this, ShowStressRestaurantActivity.class);
				startActivity(intent);
			}
		});
	}
	
	@Override
	public void onClick(View v) {
		int id=v.getId();
		if(id==R.id.btn_back){
			startActivity(new Intent(this,selectlocation.class));
		}
	}
}
