package com.gemptc.waimai;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ShopCarActivity extends Activity{

	 
	private NewsAdapter adapter;
	private TextView total;
	private Button btnAccount;
    List<Double> t = new ArrayList<Double>();
    private double totalprice = 0;
    ArrayList<ShopCar> list = new ArrayList<ShopCar>();
    ArrayList<ShopCar> list1 = new ArrayList<ShopCar>();
    private String rid;
    private Double sum = 0.0;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shopcar);
		total = (TextView) findViewById(R.id.tv_totalprice);
		rid = getIntent().getStringExtra("rid");
		list = this.getIntent().getParcelableArrayListExtra("foodlist");
		for(ShopCar s:list){
			totalprice += s.getFprice()*s.getNumber(); 
		}
		total.setText(""+totalprice);
		adapter = new NewsAdapter(list);
		ListView lv = (ListView) findViewById(R.id.lv_shopcarlist);
		lv.setAdapter(adapter);
		
		btnAccount = (Button) findViewById(R.id.btn_account);
		btnAccount.setOnClickListener(new OnClickListener() {
			
			private String usephone;

			@Override
			public void onClick(View v) {
				for(ShopCar sc:list){
					ShopCar s = new ShopCar();
					String fname = sc.getFname();
					Double fprice = sc.getFprice();
					int number = sc.getNumber();
					s.setFname(fname);
					s.setFprice(fprice);
					s.setNumber(number);
					list1.add(s);
					sum +=(fprice* (double)number);
				}
				//跳转到填写信息界面
				Intent intent = new Intent(ShopCarActivity.this,WriteMessageActivity.class);
				Bundle sBundle = new Bundle();
				sBundle.putParcelableArrayList("orderlist", list1);
				intent.putExtras(sBundle);
				intent.putExtra("rid", rid);
				intent.putExtra("sum", sum);
				SharedPreferences sp=getSharedPreferences("login", MODE_PRIVATE);
		        usephone=sp.getString("phone", "");
				if("".equals(usephone)){
					Toast.makeText(ShopCarActivity.this, "对不起，请登陆！", Toast.LENGTH_LONG).show();
				}else{
					startActivity(intent);
				}
			}
		});
	}
	
	        //自定义Adapter
			class NewsAdapter extends BaseAdapter{
		          private List<ShopCar> sc;
		          
				public List<ShopCar> getSc() {
					return sc;
				}

				public void setSc(List<ShopCar> sc) {
					this.sc = sc;
				}

				public NewsAdapter(List<ShopCar> sc){
		        	  this.sc = sc;
		          }
		        
		          public NewsAdapter(){
		        	  
		          }
		          
		          
		       //说明ListView有多少条目
				@Override
				public int getCount() {
					return sc.size();
				}

				//说明position指定的条目关联的数据对象
				@Override
				public Object getItem(int position) {
					return sc.get(position);
				}

				//条目的id
				@Override
				public long getItemId(int position) {
					return position;
				}

				//说明每个条目的布局
				//convertView：缓存的条目
				//parent：ListView
				//返回值作为ListView一个条目
				@Override
				public View getView(final int position, View convertView, ViewGroup parent) {
					
					final ShopCar s = sc.get(position);
					if(convertView == null){
						LayoutInflater inflater = LayoutInflater.from(ShopCarActivity.this);
					    convertView = inflater.inflate(R.layout.listview_item_shopcar, null);
					}
					TextView tvScfname = (TextView)convertView.findViewById(R.id.tv_scfname);
					TextView tvScfprice = (TextView)convertView.findViewById(R.id.tv_scfprice);
					TextView tvScnumber = (TextView) convertView.findViewById(R.id.tv_scnumber);
					ImageView ivAdd = (ImageView) convertView.findViewById(R.id.iv_add);
					ImageView ivRemove = (ImageView) convertView.findViewById(R.id.iv_remove);
					tvScfname.setText(s.getFname());			
					tvScfprice.setText(s.getFprice()+"元/份");
				    tvScnumber.setText(s.getNumber()+"");
				    
				    //再次添加食物数量
				    ivAdd.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
								s.setNumber(s.getNumber()+1);
								totalprice = 0;
								for(ShopCar s:list){
									totalprice += s.getFprice()*s.getNumber(); 
								}
								total.setText("共"+totalprice+"元");
								notifyDataSetChanged();
						}
					});
				    
				    //再次减去食物的数目
				    ivRemove.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if(s.getNumber()>1){
								s.setNumber(s.getNumber()-1);
								totalprice = 0;
								for(ShopCar s:list){
									totalprice += s.getFprice()*s.getNumber(); 
								}
								total.setText("共"+totalprice+"元");
								notifyDataSetChanged();
							}else{
								list.remove(position);
								totalprice = 0;
								for(ShopCar s:list){
									totalprice += s.getFprice()*s.getNumber(); 
								}
								total.setText("共"+totalprice+"元");
								notifyDataSetChanged();
							}
						}
					});
					return convertView;
				}
			}
}
