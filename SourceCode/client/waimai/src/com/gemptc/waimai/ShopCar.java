package com.gemptc.waimai;

import android.os.Parcel;
import android.os.Parcelable;

public class ShopCar implements Parcelable {

	private String fname;
	private double fprice;
	private int number;
	
	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public double getFprice() {
		return fprice;
	}

	public void setFprice(double fprice) {
		this.fprice = fprice;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public ShopCar() {
		super();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	// 实现Parcel接口必须覆盖实现的方法
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		/*
		 * 将list写入Parcel，
		 */
		dest.writeString(fname);
		dest.writeDouble(fprice);
		dest.writeInt(number);
	}

	// 该静态域是必须要有的，而且名字必须是CREATOR，否则会出错
	public static final Parcelable.Creator<ShopCar> CREATOR = new Parcelable.Creator<ShopCar>() {

		@Override
		public ShopCar createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			// 从Parcel读取通过writeToParcel方法写入相关成员信息
			ShopCar sc = new ShopCar();
			sc.fname = source.readString();
			sc.fprice = source.readDouble();
			sc.number = source.readInt();
			return sc;
		}

		@Override
		public ShopCar[] newArray(int size) {
			// TODO Auto-generated method stub
			// 返回Person对象数组
			return new ShopCar[size];
		}

	};

}
