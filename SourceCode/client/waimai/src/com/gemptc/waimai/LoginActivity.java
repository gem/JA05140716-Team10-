package com.gemptc.waimai;


import com.gemptc.waimai.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener  {
private Button butregister;
private Button butsubmit;
private TextView butforpad;
private EditText et1;
private EditText et2;
private SharedPreferences sp;
private CheckBox cb;

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_login);
	butregister=(Button) findViewById(R.id.btn_register);
	butsubmit=(Button) findViewById(R.id.btn_submit);
	butforpad=(TextView) findViewById(R.id.forget_passwd);
	et1=(EditText) findViewById(R.id.et_phone);
	et2=(EditText) findViewById(R.id.et_pad);
	cb=(CheckBox) findViewById(R.id.checkBox1);
	sp=getSharedPreferences("login", MODE_PRIVATE);
	cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(!isChecked){
				sp.edit().remove("phone").remove("password").remove("isCheck").commit();
			}
	
		}
	});
	//初始化控件信息
		if(sp.getBoolean("isCheck", false)){
			String phone=sp.getString("phone", "");
			String password=sp.getString("password", "");
			et1.setText(phone);
			et2.setText(password);
			cb.setChecked(true);
		}
	butregister.setOnClickListener(this);
	butsubmit.setOnClickListener(this);
	butforpad.setOnClickListener(this);
	
	
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_register){
		Intent intent=new Intent(this,RegisterActivity.class);
		startActivity(intent);
	}else if(id==R.id.btn_submit){
		String phone=et1.getText().toString();
		String pad=et2.getText().toString();
		if(phone.equals("")||phone==null){
			Toast.makeText(this, "手机号不能为空！", Toast.LENGTH_LONG).show();
			return;
		}
		if(!Utils.isPhone(phone)){
			  Toast.makeText(this, "手机号格式不正确，请重新输入！", Toast.LENGTH_LONG).show();
			  return;
		  }
		if(pad.equals("")||pad==null){
			Toast.makeText(this, "密码不能为空！", Toast.LENGTH_LONG).show();
			return;
		}
		
		Utils.uploadDataByHttpClientPost(this, phone, pad);
//		Intent intent = new Intent(this,
//				PersonCenterActivity.class);
//		startActivity(intent);
			Editor et=sp.edit();
			et.putString("phone", phone);
			et.putString("password", pad);
			if(cb.isChecked()){
				et.putBoolean("isCheck", true);
			}
			et.commit();
	
		
	}else if(id==R.id.forget_passwd){
		Intent intent=new Intent(this,GetPadActivity.class);
		startActivity(intent);
	}
	
}
//@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		if(resultCode!=RESULT_OK){
//			return;
//		}
//		String phone=data.getStringExtra("phone");
//		String pad=data.getStringExtra("pad");
//		System.out.println(phone);
//		System.out.println(pad);
//		
//		if(requestCode==1){
//			et1.setText(phone);
//			et2.setText(pad);			
//		}
//	}
}
