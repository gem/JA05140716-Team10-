package com.gemptc.waimai;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.gemptc.entity.Content;
import com.gemptc.entity.Flag;
import com.gemptc.entity.Food;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewDebug.FlagToString;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class Net {
	
	
	
	//查询收藏店铺的信息
		public static void uploadSelectCollectDataByHttpClientPost(
				final String rname,final String usephone,final String rid) {
			AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>(){

				@Override
				protected String doInBackground(Void... params) {
					//1.创建浏览器
					HttpClient client = new DefaultHttpClient();
					try {
						//3.说明发送的请求类型
						HttpPost post = new HttpPost("http://10.203.1.44:8080/waimai/SelectCollectionServlet");
						List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
						//6.设置发送的数据
						reqParams.add(new BasicNameValuePair("usephone", usephone));
						reqParams.add(new BasicNameValuePair("rid", rid));
                        reqParams.add(new BasicNameValuePair("rname", URLEncoder.encode(rname,"utf-8")));
						//5.表单
						UrlEncodedFormEntity entity = new UrlEncodedFormEntity(reqParams);
						//4.设置发送的实体
						post.setEntity(entity);
						//2.发送请求到服务器
						HttpResponse response = client.execute(post);
						if(response.getStatusLine().getStatusCode() == 200){
							InputStream is = response.getEntity().getContent();
							byte []data = convertInputStream2ByteArray(is);
							String back = new String(data);
							return back;
						}
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}

				@Override
				protected void onPostExecute(String result) {
					super.onPostExecute(result);
					Flag f = new Flag();
					char c=result.charAt(0);
					if('t'==c){
						f.setIsflag("true");
					}else if('f'==c){
						f.setIsflag("false");
					}
					
//					if(result != null){
//						if("true".equals(result)){
//							ivTab3.setBackgroundResource(R.drawable.heart_red_48);
//							
//						}else if("false".equals(result)){
//							ivTab3.setBackgroundResource(R.drawable.heart_48);
//						}
//					}
				}
				
			};
			at.execute();
		}
	
	//添加收藏店铺的信息
	public static void uploadAddCollectDataByHttpClientPost(final String JPGURI,
			final String rname,final String rinfo,final String rphone,final String usephone,final String rsite,final String rid,final String sname) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>(){

			@Override
			protected String doInBackground(Void... params) {
				//1.创建浏览器
				HttpClient client = new DefaultHttpClient();
				try {
					//3.说明发送的请求类型
					HttpPost post = new HttpPost("http://10.203.1.44:8080/waimai/CollectAddServlet");
					List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
					//6.设置发送的数据
					reqParams.add(new BasicNameValuePair("cinfo",URLEncoder.encode(rinfo,"utf-8")));//post编码
					reqParams.add(new BasicNameValuePair("rname",URLEncoder.encode(rname,"utf-8")));//post编码
					reqParams.add(new BasicNameValuePair("usephone", usephone));
					reqParams.add(new BasicNameValuePair("rphone", rphone));
					reqParams.add(new BasicNameValuePair("jpguri", JPGURI));
					reqParams.add(new BasicNameValuePair("rsite",URLEncoder.encode(rsite,"utf-8")));
					reqParams.add(new BasicNameValuePair("rid", rid));
					reqParams.add(new BasicNameValuePair("sname",URLEncoder.encode(sname,"utf-8")));
					//5.表单
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(reqParams);
					//4.设置发送的实体
					post.setEntity(entity);
					//2.发送请求到服务器
					HttpResponse response = client.execute(post);
					if(response.getStatusLine().getStatusCode() == 200){
						InputStream is = response.getEntity().getContent();
						byte []data = convertInputStream2ByteArray(is);
						String back = new String(data);
						return back;
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
			}
			
		};
		at.execute();
	}
	
	//取消收藏店铺的信息
	public static void uploadRemoveCollectDataByHttpClientPost(
			final String usephone,final String rid) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>(){

			@Override
			protected String doInBackground(Void... params) {
				//1.创建浏览器
				HttpClient client = new DefaultHttpClient();
				try {
					//3.说明发送的请求类型
					HttpPost post = new HttpPost("http://10.203.1.44:8080/waimai/CollectDeleteServlet");
					List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
					//6.设置发送的数据
					reqParams.add(new BasicNameValuePair("usephone", usephone));
					reqParams.add(new BasicNameValuePair("rid", rid));
//					reqParams.add(new BasicNameValuePair("jpguri", JPGURI));
					//5.表单
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(reqParams);
					//4.设置发送的实体
					post.setEntity(entity);
					//2.发送请求到服务器
					HttpResponse response = client.execute(post);
					if(response.getStatusLine().getStatusCode() == 200){
						InputStream is = response.getEntity().getContent();
						byte []data = convertInputStream2ByteArray(is);
						String back = new String(data);
						return back;
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
			}
			
		};
		at.execute();
	}
	
	public static byte[] convertInputStream2ByteArray(InputStream is){
		byte[] buffer = new byte[1024];
		int len;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			while((len=is.read(buffer))>0){
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}
}
