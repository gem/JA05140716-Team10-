package com.gemptc.waimai;

import com.gemptc.waimai.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdviceActivity extends Activity implements OnClickListener {
private EditText et;
private SharedPreferences sp;
private String phone;

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_advices);
	Button button1=(Button) findViewById(R.id.submit);
	Button button2=(Button) findViewById(R.id.btn_register);
	et=(EditText) findViewById(R.id.editText1);
	button1.setOnClickListener(this);
	button2.setOnClickListener(this);
	sp=getSharedPreferences("login", MODE_PRIVATE);
	phone=sp.getString("phone", "");
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_register){
		Intent intent=new Intent(this,PersonSettingActivity.class);
		startActivity(intent);
	}else if(id==R.id.submit){
		String advice=et.getText().toString();
		if(advice.trim().equals("")||advice.trim()==null){
			Toast.makeText(this, "建议不能为空！", Toast.LENGTH_LONG).show();
			return;
		}
		if (CommonUtils.isFastDoubleClick()) {   
			Toast.makeText(this, "建议已提交，请不要重复提交！",Toast.LENGTH_LONG).show();
	        return;   
	    }else{ 
	    	Utils.uploadAdviceByHttpClientPost(this,phone,advice);
	}
	}
}
}
class CommonUtils {
    private static long lastClickTime;
    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        lastClickTime = time;  
        if ( 0 < timeD && timeD <20000) {  
            return true;  
        }  
        return false;  
    }
}

