package com.gemptc.waimai;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.gemptc.entity.Flag;
import com.gemptc.entity.Food;
import com.gemptc.waimai.util.DummyTabContent;
import com.gemptc.waimai.util.JudgeFavStatus;

import comment.CommentlistActivity;

import fragment.Collect_Fragment;
import fragment.Comments_Fragment;
import fragment.More_Fragment;
import fragment.ShopCar_Fragment;


import android.R.integer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewDebug.FlagToString;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;



/**
 * @Description: 主Activity，底部TabHost选项卡
 *
 * @Author Hades
 *
 * @date 2012-10-6
 *
 * @version V1.0  
 */

public class ShowFoodActivity extends FragmentActivity implements ShopCar_Fragment.Callback{

	TabHost tabHost;
	TabWidget tabWidget; 
	LinearLayout bottom_layout;
	int CURRENT_TAB = 0;	//设置常量
	ShopCar_Fragment shopFragment;
	Comments_Fragment commentFragment;
	More_Fragment moreFragment;
	Collect_Fragment collectFragment;
	android.support.v4.app.FragmentTransaction ft;
	RelativeLayout tabIndicator1,tabIndicator2,tabIndicator3,tabIndicator4;

	private String flag;// 判断是否收藏
	ArrayList<Food> list =  new ArrayList<Food>();
	ArrayList<ShopCar> save = new ArrayList<ShopCar>();
	ArrayList<ShopCar> list3 = new ArrayList<ShopCar>();
	private String rid ;
	private String rname;
	private String rsite;
	private String usephone;//用户的电话
	private String rphone;//餐厅电话
	private String rinfo;//餐厅简介
	private String JPGURI;//餐厅图片
	private ImageView ivTab3;
	private Handler handler;
	private String sname;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showfoodlist);
        findTabView();
        tabHost.setup();
        /*intent.putExtra("RID", restaurant.getRid());
				intent.putExtra("RNAME", restaurant.getRname());
				intent.putExtra("TELEPHONE", restaurant.getTelephone());
				intent.putExtra("RSITE", restaurant.getRsite());
				*/
        rid = getIntent().getStringExtra("RID");
        rname = getIntent().getStringExtra("RNAME");
        rsite = getIntent().getStringExtra("RSITE");
        rphone = getIntent().getStringExtra("TELEPHONE");
        JPGURI=getIntent().getStringExtra("PIC");
        sname=getIntent().getStringExtra("sname");
        rinfo=getIntent().getStringExtra("RINFO");
        System.out.println("3333333"+rinfo);
        SharedPreferences sp=getSharedPreferences("login", MODE_PRIVATE);
        usephone=sp.getString("phone", "");
//        rid="2";
//        rname = "一品铁板西餐厅";
//        rsite = "独墅湖文星广场";
//        rphone = "8123451";
//        usephone = "123456";
//        rinfo="本店主打经济餐";
        /** 监听*/
        TabHost.OnTabChangeListener tabChangeListener = new TabHost.OnTabChangeListener(){
			@Override
			public void onTabChanged(String tabId) {
				
				/**碎片管理*/
				android.support.v4.app.FragmentManager fm =  getSupportFragmentManager();
				shopFragment = (ShopCar_Fragment) fm.findFragmentByTag("shop");
				commentFragment = (Comments_Fragment) fm.findFragmentByTag("comment");
				moreFragment = (More_Fragment) fm.findFragmentByTag("more");
				collectFragment = (Collect_Fragment) fm.findFragmentByTag("collect");
				ft = fm.beginTransaction();
				
				/** 如果存在Detaches掉 */
				if(shopFragment!=null)
					ft.detach(shopFragment);
				
				/** 如果存在Detaches掉 */
				if(commentFragment!=null)
					ft.detach(commentFragment);
				
				/** 如果存在Detaches掉 */
				if(moreFragment!=null)
					ft.detach(moreFragment);
				
				/** 如果存在Detaches掉 */
				if(collectFragment!=null)
					ft.detach(collectFragment);
				
				/** 如果当前选项卡是shop */
				if(tabId.equalsIgnoreCase("shop")){
					isTabShop();
					CURRENT_TAB = 1;
					
				/** 如果当前选项卡是comment */
				}else if(tabId.equalsIgnoreCase("comment")){	
					isTabComment();
					CURRENT_TAB = 2;
					
				/** 如果当前选项卡是more */
				}else if(tabId.equalsIgnoreCase("more")){	
					isTabMore();
					CURRENT_TAB = 3;
					
				/** 如果当前选项卡是collect */
				}else if(tabId.equalsIgnoreCase("collect")){	
					isTabCollect();
					CURRENT_TAB = 4;
				}else{
					switch (CURRENT_TAB) {
					case 1:
						isTabShop();
						break;
					case 2:
						isTabComment();
						break;
					case 3:
						isTabMore();
						break;
					case 4:
						isTabCollect();
						break;
					default:
						isTabShop();
						break;
					}		
					
				}
					ft.commit();	
			}
        	
        };
        //设置初始选项卡  
        tabHost.setCurrentTab(0);
        tabHost.setOnTabChangedListener(tabChangeListener);
        initTab();
         /**  设置初始化界面  */
        tabHost.setCurrentTab(0);

    }
    
    //判断当前
    public void isTabShop(){
    	
    	if(shopFragment==null){	
    		ShopCar_Fragment shopcarfragment = new ShopCar_Fragment();
    		Bundle bundle = new Bundle();
    		bundle.putString("rid", rid);
    		bundle.putString("rname", rname);
    		bundle.putString("rsite", rsite);
    		bundle.putString("rphone", rphone);
    		bundle.putString("sname", sname);
    		bundle.putString("pic", JPGURI);
    		shopcarfragment.setArguments(bundle);
			ft.add(R.id.realtabcontent,shopcarfragment, "shop");						
		}else{
			ft.attach(shopFragment);						
		}
    }
    
    public void isTabComment(){
    	
    	if(commentFragment==null){
			ft.add(R.id.realtabcontent,new Comments_Fragment(), "comment");						
		}else{
			ft.attach(commentFragment);						
		}
    }
    
    public void isTabMore(){
    	
    	if(moreFragment==null){
			ft.add(R.id.realtabcontent,new More_Fragment(), "more");						
		}else{
			ft.attach(moreFragment);						
		}
    }
    
    public void isTabCollect(){
    	
    	if(collectFragment==null){
			ft.add(R.id.realtabcontent,new Collect_Fragment(), "collect");						
		}else{
			ft.attach(collectFragment);	
		}
    }
    
    
    
    /**
     * 找到Tabhost布局
     */
    public void findTabView(){
    	
    	 tabHost = (TabHost) findViewById(android.R.id.tabhost);
         tabWidget = (TabWidget) findViewById(android.R.id.tabs);
         LinearLayout layout = (LinearLayout)tabHost.getChildAt(0);
         TabWidget tw = (TabWidget)layout.getChildAt(1);
         
         tabIndicator1 = (RelativeLayout) LayoutInflater.from(this)
         		.inflate(R.layout.tab_indicator, tw, false);
         TextView tvTab1 = (TextView)tabIndicator1.getChildAt(1);
         ImageView ivTab1 = (ImageView)tabIndicator1.getChildAt(0);
         ivTab1.setBackgroundResource(R.drawable.shopcar_60);
         tvTab1.setText("购物车");

         tabIndicator2 = (RelativeLayout) LayoutInflater.from(this)
         		.inflate(R.layout.tab_indicator, tw, false);
         TextView tvTab2 = (TextView)tabIndicator2.getChildAt(1);
         ImageView ivTab2 = (ImageView)tabIndicator2.getChildAt(0);
         ivTab2.setBackgroundResource(R.drawable.comment_48);
         tvTab2.setText("评论"); 
         
         tabIndicator3 = (RelativeLayout) LayoutInflater.from(this)
         		.inflate(R.layout.tab_indicator, tw, false);
         TextView tvTab3 = (TextView)tabIndicator3.getChildAt(1);
         ImageView ivTab3 = (ImageView)tabIndicator3.getChildAt(0);
         ivTab3.setBackgroundResource(R.drawable.heart_48);
         tvTab3.setText("收藏");
          
         tabIndicator4 = (RelativeLayout) LayoutInflater.from(this)
         		.inflate(R.layout.tab_indicator, tw, false);
         TextView tvTab4 = (TextView)tabIndicator4.getChildAt(1);
         ImageView ivTab4 = (ImageView)tabIndicator4.getChildAt(0);
         ivTab4.setBackgroundResource(R.drawable.more_48);
         tvTab4.setText("更多");
         
    }
    
    /** 
     * 初始化选项卡
     * 
     * */
    public void initTab(){
    	
        TabHost.TabSpec tSpecShop = tabHost.newTabSpec("shop");
        tSpecShop.setIndicator(tabIndicator1);        
        tSpecShop.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tSpecShop);
        //购物车按钮监听事件
        tabIndicator1.setOnClickListener(new OnClickListener() {		
			private int num;

			@Override
			public void onClick(View v) {
				for(Food f:list){
					ShopCar sc = new ShopCar();
					String fname = f.getFname();
					Double fprice = f.getFprice();
					int number = f.getNumber();
					sc.setFname(fname);
					sc.setFprice(fprice);
					sc.setNumber(number);
					save.add(sc);
				}
				if(list3.size()==0){
					list3.addAll(save);
				}else{
					if(save.size()!=0){
						for(ShopCar sc:save){
							for(int i=0;i<list3.size();i++){
							    	if(sc.getFname().equals(list3.get(i).getFname())){
							    	  num = list3.get(i).getNumber() + sc.getNumber();
									  System.out.println("list3.get(i).getFname="+list3.get(i).getFname());
									  System.out.println("sc.getFname"+sc.getFname());
									  sc.setNumber(num);
									  list3.remove(i);
								  }
							    }
							    	 list3.add(sc);
							}
				          }
					}
				save.clear();
				save.addAll(list3);

				
				
				Intent intent = new Intent(ShowFoodActivity.this,ShopCarActivity.class);
				Bundle sBundle = new Bundle();
				sBundle.putParcelableArrayList("foodlist", save);
				intent.putExtras(sBundle);
				intent.putExtra("rid", rid);
				startActivity(intent);
				save.clear();
			}
		});
        
        TabHost.TabSpec tSpecComment = tabHost.newTabSpec("comment");
        tSpecComment.setIndicator(tabIndicator2);        
        tSpecComment.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tSpecComment);
        //评论按钮收藏事件
        tabIndicator2.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ShowFoodActivity.this,CommentlistActivity.class);
				intent.putExtra("rid", rid);
				startActivity(intent);
			}
			
        });  
        
        
        TabHost.TabSpec tSpecCollect = tabHost.newTabSpec("collect");
        tSpecCollect.setIndicator(tabIndicator3);        
        tSpecCollect.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tSpecCollect);
        ivTab3 = (ImageView)tabIndicator3.getChildAt(0);
        handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(android.os.Message msg) {
				// 工作线程中要发送的信息全都被放到了Message对象中，也就是上面的参数msg中。要进行操作就要先取出msg中传递的数据。
							switch (msg.what) {
				case 0:
					// 工作线程发送what为0的信息代表线程开启了。主线程中相应的显示一个进度对话框
					// pDialog.show();
					break;
				case 1:
					  
					// 工作线程发送what为1的信息代表要线程已经将需要的数据加载完毕。本案例中就需要将该数据获取到，显示到指定ImageView控件中即可。
					flag = (String) msg.obj;
					System.out.println(flag+"111111111111111111");
					if (flag.equals("true")) {
						ivTab3.setBackgroundResource(R.drawable.heart_red_48);
					} else if (flag.equals("false")) {
						ivTab3.setBackgroundResource(R.drawable.heart_48);
					}
					System.out.println("44444444"+flag);
					break;
				case 2: 
					// 工作线程发送what为2的信息代表工作线程结束。本案例中，主线程只需要将进度对话框取消即可。
					// pDialog.dismiss();
					break;
				}
			}
		};
		final JudgeFavStatus mJudgeFavStatus = new JudgeFavStatus(
				handler.obtainMessage(), rname, usephone,rid);
		mJudgeFavStatus.execute();
//		

        
		//收藏按钮监听事件
        tabIndicator3.setOnClickListener(new OnClickListener(){
        	
			@Override
			public void onClick(View v) {  
				if(flag.equals("false")){
//					if("".equals(usephone)){
//						Toast.makeText(ShowFoodActivity.this, "对不起请登陆", Toast.LENGTH_SHORT).show();
//					}else{
						ivTab3.setBackgroundResource(R.drawable.heart_red_48);
						Toast.makeText(ShowFoodActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
						Net.uploadAddCollectDataByHttpClientPost(JPGURI,rname,rinfo,rphone,usephone,rsite,rid,sname);
						flag = "true";
//					}
//				ImageView ivTab3 = (ImageView)tabIndicator3.getChildAt(0);
			}else{
//				ImageView ivTab3 = (ImageView)tabIndicator3.getChildAt(0);
				ivTab3.setBackgroundResource(R.drawable.heart_48);
				Toast.makeText(ShowFoodActivity.this, "取消收藏", Toast.LENGTH_SHORT).show();
				Net.uploadRemoveCollectDataByHttpClientPost(usephone,rid);
			    flag="false";
			}
		}
			
        });

        
        TabHost.TabSpec tSpecMore = tabHost.newTabSpec("more");
        tSpecMore.setIndicator(tabIndicator4);      
        tSpecMore.setContent(new DummyTabContent(getBaseContext()));
        tabHost.addTab(tSpecMore);
        
        //更多按钮监听事件
        tabIndicator4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ShowFoodActivity.this,MoreActivity.class);
				intent.putExtra("rname", rname);
				intent.putExtra("rinfo", rinfo);
				startActivity(intent);
		  }  
        });
    }
    
    //回滚方法，得到shopfragment的数据
	@Override
	public void getObject(Food food) {
		if(list.size()==0){
			list.add(food);
		}else{
		    for(Food f:list){
			  if(food.getFname().equals(f.getFname())){
				list.remove(f);
			   }
		    }
		    list.add(food);
		}
	}
	
}
