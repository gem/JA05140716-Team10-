package com.gemptc.waimai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MoreActivity extends Activity {
    private String rname;
    private String rinfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_more);
          rname = getIntent().getStringExtra("rname");
          rinfo = getIntent().getStringExtra("rinfo");
	}
	
	public void sharelauch(View view){
		String text = rname+"/"+rinfo;
		Intent intent=new Intent(Intent.ACTION_SEND);   
        intent.setType("image/*");   
        intent.putExtra(Intent.EXTRA_SUBJECT, "����");   
        intent.putExtra(Intent.EXTRA_TEXT, text);    
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);   
        startActivity(Intent.createChooser(intent, getTitle())); 
        finish();
	}
	
	public void errorlauch(View view){
		Intent intent = new Intent();
		startActivity(intent);
	}
	
	public void closelauch(View view){
		finish();
	}
}
