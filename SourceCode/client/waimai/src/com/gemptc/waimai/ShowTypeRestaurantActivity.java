package com.gemptc.waimai;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.gemptc.entity.Restaurant;
import com.gemptc.waimai.ShowRestaurantActivity.MyTask;
import com.gemptc.waimai.ShowRestaurantActivity.RestaurantAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class ShowTypeRestaurantActivity extends Activity implements OnClickListener {
	private Spinner spinner1;
	private Spinner spinner2;
	private ListView lv;
	private ArrayAdapter<String> adapter1;
	private ArrayAdapter<String> adapter2;
	private ProgressDialog dialog;
	// private ArrayAdapter<String> adapter;
	private RestaurantAdapter adapter;
	private FinalBitmap fb;
	private List<Restaurant> data = new ArrayList<Restaurant>();
	private int currentpage;// 当前页
	private boolean is_divpage;// 是否需要分页
	public String Path = "";
	private String string1;
	private String string2;

	public List<Restaurant> getData() {
		return data;
	}

	public void setData(List<Restaurant> data) {
		this.data = data;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去标题
		setContentView(R.layout.activity_showrestaurantbytype);
		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		Button btnreturn=(Button) findViewById(R.id.btn_back);
		btnreturn.setOnClickListener(this);
		lv = (ListView) findViewById(R.id.listView1);
		dialog = new ProgressDialog(this);
		dialog.setTitle("温馨提示");
		dialog.setMessage("Loading...");
		fb = FinalBitmap.create(this);
		List<String> list1 = new ArrayList<String>();
		list1.add("请选择类型");
		list1.add("小吃部");
		list1.add("小餐馆");
		list1.add("大酒店");
		List<String> list2 = new ArrayList<String>();
		list2.add("请选择区");
		list2.add("平江区");
		list2.add("工业园区");
		list2.add("沧浪区");
		list2.add("吴中区");
		list2.add("金阊区");
		list2.add("相城区");
		list2.add("虎丘区");
		adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, list1);
		adapter2 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, list2);
		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
		Path = "http://10.203.1.44:8080/waimai/RestaurantByPageServlet?currentPage=";
		currentpage = 1;
		adapter = new RestaurantAdapter();
		new MyTask().execute(Path + currentpage);

		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position1, long id) {
				string1 = spinner1.getItemAtPosition(position1).toString();
				if (!string1.equals("请选择类型") && string2 == null) {
					currentpage = 1;
					data.clear();
					Path = "http://10.203.1.44:8080/waimai/ShowRestaurantPageByType?type="
							+ string1 + "&currentPage=";
					new MyTask().execute(Path + currentpage);
				} else if (!string1.equals("请选择类型") && string2 != null) {
					currentpage = 1;
					data.clear();
					Path = "http://10.203.1.44:8080/waimai/RestaurantPageByStressAndType?type="
							+ string1 + "&city=" + string2 + "&currentPage=";
					new MyTask().execute(Path + currentpage);
				} else {
					currentpage = 1;
					data.clear();
					Path = "http://10.203.1.44:8080/waimai/RestaurantByPageServlet?currentPage=";
					new MyTask().execute(Path + currentpage);
				}

				spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position2, long id) {
						string2 = spinner2.getItemAtPosition(position2)
								.toString();
						if (!string1.equals("请选择类型") && !string2.equals("请选择区")) {
							currentpage = 1;
							data.clear();
							Path = "http://10.203.1.44:8080/waimai/RestaurantPageByStressAndType?type="
									+ string1
									+ "&city="
									+ string2
									+ "&currentPage=";
							System.out.println("222222222" + string1);
							System.out.println("33333333333" + Path);
							new MyTask().execute(Path + currentpage);
						} else if (!string2.equals("请选择区")
								&& string1.equals("请选择类型")) {
							currentpage = 1;
							data.clear();
							Path = "http://10.203.1.44:8080/waimai/RestaurantPageByCity?city="
									+ string2 + "&currentPage=";
							new MyTask().execute(Path + currentpage);
						} else if (!string1.equals("请选择类型")
								&& string2.equals("请选择区")) {
							currentpage = 1;
							data.clear();
							Path = "http://10.203.1.44:8080/waimai/ShowRestaurantPageByType?type="
									+ string1 + "&currentPage=";
							new MyTask().execute(Path + currentpage);
						} else {
							currentpage = 1;
							data.clear();
							Path = "http://10.203.1.44:8080/waimai/RestaurantByPageServlet?currentPage=";
							new MyTask().execute(Path + currentpage);
						}

						lv.setOnScrollListener(new OnScrollListener() {

							@Override
							public void onScrollStateChanged(AbsListView view,
									int scrollState) {
								// 判断是否需要分页或者滚动条是否到了最下面
								if (is_divpage
										&& scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
									new MyTask().execute(Path + currentpage);
								}
							}

							@Override
							public void onScroll(AbsListView view,
									int firstVisibleItem, int visibleItemCount,
									int totalItemCount) {
								// 判断出现在屏幕内的第一行加上最后一行是否等于屏幕能显示的总行数
								is_divpage = (firstVisibleItem
										+ visibleItemCount == totalItemCount);
							}
						});
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		// 监听滚动条
		lv.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// 判断是否需要分页或者滚动条是否到了最下面
				if (is_divpage
						&& scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
					new MyTask().execute(Path + currentpage);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// 判断出现在屏幕内的第一行加上最后一行是否等于屏幕能显示的总行数
				is_divpage = (firstVisibleItem + visibleItemCount == totalItemCount);
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		lv = (ListView) findViewById(R.id.listView1);
		// 监听list条目
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Restaurant restaurant = (Restaurant) adapter.getItem(position);
				Intent intent=new Intent();
				intent.putExtra("RID", restaurant.getRid());
				intent.putExtra("RNAME", restaurant.getRname());
				intent.putExtra("TELEPHONE", restaurant.getTelephone());
				intent.putExtra("RSITE", restaurant.getRsite());
				intent.putExtra("jpgurl", restaurant.getJpgurl());
				intent.putExtra("sname", restaurant.getSname());
				intent.setClass(ShowTypeRestaurantActivity.this, CustumerOrder.class);
				startActivity(intent);
			}
		});
	}

	// 创建一个新的线程
	class MyTask extends AsyncTask<String, Void, List<Restaurant>> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog.show();
		}

		@Override
		protected List<Restaurant> doInBackground(String... params) {
			// 请求数据
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(Path + currentpage);
			try {
				HttpResponse response = client.execute(get);

				if (response.getStatusLine().getStatusCode() == 200) {
					InputStream is = response.getEntity().getContent();
					byte[] data = convertInputStream2ByteArray(is);
					String json = new String(data);// json字符串
					Gson g = new Gson();
					List<Restaurant> listnew = g.fromJson(json,
							new TypeToken<List<Restaurant>>() {
							}.getType());
					return listnew;
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<Restaurant> result) {
			if (result != null) {
				data.addAll(result);
				adapter.setData(data);
				if (currentpage == 1) {
					lv.setAdapter(adapter);
				}
				adapter.notifyDataSetChanged();
				currentpage++;
				dialog.dismiss();

				// } else {
				// dialog.setTitle("提示");
				// dialog.setMessage("没有数据了");
				// dialog.dismiss();
			}
		}
	}

	// 读取字符
	public static byte[] convertInputStream2ByteArray(InputStream is) {
		byte[] buffer = new byte[1024];
		int len;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			while ((len = is.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}

	// 自定义一个新的Adapter
	public class RestaurantAdapter extends BaseAdapter {

		private List<Restaurant> adapterData;

		public List<Restaurant> getData() {
			return adapterData;
		}

		public void setData(List<Restaurant> data) {
			this.adapterData = data;
		}

		public RestaurantAdapter() {
			super();
		}

		public RestaurantAdapter(List<Restaurant> data) {
			this.adapterData = data;
		}

		// 说明Listview有多少个条目
		@Override
		public int getCount() {
			return adapterData.size();
		}

		// 说明position指定的条目关联的数据对象
		@Override
		public Object getItem(int position) {
			return adapterData.get(position);
		}

		// 条目的ID
		@Override
		public long getItemId(int position) {
			return position;
		}

		// 说明每个条目的布局
		// convertView:缓存的条目
		// parent:Listview
		// 返回值作为Listview的一个条目
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Restaurant Restaurant = adapterData.get(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(ShowTypeRestaurantActivity.this);
				convertView = inflater.inflate(
						R.layout.activity_listitem_restaurant, null);
			}
			TextView tvTitle = (TextView) convertView
					.findViewById(R.id.tv_resname);
			TextView tvDesc = (TextView) convertView
					.findViewById(R.id.tv_resinfo);
			TextView tvContent = (TextView) convertView
					.findViewById(R.id.textView3);
			ImageView ivPic = (ImageView) convertView
					.findViewById(R.id.imageView1);
			// 设置控件的数据
			tvTitle.setText("店名：" + Restaurant.getRname());
			tvDesc.setText("简介：" + Restaurant.getInfo());
			tvContent.setText("手机号码：" + Restaurant.getTelephone());
//			String URL = "http://10.203.1.44:8080/waimai/pic.jpg";
			 String URL = "http://10.203.1.44:8080/SellerImages/"+Restaurant.getSname()+".jpg";
			fb.display(ivPic, URL);// 根据URL设置图片
			// ivPic.setImageResource(Restaurant.getResId());
			return convertView;
		}

	}
	@Override
	public void onClick(View v) {
		int id=v.getId();
		if(id==R.id.btn_back){
			startActivity(new Intent(this,MainActivity.class));
		}
	}
}
