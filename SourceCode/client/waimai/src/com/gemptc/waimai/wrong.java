package com.gemptc.waimai;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class wrong extends Activity  implements OnClickListener{
private Button btn1;
private Button btn2;
private EditText et;

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
	setContentView(R.layout.baocuo);
	getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title2);
	btn1=(Button) findViewById(R.id.btn_notgood);
	btn2=(Button) findViewById(R.id.btn_notrun);
	et=(EditText)findViewById(R.id.editText1);
	btn1.setOnClickListener(this);
	btn2.setOnClickListener(this);
}

@Override
public void onClick(View v) {
	int id=v.getId();
	if(id==R.id.btn_notrun){
		et.setText("不送外卖了");
	}else if(id==R.id.btn_notgood){
		et.setText("价格要更新");
	}
	
}
}
