package com.gemptc.waimai;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import net.tsz.afinal.FinalBitmap;

import com.gemptc.entity.Collection;
import com.gemptc.entity.Restaurant;
import com.gemptc.waimai.util.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.StaticLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PersonCenterActivity extends Activity implements OnClickListener {
	private Button butset;
	private ImageView iv1;
	private FinalBitmap fb;
	private MyAdapter adapter;
	private ListView lv;
	private TextView tvname;
	private TextView tvcriticle;
	private TextView tvcollect;
	private TextView tvtips;
	private String phone;
	private String password;
	private ProgressDialog dialog;
	private List<Collection> data = new ArrayList<Collection>();
	private int currentpage = 1;// 当前页
	private boolean is_divpage;// 是否需要分页
	public static final String PATH16 = "http://10.203.1.44:8080/waimai/AndroidPersonCenteterActivity?currentPage=";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personcenter);
		butset = (Button) findViewById(R.id.main_tab_settings_settings);

		iv1 = (ImageView) findViewById(R.id.imageView1);
		Button button2 = (Button) findViewById(R.id.btn_register);
		tvname = (TextView) findViewById(R.id.textView1);
		tvcriticle = (TextView) findViewById(R.id.textView3);
		tvcollect = (TextView) findViewById(R.id.textView6);
		tvtips = (TextView) findViewById(R.id.textView7);
		lv = (ListView) findViewById(R.id.listView1);
		butset.setOnClickListener(this);
		iv1.setOnClickListener(this);
		button2.setOnClickListener(this);
		SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
		phone = sp.getString("phone", "");
		password = sp.getString("password", "");
		dialog = new ProgressDialog(this);
		dialog.setTitle("温馨提示");
		dialog.setMessage("Loading...");

		adapter = new MyAdapter();
		new MyTask().execute();
		// 监听滚动条
		lv.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// 判断是否需要分页或者滚动条是否到了最下面
				if (is_divpage
						&& scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
					new MyTask().execute(PATH16);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// 判断出现在屏幕内的第一行加上最后一行是否等于屏幕能显示的总行数
				is_divpage = (firstVisibleItem + visibleItemCount == totalItemCount);
			}
		});

		Utils.getCollectionCountByName(this, phone, tvcollect);
		Utils.getContentCountByPhone(this, phone, tvcriticle);
		Utils.showUserNameByPhoneAndPassword(this, phone, password, tvname);
		fb = FinalBitmap.create(this);
		Intent intent = getIntent();
		String pathphoto = "http://10.203.1.44:8080/SellerImages/"+phone+".jpg";
		fb.display(iv1, pathphoto);

	}

	@Override
	protected void onStart() {
		super.onStart();
		// 监听listview条目
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Collection collection = (Collection) adapter.getItem(position);
				Intent intent = new Intent();
				intent.putExtra("RID", String.valueOf(collection.getRid()));
				intent.putExtra("RNAME", collection.getRname());
				intent.putExtra("TELEPHONE",
						String.valueOf(collection.getTelephone()));
				intent.putExtra("RSITE", collection.getRsite());
				intent.putExtra("PIC", collection.getJpgurl());
				intent.putExtra("RINFO", collection.getInfo());
				intent.putExtra("sname", collection.getSname());
				intent.setClass(PersonCenterActivity.this,
						ShowFoodActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.main_tab_settings_settings) {
			Intent intent = new Intent(this, PersonSettingActivity.class);
			startActivity(intent);
		} else if (id == R.id.imageView1) {
			Intent intent = new Intent(this, GetPhotoActivity.class);
			startActivityForResult(intent, GetPhotoActivity.SELECT_PIC);

		} else if (id == R.id.btn_register) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK) {
			return;
		}
		String url = data.getStringExtra("url");
		// Utils.uploadFileByHttpClient(this,phone,password,url,iv1);
		Uri imageUri = Uri.parse(url);
		if (requestCode == GetPhotoActivity.SELECT_PIC) {
			if (imageUri != null) {
				InputStream is = null;
				try {
					// 读取图片到io流
					is = getContentResolver().openInputStream(imageUri);
					// 内存中的图片
					Bitmap bm = BitmapFactory.decodeStream(is);
					iv1.setImageBitmap(bm);
					String fileName = imageUri.toString().substring(
							imageUri.toString().lastIndexOf("//") + 1);
					File file = new File(fileName);// 上传找的图片
					System.out.println(imageUri.toString());
					System.out.println(fileName);
					Utils.uploadFileByHttpClient(PersonCenterActivity.this,
							phone, file);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		} else if (requestCode == GetPhotoActivity.TAKE_PHOTO) {
			Intent intent = new Intent("com.android.camera.action.CROP");
			intent.setDataAndType(imageUri, "image/*");
			intent.putExtra("crop", "true");
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("outputX", 300);
			intent.putExtra("outputY", 300);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);
			startActivityForResult(intent, GetPhotoActivity.CROP_PHOTO);// 启动裁剪
		} else if (requestCode == GetPhotoActivity.CROP_PHOTO) {// 获取裁剪后的结果
			Bundle bundle = data.getExtras();
			if (bundle != null) {
				Bitmap bm = bundle.getParcelable("data");
				iv1.setImageBitmap(bm);
			}
		}
	}

	public class MyAdapter extends BaseAdapter {
		private List<Collection> data;

		public List<Collection> getData() {
			return data;
		}

		public void setData(List<Collection> data) {
			this.data = data;
		}

		public MyAdapter(List<Collection> data) {
			super();
			this.data = data;
		}

		public MyAdapter() {
			super();
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Collection collection = data.get(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(PersonCenterActivity.this);
				convertView = inflater.inflate(
						R.layout.activity_listitem_restaurant, null);
			}
			TextView tvTitle = (TextView) convertView
					.findViewById(R.id.tv_resname);
			TextView tvDesc = (TextView) convertView
					.findViewById(R.id.tv_resinfo);
			TextView tvContent = (TextView) convertView
					.findViewById(R.id.textView3);
			ImageView ivPic = (ImageView) convertView
					.findViewById(R.id.imageView1);
			// 设置控件的数据
			tvTitle.setText("店名：" + collection.getRname());
			tvDesc.setText("简介：" + collection.getInfo());
			tvContent.setText("手机号码：" + collection.getTelephone());
//			String URL = "http://10.203.1.44:8080/waimai/pic.jpg";
			 String URL = collection.getJpgurl();
			 System.out.println("url"+URL);
			 String fileName1 = URL.toString().substring(
						URL.toString().lastIndexOf("\\") + 1);
			 System.out.println("file"+fileName1);
			 String URL1="http://10.203.1.44:8080/SellerImages/"+fileName1;
			fb.display(ivPic, URL1);// 根据URL设置图片
			return convertView;
		}

	}

	// 创建一个新的线程
	class MyTask extends AsyncTask<String, Void, List<Collection>> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog.show();
		}

		@Override
		protected List<Collection> doInBackground(String... params) {
			// 请求数据
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(PATH16 + currentpage);
			UrlEncodedFormEntity entity;
			try {
				List<NameValuePair> parameters = new ArrayList<NameValuePair>();
				parameters.add(new BasicNameValuePair("phone", phone));
				// parameters.add(new BasicNameValuePair("currentpage",
				// String.valueOf(currentpage)));
				entity = new UrlEncodedFormEntity(parameters);
				post.setEntity(entity);
				HttpResponse response = client.execute(post);
				if (response.getStatusLine().getStatusCode() == 200) {
					InputStream is = response.getEntity().getContent();
					byte[] buffer = Utils.convertInputStream2ByteArray(is);
					String json = new String(buffer);// json字符串
					Gson g = new Gson();
					List<Collection> listnew = g.fromJson(json,
							new TypeToken<List<Collection>>() {
							}.getType());
					return listnew;
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<Collection> result) {
			if (result != null) {
				tvtips.setText("我的收藏");
				data.addAll(result);
				adapter.setData(data);
				if (currentpage == 1) {
					lv.setAdapter(adapter);
				}
				adapter.notifyDataSetChanged();
				currentpage++;
				dialog.dismiss();
			} else {
				dialog.setTitle("温馨提示");
				dialog.setMessage("没有数据了");
				dialog.dismiss();

			}
		}
	}
}
