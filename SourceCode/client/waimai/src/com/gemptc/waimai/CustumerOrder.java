package com.gemptc.waimai;

import net.tsz.afinal.FinalBitmap;

import com.gemptc.waimai.R.layout;
import com.gemptc.waimai.util.Utils;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class CustumerOrder extends Activity implements OnClickListener {
	private EditText et_seat;
	private EditText et_time;
	private EditText et_uphone;
	private Button btn_submitseat;
	private int isbj;
	private RadioGroup rg;
	private String seat;
	private String time;
	private String uphone;
	private int rid;
	private FinalBitmap fb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.order);

		Button btnreturn = (Button) findViewById(R.id.btn_back);
		btnreturn.setOnClickListener(this);
		Intent intent = getIntent();
		rid = intent.getIntExtra("RID", -1);
		String rname = intent.getStringExtra("RNAME");
		Long telephone = intent.getLongExtra("TELEPHONE", -2);
		String rsite = intent.getStringExtra("RSITE");
		TextView tv_rname = (TextView) findViewById(R.id.textView4);
		TextView tv_telephone = (TextView) findViewById(R.id.textView5);
		TextView tv_rsite = (TextView) findViewById(R.id.textView6);
		rg = (RadioGroup) findViewById(R.id.radioGroup1);
		et_seat = (EditText) findViewById(R.id.et_seat);
		et_time = (EditText) findViewById(R.id.et_time);
		et_uphone = (EditText) findViewById(R.id.et_uphone);
		btn_submitseat = (Button) findViewById(R.id.btn_submitseat);
		ImageView imageView = (ImageView) findViewById(R.id.iv_seatimg);
		fb = FinalBitmap.create(this);
		btn_submitseat.setOnClickListener(this);
		tv_rname.setText(rname.toString());
		tv_rsite.setText("地址：" + rsite.toString());
		tv_telephone.setText("电话：" + telephone.toString());
		String sname=getIntent().getStringExtra("sname");
		String url = "http://10.203.1.44:8080/SellerImages/" +sname+".jpg";
//		System.out.println(url);
//		String fileName1 = url.toString().substring(
//				url.toString().lastIndexOf("/") + 1);
//		System.out.println(fileName1);
//		String URL1 = "http://10.203.1.44:8080/SellerImages/" + fileName1;
//		System.out.println(URL1);
		fb.display(imageView, url);
		rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId == R.id.radio0) {
					isbj = 0;
				} else if (checkedId == R.id.radio1) {
					isbj = 1;
				}

			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		seat = et_seat.getText().toString();
		time = et_time.getText().toString();
		uphone = et_uphone.getText().toString();
		if (id == R.id.btn_submitseat) {
			if ("".equals(seat)) {
				Toast.makeText(this, "请输入您要预订的座位数！", Toast.LENGTH_LONG).show();
				return;
			}
			if ("".equals(time)) {
				Toast.makeText(this, "请输入您预计多久以后到达！", Toast.LENGTH_LONG).show();
				return;
			}
			if ("".equals(uphone) || !Utils.isPhone(uphone)) {
				Toast.makeText(this, "请输入正确的联系电话！", Toast.LENGTH_LONG).show();
				return;
			} else {
				Utils.uploadDataByHttpClientPost(this, rid, seat, isbj, time,
						uphone);
			}
		} else if (id == R.id.btn_back) {
			startActivity(new Intent(this, ShowTypeRestaurantActivity.class));
		}
	}
}
