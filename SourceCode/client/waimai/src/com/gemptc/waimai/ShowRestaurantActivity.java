package com.gemptc.waimai;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.gemptc.entity.Restaurant;
import com.gemptc.waimai.util.JListView.OnRefreshListener;
import com.gemptc.waimai.util.NetUtil;
import com.gemptc.waimai.util.RefreshableView;
import com.gemptc.waimai.util.RefreshableView.PullToRefreshListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.tsz.afinal.FinalBitmap;

import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterViewFlipper;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ShowRestaurantActivity extends Activity implements OnRefreshListener,OnClickListener {
	private EditText searchEt;//定义搜索框
	private ProgressDialog dialog;
	private ListView lv;
	// private ArrayAdapter<String> adapter;
	private RestaurantAdapter adapter;
	private FinalBitmap fb;
	private List<Restaurant> data=new ArrayList<Restaurant>();
	private int currentpage=1;//当前页
	private boolean is_divpage;//是否需要分页
	public static final String Path="http://10.203.1.44:8080/waimai/RestaurantByPageServlet?currentPage=";
	public static final String Path1="http://10.203.1.44:8080/waimai/RestaurantByPageServlet?currentPage=1";

	public List<Restaurant> getData() {
		return data;
	}

	public void setData(List<Restaurant> data) {
		this.data = data;
	}
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去标题
		setContentView(R.layout.activity_showrestaurant);
		
		//初始化控件
		searchEt = (EditText) findViewById(R.id.searchEt);
		Button btnselect=(Button) findViewById(R.id.btn_reselect);
		btnselect.setOnClickListener(this);
		lv = (ListView) findViewById(R.id.listView);
		dialog=new ProgressDialog(this);
		dialog.setTitle("温馨提示");
		dialog.setMessage("Loading..."); 
		fb = FinalBitmap.create(this);
		adapter = new RestaurantAdapter();
		new MyTask().execute(Path+currentpage);
		Button btnreturn=(Button) findViewById(R.id.btn_back);
		btnreturn.setOnClickListener(this);
		//监听滚动条
		lv.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				//判断是否需要分页或者滚动条是否到了最下面
				if(is_divpage && scrollState==OnScrollListener.SCROLL_STATE_IDLE){
					new MyTask().execute(Path+currentpage);
				}
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				//判断出现在屏幕内的第一行加上最后一行是否等于屏幕能显示的总行数
				is_divpage=(firstVisibleItem+visibleItemCount==totalItemCount);
			}
		});
			
		
		
		
		//下拉刷新
		 final RefreshableView refreshableView=(RefreshableView)findViewById(R.id.refresh);
		 refreshableView.setOnRefreshListener(new PullToRefreshListener() {
		
		 @Override
		 public void onRefresh() {
		 // TODO Auto-generated method stub
//		 menuAdapter = new MenuAdapter();
//		 NetUtil.getMenuDataFromServer(menuListView,menuAdapter,RESTAURANTID);
//		 refreshableView.finishRefreshing();
//			 Toast.makeText(ShowRestaurantActivity.this, "dasds", Toast.LENGTH_SHORT).show();
			 adapter=new RestaurantAdapter();
			 new MyTask().execute(Path1);
			 refreshableView.finishRefreshing();
		 }
		 }, 0);
		 
		
		
				
		 //监听listview条目
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Restaurant restaurant = (Restaurant) adapter.getItem(position);
				Intent intent=new Intent();
				intent.putExtra("RID", String.valueOf(restaurant.getRid()));
				intent.putExtra("RNAME", restaurant.getRname());
				intent.putExtra("TELEPHONE", String.valueOf(restaurant.getTelephone()));
				intent.putExtra("RSITE", restaurant.getRsite());
				intent.putExtra("PIC", restaurant.getJpgurl());
				intent.putExtra("RINFO", restaurant.getInfo());
				intent.putExtra("sname", restaurant.getSname());
				intent.setClass(ShowRestaurantActivity.this, ShowFoodActivity.class);
				startActivity(intent);				
			}
		});
		//输入时进行监听
		searchEt.addTextChangedListener(new TextWatcher() {
			// 当输入框的文字改变时，执行以下方法。
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				//输入框内的字符不为空，执行匹配过滤操作，为空时显示所有的数据
				if (!"".equals(s)) {
					List<Restaurant> newData = GetRestaurant(s);
					adapter.setData(newData);
					adapter.notifyDataSetChanged();
				} else {
					adapter.setData(data);
					adapter.notifyDataSetChanged();
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.layout.activity_showrestaurant, menu);
		return true;
	}
	
	//创建一个新的线程
	class MyTask extends AsyncTask<String, Void, List<Restaurant>>{
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
//			dialog.show();
		}
		@Override
		protected List<Restaurant> doInBackground(String... params) {
			//请求数据
			HttpClient client=new DefaultHttpClient();
			HttpGet get=new HttpGet(Path+currentpage);
			try {
				HttpResponse response=client.execute(get);
				if(response.getStatusLine().getStatusCode()==200){		
					InputStream is=response.getEntity().getContent();
					byte[] data=convertInputStream2ByteArray(is);
					String json=new String(data);//json字符串
					Gson g=new Gson();
					List<Restaurant> listnew=g.fromJson(json, new TypeToken<List<Restaurant>>(){}.getType());
					return listnew;
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(List<Restaurant> result) {
			if(result!=null){
//				activity.setData(result);	
//				activity.getData().addAll(result);
//				data.addAll(result);
//				adapter.setData(data);
//				lv.setAdapter(adapter);
//				adapter.notifyDataSetChanged();
//				adapter=new RestaurantAdapter();
				data.addAll(result);
				adapter.setData(data);
				if(currentpage==1){
					lv.setAdapter(adapter);
				}
				adapter.notifyDataSetChanged();
				currentpage++;
				dialog.dismiss();
			}else{
				dialog.setTitle("提示");
				dialog.setMessage("没有数据了");		
				dialog.dismiss();				
			}
		}
	}
	
	//读取字符
	public static byte[] convertInputStream2ByteArray(InputStream is) {
		byte[] buffer = new byte[1024];
		int len;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			while ((len = is.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}


	
	//自定义一个新的Adapter
	public class RestaurantAdapter extends BaseAdapter {

		private List<Restaurant> adapterData;

		public List<Restaurant> getData() {
			return adapterData;
		}

		public void setData(List<Restaurant> data) {
			this.adapterData = data;
		}

		public RestaurantAdapter() {
			super();
		}

		public RestaurantAdapter(List<Restaurant> data) {
			this.adapterData = data;
		}

		// 说明Listview有多少个条目
		@Override
		public int getCount() {
			return adapterData.size();
		}

		// 说明position指定的条目关联的数据对象
		@Override
		public Object getItem(int position) {
			return adapterData.get(position);
		}

		// 条目的ID
		@Override
		public long getItemId(int position) {
			return position;
		}

		// 说明每个条目的布局
		// convertView:缓存的条目
		// parent:Listview
		// 返回值作为Listview的一个条目
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Restaurant Restaurant = adapterData.get(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(ShowRestaurantActivity.this);
				convertView = inflater.inflate(
						R.layout.activity_listitem_restaurant, null);
			}
			TextView tvTitle = (TextView) convertView
					.findViewById(R.id.tv_resname);
			TextView tvDesc = (TextView) convertView
					.findViewById(R.id.tv_resinfo);
			TextView tvContent = (TextView) convertView
					.findViewById(R.id.textView3);
			ImageView ivPic = (ImageView) convertView
					.findViewById(R.id.imageView1);
			// 设置控件的数据
			tvTitle.setText("店名："+Restaurant.getRname());
			tvDesc.setText("简介："+Restaurant.getInfo());
			tvContent.setText("手机号码：" + Restaurant.getTelephone());
//			String URL="http://10.203.1.44:8080/waimai/pic.jpg";
//			String URL = Restaurant.getJpgurl();
			String URL="http://10.203.1.44:8080/SellerImages/"+Restaurant.getSname()+".jpg";
			fb.display(ivPic, URL);// 根据URL设置图片
			// ivPic.setImageResource(Restaurant.getResId());
			return convertView;
		}

	}

	public List<Restaurant> GetRestaurant(CharSequence s) {
		List<Restaurant> newRestaurant = new ArrayList<Restaurant>();
		for (Restaurant ns : data) {
			if (ns.getRname().contains(s)) {
				newRestaurant.add(ns);
			}
		}
		return newRestaurant;
	}

	@Override
	public void onDownPullRefresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLoadingMore() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v) {
		int id=v.getId();
		if(id==R.id.btn_reselect){
			startActivity(new Intent(this,selectlocation.class));
		}else if(id==R.id.btn_back){
				startActivity(new Intent(this,MainActivity.class));		
		}
	}
}
