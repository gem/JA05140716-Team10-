package fragment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.tsz.afinal.FinalBitmap;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.R.integer;
import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler.Callback;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Service;

import com.gemptc.entity.Food;
import com.gemptc.waimai.R;
import com.gemptc.waimai.R.id;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ShopCar_Fragment extends Fragment{

	private NewsAdapter adapter;
	private TextView tvShowMessage; 
	private Callback mCallback;  
	private String rid;
	private String rname;
	private String rsite;
	private String rphone;
	private TextView tv_restaurantmessage;
	private FinalBitmap fb;
	private String sname;
	private String url1;
	
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}

	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		 super.onAttach(activity);
		 try {
	            mCallback = (Callback) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implements Callback");
	        }
	}

	public interface Callback{
		public void getObject(Food food);
	}
	
	@Override
	public void onStart() {
		super.onStart();	
		System.out.println("rid="+rid);
		ListView lv = (ListView) getView().findViewById(R.id.lv_foodlist);
	    tvShowMessage = (TextView) getView().findViewById(R.id.tv_showmessage);
	    tv_restaurantmessage=(TextView) getView().findViewById(R.id.tv_restaurantmessage);
	    tv_restaurantmessage.setText(rname);
		adapter = new NewsAdapter();
		ImageView iv_restaurantphoto=(ImageView) getView().findViewById(R.id.iv_restaurantphoto);
		fb=FinalBitmap.create(getActivity());
		String url = "http://10.203.1.44:8080/SellerImages/"+sname+".jpg";
//		String url = url1.replaceAll("\\", "/");
		System.out.println(url+"!!!!!!1111111111111111");
		fb.display(iv_restaurantphoto, url);
		getDataFromServlet(rid,lv,adapter);
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Food food = (Food) parent.getItemAtPosition(position);
				mCallback.getObject(food);
			}
		  });
		tvShowMessage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String ss1= "餐厅地址："+rsite;
				String ss2= "平均速度：50分钟";
				String ss3= "营业时间：";
				String ss4= "起送价格：40元";
				String ss5= "人均价格：20元";
				String ss6= "餐厅电话："+rphone;
				String ss7= "备注信息：";
				String []s = {ss1,ss2,ss3,ss4,ss5,ss6,ss7};
				new AlertDialog.Builder(getActivity())  
				.setTitle("     	  			   "+rname)  
				.setItems(s, null)  
				.setNegativeButton("确定", null)  
				.show(); 
			}
		});
    }
	
	//根据餐厅rid查询出该餐厅的菜单列表
	public static void getDataFromServlet(final String rid,final ListView lv,final NewsAdapter adapter) {
		AsyncTask<Void, Void, List<Food>> at = new AsyncTask<Void, Void, List<Food>>(){
            
			@Override
			protected List<Food> doInBackground(Void... params) {
				try {
					//创建浏览器
					HttpClient client = new DefaultHttpClient();
					//说明发送的请求类型
					HttpPost post = new HttpPost("http://10.203.1.44:8080/waimai/ShowFoodServlet");
					//设置发送的数据
					List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
					reqParams.add(new BasicNameValuePair("rid",rid));
					//表单
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(reqParams);
					//设置发送的实体
					post.setEntity(entity);
					//发送请求到服务器
					HttpResponse response = client.execute(post);
					if(response.getStatusLine().getStatusCode() == 200){
						InputStream is = response.getEntity().getContent();
						byte []data = convertInputStream2ByteArray(is);
					    String json = new String(data);
					    Gson g = new Gson();
					    List<Food> listFood = g.fromJson(json, new TypeToken<List<Food>>(){}.getType());
					    return listFood;
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			public byte[] convertInputStream2ByteArray(InputStream is){
				byte[] buffer = new byte[1024];
				int len;
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				try {
					while((len=is.read(buffer))>0){
						bos.write(buffer, 0, len);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}finally{
					if(is != null){
						try {
							is.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				return bos.toByteArray();
			}

			
			@Override
			protected void onPostExecute(List<Food> result) {
				if(result != null){
					adapter.setFood(result);
		        	lv.setAdapter(adapter);
		        	for(Food f:result){
		        		System.out.println(f.getFname());
		        	}
				}
			}  
		};
		at.execute();
	}
	
	    //自定义Adapter
		public class NewsAdapter extends BaseAdapter{
	          private List<Food> food;
	          
	          public List<Food> getFood() {
				return food;
			}

			public void setFood(List<Food> food) {
				this.food = food;
			}

			public NewsAdapter(List<Food> food){
	        	  this.food = food;
	          }
	        
	          public NewsAdapter(){
	        	  
	          }   
	          
	       //说明ListView有多少条目
			@Override
			public int getCount() {
				return food.size();
			}

			//说明position指定的条目关联的数据对象
			@Override
			public Object getItem(int position) {
				return food.get(position);
			}

			//条目的id
			@Override
			public long getItemId(int position) {
				return position;
			}

			//说明每个条目的布局
			//convertView：缓存的条目
			//parent：ListView
			//返回值作为ListView一个条目
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				final Food f = food.get(position);
				if(convertView == null){
					LayoutInflater inflater = LayoutInflater.from(getActivity());
				    convertView = inflater.inflate(R.layout.listview_item_food, null);
				}
				TextView tvFname = (TextView)convertView.findViewById(R.id.tv_fname);
				TextView tvFprice = (TextView)convertView.findViewById(R.id.tv_fprice);
				final TextView tvCheck = (TextView) convertView.findViewById(R.id.tv_check);
				final ImageView ivdelete = (ImageView) convertView.findViewById(R.id.iv_delete);
				tvFname.setText(f.getFname());
				
				if(f.getNumber() ==0 ){
				  tvCheck.setText("");
				  ivdelete.setVisibility(View.INVISIBLE);
				}else{
				  tvCheck.setText(String.valueOf(f.getNumber()));
				  ivdelete.setVisibility(View.VISIBLE);
				}
				tvFprice.setText(f.getFprice()+"元/份");
//				tvCheck.setOnClickListener(new OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						f.setNumber(f.getNumber() + 1);
//						tvCheck.setBackgroundResource(R.drawable.unchecked_32);
//						notifyDataSetChanged();	
//					 //   Intent intent = new Intent(getActivity(),ShopCarActivity.class);
//				  }
//	        
//				});
//				
//				ivdelete.setOnClickListener(new OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						if(f.getNumber()>1){
//						f.setNumber(f.getNumber() - 1);
//						//tvCheck.setBackgroundResource(R.drawable.unchecked_32);
//						notifyDataSetChanged();	
//						}else if(f.getNumber() == 1){
//							f.setNumber(f.getNumber() - 1);
//							System.out.println(f.getNumber());
//							tvCheck.setBackgroundResource(R.drawable.checkbox_48);
//							ivdelete.setVisibility(View.INVISIBLE);
//							notifyDataSetChanged();	
//						}
//				    }
//				});
				tvCheck.setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						f.setNumber(f.getNumber() + 1);
						tvCheck.setBackgroundResource(R.drawable.unchecked_32);
						notifyDataSetChanged();	
						return false;
					}
				});
				
				ivdelete.setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(f.getNumber()>1){
							f.setNumber(f.getNumber() - 1);
							notifyDataSetChanged();	
							}else if(f.getNumber() == 1){
								f.setNumber(f.getNumber() - 1);
								tvCheck.setBackgroundResource(R.drawable.checkbox_48);
								ivdelete.setVisibility(View.INVISIBLE);
								notifyDataSetChanged();	
							}
						return false;
					}
				});
				return convertView;
			}
			

		}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
				View view = inflater.inflate(R.layout.shopcarfragment, container, false);
				Bundle bundle = getArguments();
				rid = bundle.getString("rid");
				rname = bundle.getString("rname");
				rsite = bundle.getString("rsite");
				rphone = bundle.getString("rphone");
				sname=bundle.getString("sname");
				url1=bundle.getString("pic");
				return view;
	}

}
