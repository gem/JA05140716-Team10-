package comment;


import com.gemptc.waimai.R;
import com.gemptc.waimai.ShowFoodActivity;
import com.gemptc.waimai.ShowRestaurantActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WriteCommentActivity extends Activity implements OnClickListener{

	private EditText inputcomment;
	private Button btn;
	private String rid;
	private String usephone;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_writecomment);
		
		rid = getIntent().getStringExtra("rid");
//		SharedPreferences sp=getSharedPreferences("login", MODE_PRIVATE);
//		usephone=sp.getString("phone", "");
		SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
		usephone = sp.getString("phone", "");
		
		inputcomment = (EditText) findViewById(R.id.et_writecomment);
		btn = (Button)findViewById(R.id.btn_writecomment);
		btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		String comment = inputcomment.getText().toString();
		if(comment.equals("")){
			Toast.makeText(this, "���۲���Ϊ��", Toast.LENGTH_SHORT).show();
		}else{
		 NetUtil.uploadDataByHttpClientPost(this,usephone,rid,comment);
		 Intent intent = new Intent(this,ShowRestaurantActivity.class);
		 startActivity(intent);
		}
	}
   
}
