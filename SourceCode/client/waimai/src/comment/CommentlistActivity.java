package comment;

import java.util.List;

import com.gemptc.entity.Content;
import com.gemptc.waimai.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class CommentlistActivity extends Activity {
    private TextView tvPoint;
	private NewsAdapter adapter;
    private String rid;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_commentlist);
		
		rid = getIntent().getStringExtra("rid");//得到该餐厅的id
		System.out.println("rid="+rid);
		ListView lv = (ListView) findViewById(R.id.lv_CommentList);
		tvPoint = (TextView) findViewById(R.id.tv_CommentPoint);
		adapter = new NewsAdapter();
		NetUtil.getDataFromServer(rid,lv,adapter,tvPoint);
	}
	
	 //自定义Adapter
	class NewsAdapter extends BaseAdapter{
          private List<Content> content;
          
          public List<Content> getContent() {
			return content;
		}

		public void setContent(List<Content> content) {
			this.content = content;
		}

		public NewsAdapter(List<Content> content){
        	  this.content = content;
          }
        
          public NewsAdapter(){
        	  
          }
          
       //说明ListView有多少条目
		@Override
		public int getCount() {
			return content.size();
		}

		//说明position指定的条目关联的数据对象
		@Override
		public Object getItem(int position) {
			return content.get(position);
		}

		//条目的id
		@Override
		public long getItemId(int position) {
			return position;
		}

		//说明每个条目的布局
		//convertView：缓存的条目
		//parent：ListView
		//返回值作为ListView一个条目
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Content con = content.get(position);
			if(convertView == null){
				LayoutInflater inflater = LayoutInflater.from(CommentlistActivity.this);
			    convertView = inflater.inflate(R.layout.listview_item_comments, null);
			}
			TextView tvContent = (TextView)convertView.findViewById(R.id.tv_content);
			TextView tvUsername = (TextView)convertView.findViewById(R.id.tv_name);
			TextView tvDate = (TextView)convertView.findViewById(R.id.tv_date);
			
			tvContent.setText(con.getCinfo());			
			tvUsername.setText("@"+con.getUphone()+",");
			tvDate.setText(con.getDate());
			return convertView;
		}
	}
	
	public void lauch(View v){
		Intent intent = new Intent(this,WriteCommentActivity.class);
		intent.putExtra("rid", rid);
		startActivity(intent);
	}
}
