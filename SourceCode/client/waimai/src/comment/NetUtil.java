package comment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.gemptc.entity.Content;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import comment.CommentlistActivity.NewsAdapter;


import android.R.integer;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class NetUtil {
	
	//得到评论列表数据
	public static void getDataFromServer(final String rid,final ListView lv,final NewsAdapter adapter,final TextView tvPoint) {
		AsyncTask<Void, Void, List<Content>> at = new AsyncTask<Void, Void, List<Content>>(){
            

			@Override
			protected List<Content> doInBackground(Void... params) {
				try {
					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost("http://10.203.1.44:8080/waimai/ShowCommentServlet");
					List<NameValuePair> reqParams = new ArrayList<NameValuePair>(); 
					reqParams.add(new BasicNameValuePair("rid",rid));
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(reqParams);
					post.setEntity(entity);
					HttpResponse response = client.execute(post);
					if(response.getStatusLine().getStatusCode() == 200){
						InputStream is = response.getEntity().getContent();
						byte []data = convertInputStream2ByteArray(is);
					    String json = new String(data);
					    Gson g = new Gson();
					    List<Content> listContents = g.fromJson(json, new TypeToken<List<Content>>(){}.getType());
					    return listContents;
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<Content> result) {
				super.onPostExecute(result);
					if(result != null ){
					tvPoint.setText("看大家的评价");
					adapter.setContent(result);
		        	lv.setAdapter(adapter);
					}
			    }
		};
		at.execute();
	}
	
	//上传评论到评论列表
	public static void uploadDataByHttpClientPost(
			final WriteCommentActivity writeCommentActivity,final String usephone,final String rid,final String comment) {
		AsyncTask<Void, Void, String> at = new AsyncTask<Void, Void, String>(){

			@Override
			protected String doInBackground(Void... params) {
				//1.创建浏览器
				HttpClient client = new DefaultHttpClient();
				try {
					//3.说明发送的请求类型
					HttpPost post = new HttpPost("http://10.203.1.44:8080/waimai/WriteCommentServlet");
					List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
					//6.设置发送的数据
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
					String date = df.format(new Date());// new Date()为获取当前系统时间
					//Log.i("NetUtil", "date="+date);
					reqParams.add(new BasicNameValuePair("usephone",usephone));
					reqParams.add(new BasicNameValuePair("rid",rid));
					reqParams.add(new BasicNameValuePair("cinfo", URLEncoder.encode(comment,"utf-8")));//post编码
					reqParams.add(new BasicNameValuePair("date",date));
					//5.表单
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(reqParams);
					//4.设置发送的实体
					post.setEntity(entity);
					//2.发送请求到服务器
					HttpResponse response = client.execute(post);
					if(response.getStatusLine().getStatusCode() == 200){
						InputStream is = response.getEntity().getContent();
						byte []data = convertInputStream2ByteArray(is);
						String back = new String(data);
						return back;
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if(result != null){
					if("true".equals(result)){
						Toast.makeText(writeCommentActivity, "提交成功", Toast.LENGTH_SHORT).show();
					}else if("false".equals(result)){
						Toast.makeText(writeCommentActivity, "提交失败", Toast.LENGTH_SHORT).show();
					}
				}
			}
			
		};
		at.execute();
	}
	
	public static byte[] convertInputStream2ByteArray(InputStream is){
		byte[] buffer = new byte[1024];
		int len;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			while((len=is.read(buffer))>0){
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}

}
