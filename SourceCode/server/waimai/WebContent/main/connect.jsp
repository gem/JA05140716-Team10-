<%@ page pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>联系我们</title>
<link href="connect.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="first">
   <p>你好，欢迎来到点点外卖!</p>
 </div>
<div id="container">
 <div id="title">
    <span>点点外卖</span><select name="city" id="cityid">
    <option selected="selected">苏州</option>
    <option>上海</option>
    <option>杭州</option>
    <option>南京</option>
    <option>北京</option>
    <option>天津</option>
    <option>无锡</option>
    <option>常州</option>
    <option>重庆</option>
    <option>广州</option>
    </select>
  </div>
  <hr />
  <div class="conn">
   <p>我们的想法或许很单纯，希望和各位多多沟通。</p>
   <p>这是我们的联系方式：QQ：123456789</p>
  </div>
  <hr />
  <div id="last">
  <p><a href="about.html">关于我们</a>
  <a href="#">帮助中心</a>
  <a href="#">意见反馈</a>
  <a href="connect.html">联系我们</a>
  <a href="#">法律声明</a></p>
 </div> 
 </div>
</body>
</html>
