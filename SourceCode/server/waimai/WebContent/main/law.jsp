<%@ page pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>法律声明</title>
<link href="law.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="first">
   <p>你好，欢迎来到点点外卖!</p>
   <p><a href="main.jsp">主页</a></p>
 </div>
<div id="container">
 <div id="title">
    <span>点点外卖</span><select name="city" id="cityid">
    <option selected="selected">苏州</option>
    <option>上海</option>
    <option>杭州</option>
    <option>南京</option>
    <option>北京</option>
    <option>天津</option>
    <option>无锡</option>
    <option>常州</option>
    <option>重庆</option>
    <option>广州</option>
    </select>
  </div>
  <hr />
  <div class="law">
    <p>法律声明</p>
  </div>
  <div class="pro">
   <p class="p1">点点外卖提醒您：在使用点点外卖平台各项服务前，请您务必仔细阅读并透彻理解本声明。您可以选择不使用点点外卖平台服务，</p>
   <p class="p2">但如果您使用点点外卖平台服务的，您的使用行为将被视为对本声明全部内容的认可。“点点外卖平台”指由高博安卓班G4班外卖</p>
   <p class="p2">小组运营的网络交易平台。</p>
  </div>
  <hr />
  <div id="last">
  <p><a href="about.html">关于我们</a>
  <a href="help.html">帮助中心</a>
  <a href="advise.html">意见反馈</a>
  <a href="connect.html">联系我们</a>
  <a href="law.html">法律声明</a></p>
 </div> 
 </div>
</body>
</html>
