<%@ page pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>关于我们</title>
<link href="about.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="first">
   <p>你好，欢迎来到点点外卖!</p>
 </div>
<div id="container">
 <div id="title">
    <span>点点外卖</span><select name="city" id="cityid">
    <option selected="selected">苏州</option>
    <option>上海</option>
    <option>杭州</option>
    <option>南京</option>
    <option>北京</option>
    <option>天津</option>
    <option>无锡</option>
    <option>常州</option>
    <option>重庆</option>
    <option>广州</option>
    </select>
  </div>
  <hr />
  <div class="abo">
   <p>点点外卖是我们小组的第一个项目，在完成这个项目中，我们接触了很多，也感受了很多。</p>
   <p>这是我们的起点，但绝不是终点。希望以后越做越好。</p>
  </div>
  <hr />
  <div id="last">
  <p><a href="about.html">关于我们</a>
  <a href="help.html">帮助中心</a>
  <a href="advise.html">意见反馈</a>
  <a href="connect.html">联系我们</a>
  <a href="law.html">法律声明</a></p>
 </div> 
 </div>
</body>
</html>
