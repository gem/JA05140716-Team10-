<%@ page pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>点点外卖</title>
<link href="main.css" rel="stylesheet" type="text/css">
</head>
  
<body>
 <div class="first">
   <p>你好，欢迎来到点点外卖!</p>
 </div>
<div id="container">
 <div id="title">
    <span>点点外卖</span><select name="city" id="cityid">
    <option selected="selected">苏州</option>
    <option>上海</option>
    <option>杭州</option>
    <option>南京</option>
    <option>北京</option>
    <option>天津</option>
    <option>无锡</option>
    <option>常州</option>
    <option>重庆</option>
    <option>广州</option>
    </select>
 </div>
 <hr />
 <div id="login"> 
   <a href="/waimai/user/UserUnloginShow.jsp"><img src="image/buy.jpg" /></a>
   <a href="/waimai/Sellerlogin.jsp"><img src="image/sell.jpg" /></a>
   <a href="/waimai/addseller/login.jsp"><img src="image/seller.jpg" /></a>
 </div>
 <hr />
 <div id="download">
  <div id="left"> 
   <p>方法一：扫描二维码下载</p>
   <a href="#"><img src="image/erwei.jpg"/></a>
  </div>
  <div id="right">
   <p>方法二：电脑直接下载</p><br /><br />
   <span><a href="#" target="_blank"><img src="image/android.PNG" /></a></span>
  </div>
 </div>
  <hr />
 <div>
 <div id="restaurant">
  <p class="res">品牌餐厅</p>
  <ul>
				        <li>
                            <a href="#" target="_blank">
                                <img src="image/restaurant_03.png" alt="巴贝拉">
                            </a>
                            <p><a href="#" target="_blank" >巴贝拉</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/restaurant_09.png" alt="鲜芋仙">
                            </a>
                            <p><a href="#" target="_blank" >鲜芋仙</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/restaurant_15.png" alt="星巴克">
                            </a>
                            <p><a href="#" target="_blank" >星巴克</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/subway.png" alt="赛百味">
                            </a>
                           <p><a href="#" target="_blank" >赛百味</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/restaurant_07.png" alt="味千拉面">
                            </a>
                          <p><a href="#" target="_blank" >味千拉面</a></p>
                        </li>
                    </ul>
 </div>
 </div>
  <hr />
 <div id="waimai">
  <p class="wm">品牌外卖</p>
  <ul>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/wm-04.jpg" alt="肯德基">
                            </a>
                            <p><a href="#" target="_blank" >肯德基</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/wm-03.png" alt="麦当劳">
                            </a>
                            <p><a href="#" target="_blank" >麦当劳</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/wm-01.png" alt="海底捞火锅">
                            </a>
                            <p><a href="#" target="_blank" >海底捞火锅</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/wm-02.jpg" alt="必胜客">
                            </a>
                           <p><a href="#" target="_blank" >必胜客</a></p>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <img src="image/restaurant_07.png" alt="味千拉面">
                            </a>
                          <p><a href="#" target="_blank" >味千拉面</a></p>
                        </li>
	</ul> 
 </div>
  <hr />
 <div id="last">
  <p><a href="about.jsp">关于我们</a>
  <a href="help.jsp">帮助中心</a>
  <a href="advise.jsp">意见反馈</a>
  <a href="connect.jsp">联系我们</a>
  <a href="law.jsp">法律声明</a></p>
 </div> 
</div>
</body>
</html>
