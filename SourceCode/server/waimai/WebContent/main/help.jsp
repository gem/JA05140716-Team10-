<%@ page pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>帮助中心</title>
<link href="help.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="first">
   <p>你好，欢迎来到点点外卖!</p>
 </div>
<div id="container">
 <div id="title">
    <span>点点外卖</span><select name="city" id="cityid">
    <option selected="selected">苏州</option>
    <option>上海</option>
    <option>杭州</option>
    <option>南京</option>
    <option>北京</option>
    <option>天津</option>
    <option>无锡</option>
    <option>常州</option>
    <option>重庆</option>
    <option>广州</option>
    </select>
  </div>
  <hr />
  <div class="h1">
    <p>使用帮助</p>
  </div>
  <hr />
  <div class="gro">
   <p class="p1">服务使用步骤：</p>
   <p class="p2">1. 请输入您的地址</p>
   <p class="p2">2. 选择您喜爱的餐厅</p>
   <p class="p2">3. 选择您喜爱的菜式下单，并输入确认姓名、联系电话、送餐地址以及备注等，完成订餐。</p>
  </div>
  <hr />
  <div id="last">
  <p><a href="about.html">关于我们</a>
  <a href="help.html">帮助中心</a>
  <a href="#">意见反馈</a>
  <a href="connect.html">联系我们</a>
  <a href="#">法律声明</a></p>
 </div> 
 </div>
</body>
</html>
