<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="js/jquery.js"></script>
<title>点点外卖</title>
<script>
	$(document)
			.ready(
					function() {
						var arrayarea = [
								[ "请选择地区" ],
								[ "观前街道", "平江路街道", "苏锦街道", "娄门街道", "城北街道",
										"桃花坞" ],
								[ "唯亭街道", "斜塘街道", "娄葑街道", "胜浦街道", "独墅湖科教创新区" ],
								[ "公园街道", "府前街道", "南门街道", "吴门桥街道", "葑门街道",
										"双塔街", "友新街道", "胥江街道" ],
								[ "长桥镇", "胥口镇", "木渎镇", "横泾镇", "浦庄镇", "渡村镇",
										"东山镇", "西山镇", "藏书镇", "光福镇", "镇湖镇",
										"东渚镇", "角直镇", "车坊镇", "郭巷镇" ],
								[ "太平街道", "北桥街道", "元和街道 ", "黄桥街道" ],
								[ "石路街道", "彩香街道", "留园街道", "虎丘街道", "白洋湾街道" ],
								[ "横塘街道", "狮山街道", "枫桥街道", "镇湖街道" ] ];

						$("[name='areaname']").change(
								function() {
									var index = $(this).val()
									var area = arrayarea[index]
									$("[name='placename']").empty();
									for (var i = 0; i < area.length; i++) {
										var place = ("<option value='"+i+"'>"
												+ area[i] + "</option>")
										$("[name='placename']").append(place);
									}

								})
						$("[name='regstsubmit']").submit(function() {
							var address = $("[name='phone']").val();
							if (address == "") {
								alert("请输入电话号码");
								$("[name='address']").focus();
								return false;
							}

							var merchant = $("[name='merchant']").val();
							if (merchant == "") {
								alert("请输入商家名称");
								$("[name='merchant']").focus();
								return false;
							}

							var address = $("[name='address']").val();
							if (address == "") {
								alert("请输入公司地址");
								$("[name='address']").focus();
								return false;
							}

							var info = $("[name='info']").val();
							if (info == "") {
								alert("请输入公司简介");
								$("[name='info']").focus();
								return false;
							}
							return true;
						})
					})
</script>
</head>
<style>
.J_ServiceItemsBox a {
	font-size: 12px;
}

* {
	margin: 0;
	padding: 0;
	list-style: none;
}

#J_MUIMallbar {
	display: none;
}

#header {
	height: 65px;
	background-color: #3366ff !important;
	font-family: "微软雅黑";
}

.hcolor {
	color: #0C5580
}

.header-wrap {
	width: 990px;
	margin: 0 auto;
	height: 65px;
}

.header-tab {
	float: left;
}

.header-tab ul {
	margin-left: 50px;
}

.header-tab ul li {
	float: left;
	line-height: 65px;
	font-size: 16px;
	padding: 0 17px;
	font-weight: 500;
}

.header-tab ul li a {
	color: #FFF;
	text-decoration: none;
	font-weight: bold;
}

.header-tab ul li a:hover {
	text-decoration: none;
}

.header-tab ul .selected {
	background: #0c5580;
	height: 65px;
}

.header-tab ul .selected a {
	color: #FFF;
	text-decoration: none;
}

.login {
	float: right;
	font-size: 14px;
	line-height: 65px;
	color: #FFF;
}

.login span {
	margin: 0 15px;
}

.login a {
	margin: 0 15px;
	color: #FFF;
}

table.pos {
	position: absolute;
	top: 130px;
	left: 300px;
	width: 500px;
	height: 520px;
}
</style>
<body>
	<h1 align="center" class="hcolor">欢迎入驻点点外卖</h1>
	<div id="header">
		<div class="header-wrap">
			<div class="header-tab">
				<ul style="list-style-type: none">
					<li class="selected"><a>首页</a></li>
					<li><a href="#">登录后台</a></li>
					<li><a href="#">点点掌柜下载</a></li>
					<li><a href="#" target="_blank">帮助中心</a></li>
				</ul>
			</div>
			<div id="J_Login" class="login"></div>
		</div>
	</div>
	<form
		action="${pageContext.request.contextPath }/SellerAddSuccessServlet"
		method="post" id="regstudent" name="regstsubmit"
		enctype="multipart/form-data">
		<table align="center" width="500px" border="1px" cellpadding="6px"
			cellspacing="0px" style="border-color: #999" class="pos">
			<tr>
				<th>你的账号</th>
				<td><input type="text" name="sellname" /></td>
			</tr>
			<tr>
				<th>你的密码</th>
				<td><input type="text" name="sellpwd" /></td>
			</tr>

			<tr>
				<th>您的电话</th>
				<td><input type="text" class="input-txt" id="mobile"
					name="phone" maxlength="255" /></td>
			</tr>
			<tr>
				<th>email</th>
				<td><input type="text" class="input-txt" id="email"
					name="email" maxlength="255" /></td>
			</tr>
			<tr>
				<th>城市</th>
				<td><select id="areaId" name="areaname">
						<option value="0">--请选择市区--</option>
						<option value="1">平江区</option>
						<option value="2">工业园区</option>
						<option value="3">沧浪区</option>
						<option value="4">吴中区</option>
						<option value="5">金阊区</option>
						<option value="6">相城区</option>
						<option value="7">虎丘区</option>
				</select> <select id="placeId" name="placename">
						<option value="0">--请选择地区--</option>
				</select></td>
			</tr>
			<tr>
				<th>商家名称</th>
				<td><input type="text" class="input-txt" id="merchant"
					name="rname" maxlength="255" /></td>
			</tr>
			<tr>
				<th>类型</th>
				<td><select id="typeid" name="typename">
						<option value="请选择类型">--请选择类型--</option>
						<option value="小吃部">小吃部</option>
						<option value="小餐馆">小餐馆</option>
						<option value="大酒店">大酒店</option>
				</select>（请按实际情况选择，造假必究！）</td>
			</tr>
			
			
			<tr>
				<th>餐厅地址</th>
				<td><input type="text" class="input-txt" id="address"
					name="address" maxlength="255" /></td>
			</tr>
			<tr>
				<th>餐厅简介</th>
				<td><textarea cols="35" rows="10" name="info"></textarea></td>
			</tr>
			<th>餐厅照片</th>
			<td><input type="file" name="myfile"></td>
			</tr>
			<tr align="center">
				<td colspan="2"><input type="submit" value="确认提交" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
					type="button" value="返回"
					onclick="window.location.href='${pageContext.request.contextPath}/addseller/login.jsp';" /></td>
			</tr>
		</table>
	</form>


</body>
</html>
