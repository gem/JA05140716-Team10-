<%@ page contentType="text/html; charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>修改菜单</title>
<link href="addlist.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="first">
   <p>你好，欢迎来到点点外卖!</p>
 </div>
<div id="container">
 <div id="title">
    <span>点点外卖</span><select name="city" id="cityid">
    <option selected="selected">苏州</option>
    <option>上海</option>
    <option>杭州</option>
    <option>南京</option>
    <option>北京</option>
    <option>天津</option>
    <option>无锡</option>
    <option>常州</option>
    <option>重庆</option>
    <option>广州</option>
    </select>
 </div>
 <hr />
 <div>
 <form action="${pageContext.request.contextPath}/UpdateSuccessServlet" method="post" target="_top">
  <table border="1px" align="center" width="350px" cellpadding="10px" >
   <caption>修改菜单</caption>
   <tr align="center">
   <input type="hidden" name="id" value="${food.fid}"/>
   <input type="hidden" name="rid" value="${food.rid}"/>
     <td>菜&nbsp;&nbsp;&nbsp;&nbsp;名：<input type="text" name="fname" id="fnam" value="${food.fname}"/></td>
   </tr>
   <tr align="center">
     <td>菜的价格：<input type="text" name="fprice" id="fpri" value="${food.fprice}"/></td>
   </tr>
   <tr align="center">
    <td>菜的种类：<input type="text" name="ftype" id="ftyp" placeholder="例如：热菜，冷菜" value="${food.ftype}"/></td>
   </tr>
   <tr align="center">
     <td><input type="submit" name="submit" id="sbt" value="确定"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <input type="button" name="button" id="btn" value="取消" onclick="window.open('${pageContext.request.contextPath}/FanhuiServlet','_top');"/></td>
   </tr>
  </table>
 </form>
 </div>
  <hr />
 <div id="last">
  <p><a href="about.jsp">关于我们</a>
  <a href="help.jsp">帮助中心</a>
  <a href="advise.jsp">意见反馈</a>
  <a href="connect.jsp">联系我们</a>
  <a href="law.jsp">法律声明</a></p>
 </div> 
 </div>
</body>
</html>
