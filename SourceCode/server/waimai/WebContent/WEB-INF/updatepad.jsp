<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改密码</title>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.11.0.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#reg").submit(function(){
                    var password=$("[name='password']").val();
                    if(password==""){
                        alert("密码不能为空！");
                        $("[name='password']").focus();
                        return false;
                    }
                    var repassword=$("[name='repassword']").val();
                    if(repassword==""){
                        alert("确认密码不能为空！");
                        $("[name='repassword']").focus();
                        return false;
                    }
             
                  if(repassword!=password){
                    	 alert("确认密码输入有误，请重新输入！");
                         $("[name='repassword']").focus();
                         return false;
                    }
            	
                return true;
            });
        });

    </script>
</head>
<body>
<table width="100%" border="0">
<tr>
<td align="right"><a href="${pageContext.request.contextPath }/login.jsp">登陆</a></td>
</tr>
</table>
<hr color="#00FF66"  />
<table align="center" width="100%" border="0">
<tr>
<td align="center" valign="middle" height="600px">
<form action="${pageContext.request.contextPath }/UserUpdatePad" method="post" id="reg">
<table align="center"> 
<tr>
<td align="center">新密码：</td>
<td><input type="password" name="password" />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
</tr>
<tr>
<td align="center">确认密码：</td>
<td><input type="password" name="repassword" />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
</tr>
<tr>
<input type="hidden"  name="id" value="${id }" />
<td colspan="2px" align="center"><input type="submit" value="提交" /></td>
</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
<hr color="#00FF66"  />
</td>
</tr>
</table>
<p align="center">&copy; 2014 <a href="http://fs.gem-inno.com/">Gemptc.com</a>  版权所有</p>
</body>
</html>