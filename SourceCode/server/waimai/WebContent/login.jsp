<%@ page contentType="text/html; charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户登录界面</title>
</head>
<title>登陆</title>
<style type="text/css">
    <!--
    .table1 {
        border: 1px solid #CCCCCC;
    }
    .font {
        font-size: 12px;
        text-decoration: none;
        color: #999999;
        line-height: 20px;


    }
    .input {
        font-size: 12px;
        color: #999999;
        text-decoration: none;
        border: 0px none #999999;


    }

    td {
        font-size: 12px;
        color: #007AB5;
    }
    form {
        margin: 1px;
        padding: 1px;
    }
    input {
        border: 0px;
        height: 26px;
        color: #007AB5;

    .unnamed1 {
        border: thin none #FFFFFF;
    }
    .unnamed1 {
        border: thin none #FFFFFF;
    }
    select {
        border: 1px solid #cccccc;
        height: 18px;
        color: #666666;

    .unnamed1 {
        border: thin none #FFFFFF;
    }
    body {
        background-repeat: no-repeat;
        background-color: #9CDCF9;
        background-position: 0px 0px;

    }
    .tablelinenotop {
        border-top: 0px solid #CCCCCC;
        border-right: 1px solid #CCCCCC;
        border-bottom: 0px solid #CCCCCC;
        border-left: 1px solid #CCCCCC;
    }
    .tablelinenotopdown {

        border-top: 1px solid #eeeeee;
        border-right: 1px solid #eeeeee;
        border-bottom: 1px solid #eeeeee;
        border-left: 1px solid #eeeeee;
    }
    .style6 {FONT-SIZE: 9pt; color: #7b8ac3; }

    -->
</style>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.11.0.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
    	var cookvalue=document.cookie;
    	if(cookvalue.length > 0){
    		var usernames=[];
        	var usernames=cookvalue.split("; ");
        	for(var i=0;i<=usernames.length;i++){
        	var temp = [];	
        	var username = [];
        	temp=usernames[i].split("=");
        	var cookieKey = temp[0];
        	temp[1] = temp[1].replace(/\"/g,"");
        	username = temp[1].split(":");
        	if(cookieKey == "login"){
        	   	var usernamephone=username[0];
            	var usernamepassword=username[1];
            	$("#uname").val(usernamephone);
            	$("#pwd").val(usernamepassword);
        		}
        	}
    	}

        $("#regform").submit(function(){
        	var phone=$("[name='phone']").val();
            if(phone==""){
                alert("用户名不能为空！");
                $("[name='phone']").focus();
                return false;
            }
       	 var flag=phone.match(/^[1]([3][0-9]{1}|[5][0-9]{1}|[8][0-9]{1}|[4][7])[0-9]{8}$/);
            if(!flag){
                alert("手机号格式不正确,请从新输入！");
                $("[name='phone']").focus();
                return false;
            }
            var password=$("[name='password']").val();
            if(password==""){
                alert("请输入密码");
                $("[name='password']").focus();
                return false;
            }
            var rand=$("[name='yzm']").val();
            if(rand==""){
                alert("请输入验证码");
                $("[name='rand']").focus();
                return false;
            }
            return true;
        });

    });
</script>
</head>
<body>
<table width="681" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:120px">
    <tr>
        <td width="353" height="259" align="center" valign="bottom" background="Images/login_1.gif"><table width="90%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td align="right" valign="bottom" style="color:#05B8E4">Power by <a href="http://www.gemptc.com" target="_blank">gemptc</a> 高博 2014</td>
            </tr>
        </table></td>
        <td width="195" background="Images/login_2.gif"><table width="190" height="106" border="0" align="center" cellpadding="2" cellspacing="0">
            <form action="${pageContext.request.contextPath }/UserLoginServlet" method="post" id="regform">
                <tr>
                    <td height="50" colspan="2" align="left">&nbsp;</td>
                </tr>
                <tr>
                    <td width="60" height="30" align="center">手机号:</td>
                    <td><input type="text" name="phone"  style="background:url(Images/login_6.gif) repeat-x; border:solid 1px #27B3FE; height:20px; background-color:#FFFFFF"  size="14"   id="uname" ></td>
                </tr>
                <tr>
                    <td height="30" align="center">密码:</td>
                    <td><input name="password"  id="pwd" type="password" style="background:url(Images/login_6.gif) repeat-x; border:solid 1px #27B3FE; height:20px; background-color:#FFFFFF" size="14"><a href="${pageContext.request.contextPath }/user/forgetpossword.jsp"><br/>忘记密码？</a></td>
                </tr>
                <tr>
               
                   <td align="right"><input type="checkbox" name="checkphoneandpwd" value="1" id="checked"  style="background:url(Images/login_6.gif) repeat-x; border:solid 1px #27B3FE; height:20px; background-color:#FFFFFF"  size="14"  ></input></td><td align="left">记住用户名与密码</td>
                   
                </tr>

                <tr>
                    <td colspan="2" align="center"><input type="submit" style="background:url(Images/login_5.gif) no-repeat" value=" 登  陆 ">&nbsp;&nbsp;还没有账号？<a href="${pageContext.request.contextPath }/user/register.jsp">注册</a></td>

                <tr>
                    <td height="5" colspan="2"></td>
            </form>
        </table></td>
        <td width="133" background="Images/login_3.gif">&nbsp;</td>
    </tr>
    <tr>
        <td height="161" colspan="3" background="Images/login_4.gif"></td>
    </tr>
</table>
<p align="center">&copy; 2014 <a href="http://fs.gem-inno.com/">Gemptc.com</a>  版权所有</p>
</body>
</html>