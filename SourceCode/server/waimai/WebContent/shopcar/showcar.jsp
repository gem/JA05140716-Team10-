<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.gemptc.waimai.entity.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>购物车</title>
<style type="text/css">
body{
	margin:0px;
}
.first{
	margin:0px;
	padding:2px;
	background-color:#CCC;
	}
 .first p{
	padding:1px 0px 1px 175px;
	font-size:12px;
	}
#container{
		position:relative;
		margin:0px auto 0px auto;
		width:1000px;
		padding-bottom:175px;
		text-align:left;
		background-color:#f8f1f1;
		}
#title{
	width:1000px;
	margin:0px 0px 0px 0px;
	padding-top:15px;
	padding-bottom:15px;
	text-align:left;
	background-color:#FFF;
	}
#title span{
	margin-top:auto;
	margin-bottom:auto;
	font-family:"Arial Black", Gadget, sans-serif;
	color:#F00;
	font-size:28px;
	}	
.p2{
	float:right;
	padding-right:460px;
	font-size:22px;
}
.d{
	margin:0px;
	padding-top:70px;
	}
.total{
	padding-right:50px;	
}
a:link,a:visited{ 
text-decoration:none;  /*超链接划线*/
}
a:hover{ 
	text-decoration:underline;  /*鼠标放划线*/
}
</style>
</head>

<body>
 <div class="first">
   <p>你好，欢迎来到点点外卖!</p>
 </div>
<div id="container">
 <div id="title">
    <span>点点外卖</span>
    <p class="p2">我的购物车</p>
 </div>
<div class="d">
<%--获取存储在session中用来存储用户已购买商品的buylist集合对象 --%>
<% ArrayList buylist=(ArrayList)session.getAttribute("buylist");
float total=0;
%>
<form action="${pageContext.request.contextPath}/OrderListSevlet" method="post">
<table border="1" width="500" rules="none" cellspacing="0" cellpadding="0" align="center">
<tr height="60"><td colspan="5" align="center">购买的商品如下</td></tr>
<tr align="center" height="40" bgcolor="lightgrey">
<td width="25%">名称</td>
<td>价格</td>
<td>数量</td>
<td>总价</td>
<td>移除</td>
</tr>
<% if(buylist==null||buylist.size()==0){ %>
<tr height="100"><td colspan="5" align="center">您的购物车为空</td></tr>
<%}else{
	for(int i=0;i<buylist.size();i++){
	Shoppingcar food=(Shoppingcar)buylist.get(i);
	String fname=food.getFname();//获取商品的名称
	Double price= food.getFprice();//获取商品的价格
	int num = food.getNumber();//获取购买数量
	//计算当前商品的总价，并进行四舍五入
	double money =price*num;
	total +=money;
	%>
	<tr align="center" height="50">
	<td><%=fname %></td>
	<td><%=price %></td>
	<td><%=num %></td>
	<td><%=money %></td>
	<td><a href="DoCarServlet?action=remove&id=<%=food.getFid() %>">移除</a></td>
	</tr>
	<% 
   }
  }
 %>
<tr height="50" align="right" ><td colspan="5" class="total">总计:<%=total%></td></tr>
  <tr> 
   <td colspan="1" align="center"><a href="/waimai/seller/showmenu.jsp" >继续购物</a></td>
   <td colspan="2" align="center"><a href="DoCarServlet?action=clear" id="clear">清空购物车</a></td>
   <td colspan="2" align="center"><input type="submit" value="提交订单" id="sbt"/></td>
  </tr>
</table>
</form>
</div>
</div>
</body>
</html>