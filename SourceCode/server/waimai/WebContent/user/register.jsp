<%@page import="java.util.UUID"%>
<%@ page contentType="text/html; charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>注册</title>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.11.0.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#reg").submit(function(){
            	 var phone=$("[name='phone']").val();
                 if(phone==""){
                     alert("手机号不能为空！");
                     $("[name='phone']").focus();
                     return false;
                 }
            	 var flag=phone.match(/^[1]([3][0-9]{1}|[5][0-9]{1}|[8][0-9]{1}|[4][7])[0-9]{8}$/);
                 if(!flag){
                     alert("手机号格式不正确,请从新输入！");
                     $("[name='phone']").focus();
                     return false;
                 }
            
                    var password=$("[name='password']").val();
                    if(password==""){
                        alert("密码不能为空！");
                        $("[name='password']").focus();
                        return false;
                    }
                    var repassword=$("[name='repassword']").val();
                    if(repassword==""){
                        alert("确认密码不能为空！");
                        $("[name='repassword']").focus();
                        return false;
                    }
             
                  if(repassword!=password){
                    	 alert("确认密码输入有误，请重新输入！");
                         $("[name='repassword']").focus();
                         return false;
                    }
            	var name=$("[name='username']").val();
                if(name==""){
                    alert("昵称不能为空！");
                    $("[name='username']").focus();
                    return false;
                }
                var answer=$("[name='answer']").val();
                if(answer==""){
                    alert("请选择其中一个问题输入答案或者选择其他随便填入，以方便找回密码！");
                    $("[name='answer']").focus();
                    return false;
                }
                return true;
            });
        });

    </script>
</head>

<body>
<%
   String id=UUID.randomUUID().toString();
   request.getSession().setAttribute("id", id);
%>
<table width="100%" border="0">
<tr>
<td align="left">账号注册</td>
<td align="right">已有账号？<a href="${pageContext.request.contextPath }/login.jsp">登陆</a></td>
</tr>
</table>
<hr color="#00FF66"  />
<table align="center" width="100%" border="0">
<tr>
<td align="center" valign="middle" height="600px">
<form action="${pageContext.request.contextPath }/RegisterServlet" method="post" id="reg">
<table align="center"> 
<tr>
<td align="center">手机号：</td>
<td><input type="text" name="phone" />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
</tr>
<tr>
<td align="center">密码：</td>
<td><input type="password" name="password" />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
</tr>
<tr>
<td align="center">确认密码：</td>
<td><input type="password" name="repassword" />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
</tr>
    <tr>
        <td align="center">昵称：</td>
<td><input type="text" name="username"  />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
    </tr>
<tr>
<td align="center">问题：</td>
<td>
<select name="problem">
        <option value="0">----请选择问题并回答以方便找回密码----</option>
        <option value="1" >您的生日</option>
        <option value="2" >您的爱好</option>
        <option value="3" >您的姓名</option>
        <option value="4" >其它（随意输入但不能为空）</option>
    </select>
</td>
</tr>
  <tr>
        <td align="center">答案：</td>
        <td><input type="text" name="answer" />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
    </tr>
<tr>
<input type="hidden"  name="id" value="${id }" />
<td colspan="2px" align="center"><input type="submit" value="注册" /></td>
</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
<hr color="#00FF66"  />
</td>
</tr>
</table>
<p align="center">&copy; 2014 <a href="http://fs.gem-inno.com/">Gemptc.com</a>  版权所有</p>
</body>
</html>
