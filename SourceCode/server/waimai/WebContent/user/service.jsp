<%@page import="java.util.UUID"%>
<%@ page contentType="text/html; charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>联系客服</title>
 <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.11.0.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#texts").submit(function(){
            var message=$("[name='message']").val();
            if(message.trim()==""){
                alert("请输入您宝贵的意见，我们将会做得更好！");
                $("[name='message']").focus();
                return false;
            }
            return true;
        });

    });
    </script>
</head>

<body>
<%
String id=UUID.randomUUID().toString();
request.getSession().setAttribute("id", id);
%>
<table width="100%" border="0">
<tr>
<td align="left">欢迎您：${up.name }</td>

</tr>
</table>
<hr color="#00FF66"  />
<table width="100%" align="center" >
<tr>
<td align="center" valign="middle" height="600px">
<form action="${pageContext.request.contextPath }/UserAdviceServlet?name=${up.name }" method="post" id="texts">
<table border="0" width="80%">
<tr>
<td colspan="4px" bgcolor="#FF9900">反馈通道</td>
</tr>
<tr>
<td >客服电话：18238828006</td>
<td>客服微信：1578971354</td>
</tr>
<tr>
<td colspan="2px">客服QQ：1578971354 &nbsp;&nbsp; <input type="button" value="QQ交谈" style="background:red"  onclick=window.location.href="http://wpa.qq.com/msgrd?v=3&uin=1578971354&site=qq&menu=yes"></td>
</tr>
<tr>
<td colspan="4px" bgcolor="#FF9900">反馈留言</td>
</tr>
<tr>
  <td colspan="2px"><textarea cols="150px" rows="8px" name="message"></textarea></td>
</tr>
<tr>
<input type="hidden"  name="id" value="${id }" />
<td colspan="2px" align="right"><input type="submit" value="提交" /></td>
</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
<hr color="#00FF66"  />
</td>
</tr>
</table>
<p align="center">&copy; 2014 <a href="http://fs.gem-inno.com/">Gemptc.com</a>  版权所有</p>
            
</body>
</html>
