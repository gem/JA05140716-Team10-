<%@ page contentType="text/html; charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>忘记密码</title>
 <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.11.0.js"></script>
    <script type="text/javascript">
   
    $(document).ready(function(){
    	 $("#getpad").submit(function(){
        	 var phone=$("[name='phone']").val();
             if(phone==""){
                 alert("手机号不能为空！");
                 $("[name='phone']").focus();
                 return false;
             }
        	 var flag=phone.match(/^[1]([3][0-9]{1}|[5][0-9]{1}|[8][0-9]{1}|[4][7])[0-9]{8}$/);
             if(!flag){
                 alert("手机号格式不正确,请从新输入！");
                 $("[name='phone']").focus();
                 return false;
             }
             var answer=$("[name='answer']").val();
             if(answer==""){
                 alert("请输入注册时的问题与答案，以方便找回密码！");
                 $("[name='answer']").focus();
                 return false;
             }
             return true;
         });
    });
</script>
</head>

<body>
<table width="100%" border="0">
<tr>
<td align="left">找回密码</td>
<td align="right"><a href="${pageContext.request.contextPath }/user/register.jsp">重新注册</a></td>
</tr>
</table>
<hr color="#00FF66"  />
<table align="center" width="100%" border="0">
<tr>
<td align="center" valign="middle" height="600px">
<form action="${pageContext.request.contextPath }/FindPasswordServlet" method="post" id="getpad">
<table align="center"> 

<tr>
        <td align="center">手机号：</td>
        <td><input type="text" name="phone"  />&nbsp;&nbsp;<span style="color:#FF0000">*</span>
       
    </tr>
<tr>
<td align="center">问题：</td>
<td>
<select name="problem">
        <option value="0">----请选择注册时的问题及答案----</option>
        <option value="1" >您的生日</option>
        <option value="2" >您的爱好</option>
        <option value="3" >您的姓名</option>
        <option value="4" >其它（注册时的输入值）</option>
    </select>
</td>
</tr>
  <tr>
        <td align="center">答案：</td>
        <td><input type="text" name="answer" />&nbsp;&nbsp;<span style="color:#FF0000">*</span></td>
    </tr>

<tr>
<td colspan="2px" align="center"><input type="submit" value="提交" /></td>
</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
<hr color="#00FF66"  />
</td>
</tr>
</table>
<p align="center">&copy; 2014 <a href="http://fs.gem-inno.com/">Gemptc.com</a>  版权所有</p>
</body>
</html>
