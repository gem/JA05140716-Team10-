<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>预订位置</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style.css" type="text/css" media="all" />
</head>

<body>

	<div id="container">
		<div class="shell">


			<br />
			<!-- Main -->
			<div id="main">
				<div class="cl">&nbsp;</div>

				<!-- Content -->
				<div id="content">

					<!-- Box -->
					<div class="box">
						<!-- Box Head -->
						<div class="box-head">
							<h2 class="left">订座详情</h2>
							<div class="right">
								<input type="button" value="刷新"
									onclick="window.location.href='${pageContext.request.contextPath}/ShowSeat';" />

								<!--  	<input type="button" value="增加菜单" onclick="window.location.href='${pageContext.request.contextPath}/addfood/addlist.jsp'; "  /> -->
							</div>
						</div>
						<!-- End Box Head -->

						<!-- Table -->
						<div class="table">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>

									<th align="center">座位（个）</th>
									<th align="center">多久到达（分钟）</th>
									<th align="center">是否要包间</th>
									<th align="center">联系方式</th>
									<th width="110" class="ac">操作</th>
								</tr>
								<c:choose>
									<c:when test="${empty seatinfo }">
										<tr>
											<td colspan="5" align="center">对不起，还没有人预订位置</td>
										</tr>

									</c:when>
									<c:otherwise>
										<c:forEach items="${seatinfo }" var="s">
											<tr>
												<td align="center">${s.seatnum }</td>
												<td align="center">${s.arrivetime }</td>
												<td align="center">${s.booleanbj }</td>
												<td align="center">${s.uphoneLong}</td>
												<td align="center"><a
													href="${pageContext.request.contextPath}/DeleteSeatServlet?seatid=${s.seatid}"
													target="_top">删除</a>
													<td><input type="hidden" name="seatid"
														value="${s.seatid }" /></td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</table>
						</div>
</body>
</html>
