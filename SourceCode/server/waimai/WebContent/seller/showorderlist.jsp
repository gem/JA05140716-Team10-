<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="style.css" type="text/css" media="all" />
</head>

<body>

	<div id="container">
	<div class="shell">
		
		
		<br />
		<!-- Main -->
		<div id="main">
			<div class="cl">&nbsp;</div>
			
			<!-- Content -->
			<div id="content">
				
				<!-- Box -->
				<div class="box">
					<!-- Box Head -->
					<div class="box-head">
						<h2 class="left">订单列表</h2>
						<div class="right">
						<input type="button" value="刷新" onclick="window.location.href='${pageContext.request.contextPath}/ShowOrderList';" />
							<!--   <input type="submit" class="button" value="刷新" />-->
						</div>
					</div>
					<!-- End Box Head -->	

					<!-- Table -->
					<div class="table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<th align="center">订单ID</th>
								<th align="center">时间</th>
								<th align="center">餐厅ID</th>
                                <th align="center" width="120">菜单列表</th>
                                <th align="center">收货人</th>
                                 <th align="center">地址</th>
                                <th align="center">联系方式</th>
								<th align="center">总价</th>
								<th align="center">交易详情</th>
							</tr>
							
							<c:choose>
<c:when test="${empty order }">
<tr>
<td colspan="7" align="center">对不起，暂时还没有订单^-^</td>
</tr>

</c:when>
<c:otherwise>
<c:forEach items="${order }" var="o">
<tr>
<td align="center" name="oid">${o.oid}</td>
<td align="center" id="otime">${o.otime }</td>
<td align="center" id="oid">${o.rid}</td>
<td align="center" id="foodinfo">${o.foodinfo }</td>
<td align="center" id="bname">${o.bname}</td>
<td align="center" id="osite">${o.osite}</td>
<td align="center" id="ophone">${o.ophone}</td>
<td align="center" id="totalprice">${o.totalprice}</td>
<td align="center" id="state">${o.state}</td>
<td align="center"><a href="${pageContext.request.contextPath}/DeleteOrderServlet?oid=${o.oid}" target="_top">删除</a>&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/UpdateOrderList?oid=${o.oid}" target="_top">修改</a>&nbsp;&nbsp;&nbsp;</td>
<td><input type="hidden" name="oid" value="${o.oid }"/></td>

</tr>
</c:forEach>
</c:otherwise>
</c:choose>
							
							
						</table>
</body>
</html>
