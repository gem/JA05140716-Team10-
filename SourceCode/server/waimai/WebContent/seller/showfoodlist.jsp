<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>本店菜单</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
</head>

<body>

	<div id="container">
	<div class="shell">
		
		
		<br />
		<!-- Main -->
		<div id="main">
			<div class="cl">&nbsp;</div>
			
			<!-- Content -->
			<div id="content">
				
				<!-- Box -->
				<div class="box">
					<!-- Box Head -->
					<div class="box-head">
						<h2 class="left">菜单列表</h2>
						<div class="right">
							<input type="button" value="增加菜单" onclick="window.open('${pageContext.request.contextPath}/addfood/addlist.jsp','_top');" />
						<!--  	<input type="button" value="增加菜单" onclick="window.location.href='${pageContext.request.contextPath}/addfood/addlist.jsp'; "  /> -->
							<input type="text" class="field small-field" name="foodname"/><input type="button" value="搜索" onclick="window.open('${pageContext.request.contextPath}/SearchFoodServlet','_top');" />
						</div>
					</div>
					<!-- End Box Head -->	

					<!-- Table -->
					<div class="table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
							
								<th align="center">菜单ID</th>
                                <th align="center">餐厅ID</th>
                                <th align="center">菜名</th>
                                <th align="center">类型</th>
                                <th align="center">价格（元/份）</th>
								<th width="110" class="ac">操作</th>
							</tr>
							<c:choose>
<c:when test="${empty foods }">
<tr>
<td colspan="6" align="center">对不起，您还没有添加菜单^-^</td>
</tr>

</c:when>
<c:otherwise>
<c:forEach items="${foods }" var="f">
<tr>
<td align="center" name="fid">${f.fid }</td>
<td align="center">${f.rid }</td>
<td align="center">${f.fname }</td>
<td align="center">${f.ftype }</td>
<td align="center">${f.fprice }</td>
<td align="center"><a href="${pageContext.request.contextPath}/UpdateFoodServlet?fid=${f.fid}" target="_top">修改</a>&nbsp;&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/DeleteFoodServlet?fid=${f.fid}" target="_top">删除</a></td>
<td><input type="hidden" name="fid" value="${f.fid }"/></td>

</tr>
</c:forEach>
</c:otherwise>
</c:choose>
						</table>
           </div>
</body>
</html>
