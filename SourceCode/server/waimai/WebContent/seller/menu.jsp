<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>菜单</title>
</head>

<body>
<table border="0" width="70%">
<tr>
<td align="left">欢迎您：${sellname}&nbsp;&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/LoginOutServlet">注销</a></td>
</tr>
</table>
<form action="" method="post">
<table border="0" width="70%">
<tr>
<td align="center" >
<input type="button" value="增加菜单" onclick="window.location.href='${pageContext.request.contextPath}/addfood/addlist.jsp';" />&nbsp;&nbsp;&nbsp;
<input type="button" value="查看订单" />&nbsp;&nbsp;&nbsp;
<input type="text" name="menu" /><a href="">搜索</a>

</td>
</tr>
</table>
</form>
<br />
<hr color="#00FF66"  width="90%" />
<br />
<br />

<table width="90%" align="center" border="1px" cellpadding="0" cellspacing="0">
<tr>
<th align="center">菜单ID</th>
<th align="center">餐厅ID</th>
<th align="center">菜名</th>
<th align="center">类型</th>
<th align="center">价格（元/份）</th>
<th align="center">操作</th>
</tr>

<c:choose>
<c:when test="${empty foods }">
<tr>
<td colspan="6" align="center">对不起，您还没有添加菜单^-^</td>
</tr>

</c:when>
<c:otherwise>
<c:forEach items="${foods }" var="f">
<tr>
<td align="center" name="fid">${f.fid }</td>
<td align="center">${f.rid }</td>
<td align="center">${f.fname }</td>
<td align="center">${f.ftype }</td>
<td align="center">${f.fprice }</td>
<td align="center"><a href="${pageContext.request.contextPath}/UpdateFoodServlet?fid=${f.fid}">修改</a>&nbsp;&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/DeleteFoodServlet?fid=${f.fid}">删除</a></td>
<td><input type="hidden" name="fid" value="${f.fid }"/></td>

</tr>
</c:forEach>
</c:otherwise>
</c:choose>
</table>
</body>
</html>
