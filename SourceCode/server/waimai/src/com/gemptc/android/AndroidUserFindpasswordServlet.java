package com.gemptc.android;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.ShowPwd;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;

/**
 * Servlet implementation class AndroidUserFindpasswordServlet
 */
@WebServlet("/AndroidUserFindpasswordServlet")
public class AndroidUserFindpasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String phone=request.getParameter("phone");
		String pid=request.getParameter("pid");
		String answer=request.getParameter("answer");
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		UserPeople user=us.isRegister(Long.valueOf(phone));
		UserPeople up=us.getUserPwd(Long.valueOf(phone), Integer.valueOf(pid)+1, answer);
		if(user!=null){
			if(up!=null){
				String pwd=up.getPassword();
				ShowPwd sp=new ShowPwd("true", pwd);
				Gson g=new Gson();
				String json=g.toJson(sp);
				response.getWriter().print(json);
			}else{
				ShowPwd sp=new ShowPwd("false","null");
				Gson g=new Gson();
				String json=g.toJson(sp);
				response.getWriter().print(json);
			}
		}else{
			ShowPwd sp=new ShowPwd("no","null");
			Gson g=new Gson();
			String json=g.toJson(sp);
			response.getWriter().print(json);
		}
	}

}
