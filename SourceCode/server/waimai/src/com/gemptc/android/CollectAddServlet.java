package com.gemptc.android;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.entity.Collect;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.impl.SellerServiceImpl;

/**
 * Servlet implementation class CollectServlet
 */
@WebServlet("/CollectAddServlet")
public class CollectAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain;charset=utf-8");
//		String cinfo = request.getParameter("cinfo");
		String cinfo = URLDecoder.decode(request.getParameter("cinfo"), "utf-8");
		String rname = request.getParameter("rname");
		rname = URLDecoder.decode(rname,"utf-8");
		String usephone = request.getParameter("usephone");
		String jpguri=request.getParameter("jpguri");
		String rsite=request.getParameter("rsite");
		int rid=Integer.valueOf(request.getParameter("rid"));
		String sname=request.getParameter("sname");
//		System.out.println(cinfo+rname+usephone+jpguri+"ccccccccccc");
		long uphone = Long.valueOf(usephone);//用户电话
		long rphone = Long.valueOf(request.getParameter("rphone"));//餐厅电话
		Collection cc = new Collection(jpguri,rname,cinfo,rphone,uphone,rsite,rid,sname);
		SellerService ss = new SellerServiceImpl(new SellerDaoImplMysql());
		ss.addCollect(cc);
	}

}
