package com.gemptc.android;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;


/**
 * Servlet implementation class AndroidUploadFileServlet
 */

@WebServlet("/AndroidUploadFileServlet")
@MultipartConfig
public class AndroidUploadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String phone=request.getParameter("phone");
		Part part = request.getPart("file");
		// String path = getServletContext().getRealPath("/file");
		String path = "D:\\tomcat\\apache-tomcat-7.0.56\\webapps\\SellerImages";
		String path2="http:\\10.203.1.44:8080\\SellerImages\\";
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdir();
		}
		File file = new File(dir, phone + ".jpg");
		part.write(file.getPath());// �����ļ�
		String jpgurl = path2+phone+".jpg";
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		boolean flag=us.addUserHeadPhoto(Long.valueOf(phone), jpgurl);
		if(flag){
			response.getWriter().print("true");
		}
		
	}
 
}
