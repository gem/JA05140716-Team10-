package com.gemptc.android;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AndroidUsersRegisterServlet
 */
@WebServlet("/AndroidUsersRegisterServlet")
public class AndroidUsersRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userphone=request.getParameter("phone");
		String userpassword=request.getParameter("password");
		String username=request.getParameter("name");
		String userpid=request.getParameter("pid");
		String useranswer=request.getParameter("answer");
		String pwd=URLDecoder.decode(userpassword,"utf-8");
		String name=URLDecoder.decode(username,"utf-8");
		String answer=URLDecoder.decode(useranswer,"utf-8");
		UserPeople up=new UserPeople(name, pwd, Long.valueOf(userphone), Integer.valueOf(userpid)+1, answer);
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		UserPeople user=us.isRegister(Long.valueOf(userphone));
		if(user==null){
			boolean flag=us.addUserPeople(up);
			if(flag){
				response.getWriter().print("true");
			}
		}else{
			response.getWriter().print("false");
		}
	}

}
