package com.gemptc.android;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;

/**
 * Servlet implementation class OrderListByphoneAndState
 */
@WebServlet("/OrderListByphoneAndState")
public class OrderListByphoneAndState extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OrderListByphoneAndState() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String currentPage = request.getParameter("currentPage");
		String uphone=request.getParameter("phone");
		int state=Integer.parseInt(request.getParameter("state"));
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		List<OrderList> orderList = us.getOrderlistByPhoneAndstate(Integer.parseInt(currentPage), 10,uphone,state);
		Gson g=new Gson();
		String json=g.toJson(orderList);
		response.getWriter().println(json);
	}

}
