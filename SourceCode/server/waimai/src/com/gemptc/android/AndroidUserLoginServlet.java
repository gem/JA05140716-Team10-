package com.gemptc.android;

import java.io.IOException;


import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserHeadPhoto;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.entity.showHeadPhoto;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;

/**
 * Servlet implementation class AndroidUserLoginServlet
 */
@WebServlet("/AndroidUserLoginServlet")
public class AndroidUserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String phone=request.getParameter("phone");
		String password=request.getParameter("password");
		String pwd=URLDecoder.decode(password,"utf-8");
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		UserPeople user=us.isRegister(Long.valueOf(phone));
		UserHeadPhoto uhp=us.seachUserHeadPhoto(Long.valueOf(phone));
		
		
		UserPeople up=us.getUserByNameAndPassword(Long.valueOf(phone), pwd);
		
		if(user!=null){
			if(up!=null){
				if(uhp!=null&&!uhp.equals("")){
					String path=uhp.getHeadpath();
					showHeadPhoto shp=new showHeadPhoto("true", path);
					Gson g=new Gson();
					String json=g.toJson(shp);
					response.getWriter().print(json);
				}else{
					showHeadPhoto shp=new showHeadPhoto("true", null);
					Gson g=new Gson();
					String json=g.toJson(shp);
					response.getWriter().print(json);
				}
				
			}else{
				showHeadPhoto shp=new showHeadPhoto("false", null);
				Gson g=new Gson();
				String json=g.toJson(shp);
				response.getWriter().print(json);
			}
		}else{
			showHeadPhoto shp=new showHeadPhoto("no", null);
			Gson g=new Gson();
			String json=g.toJson(shp);
			response.getWriter().print(json);
		}
		
	}

}
