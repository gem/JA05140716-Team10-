package com.gemptc.android;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;


/**
 * Servlet implementation class ShakeRestaurantServlet
 */
@WebServlet("/ShakeRestaurantServlet")
public class ShakeRestaurantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		int count=us.searchResturantCount();
		Random r=new Random();
		int id=r.nextInt(count)+1;
		List<Restaurant> rest=us.serchRestaurantsById(id);
		if(rest!=null){
			Gson g=new Gson();
			String json=g.toJson(rest);
			response.getWriter().print(json);
		}
		
	}

}
