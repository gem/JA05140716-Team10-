package com.gemptc.android;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Seat;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AndroidAddSeat
 */
@WebServlet("/AndroidAddSeat")
public class AndroidAddSeat extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int rid=Integer.valueOf(request.getParameter("rid"));
		int seatnum=Integer.valueOf(request.getParameter("seat"));
		int isbj=Integer.valueOf(request.getParameter("isbj"));
		Long uphone=Long.valueOf(request.getParameter("uphone"));
		String time=request.getParameter("time");
		String arrivetime=new String(time.getBytes("iso-8859-1"),"utf-8");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		Seat seat=new Seat(rid,seatnum,arrivetime,isbj,uphone);
		boolean flag=us.addSeat(seat);
		if(flag){
			response.getWriter().print("true");
		}
	}

}
