package com.gemptc.android;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.entity.ShopCar;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.SellerServiceImpl;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Servlet implementation class GetOrderServlet
 */
@WebServlet("/GetOrderServlet")
public class GetOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ArrayList<ShopCar> list = new ArrayList<ShopCar>();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain;charset=utf-8");
		String json = URLDecoder.decode(request.getParameter("gson"),"utf-8");
		Gson g = new Gson();
		Type typeOfT = new TypeToken<List<ShopCar>>(){}.getType();
		list = g.fromJson(json, typeOfT);
		String str="";
		for(int i=0;i<list.size();i++){
			str += list.get(i).getFname()+","+list.get(i).getFprice()+"Ԫ/��"+","+list.get(i).getNumber()+"��"+";";
		}
	    Double sum = Double.valueOf(request.getParameter("sum"));
	    String buyname = URLDecoder.decode(request.getParameter("buyname"),"utf-8");
        long phone = Long.valueOf(request.getParameter("phone"));
	    String address = URLDecoder.decode(request.getParameter("address"),"utf-8");
	    Timestamp date = Timestamp.valueOf(request.getParameter("date"));
	    int rid = Integer.valueOf(request.getParameter("rid"));
	    String usephone =request.getParameter("usephone");
//	    System.out.println(sum+buyname+address+date+rid+usephone+"aaasasdsadasd");
//	    OrderList or = new OrderList(rid,usephone,buyname,str,address,date,phone,sum);
		OrderList orderList=new OrderList(rid, buyname, str, address, date, phone, sum,0,usephone);
//	    SellerService ss = new SellerServiceImpl(new SellerDaoImplMysql());
	    UserService us = new UserServiceImpl(new UserDaoImplMysql());
	    boolean flag = us.addorderlist(orderList);
	    if(flag){
	    	response.getWriter().println("true");
	    }else{
	    	response.getWriter().println("false");
	    }
	    
	}
}









