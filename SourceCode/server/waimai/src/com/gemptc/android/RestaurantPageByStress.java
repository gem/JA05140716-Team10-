package com.gemptc.android;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.entity.RestaurantPage;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;

/**
 * Servlet implementation class RestaurantPageByStress
 */
@WebServlet("/RestaurantPageByStress")
public class RestaurantPageByStress extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String currentPage = request.getParameter("currentPage");
		String stress=request.getParameter("stress");
//		String stressname=URLEncoder.encode(stress);
		String stressname=new String(stress.getBytes("iso-8859-1"),"utf-8");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		RestaurantPage pager = us.searchRestaurantPageByStress(Integer.parseInt(currentPage), 10,stressname);
		List<Restaurant> data=pager.getData();
		Gson g=new Gson();
		String json=g.toJson(data);
		response.getWriter().println(json);
	}

}
