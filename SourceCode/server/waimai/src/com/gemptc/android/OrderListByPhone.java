package com.gemptc.android;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.entity.RestaurantPage;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;

/**
 * Servlet implementation class OrderListByPhone
 */
@WebServlet("/OrderListByPhone")
public class OrderListByPhone extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String currentPage = request.getParameter("currentPage");
		String uphone=request.getParameter("phone");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		List<OrderList> orderList = us.getOrderlistByPhone(Integer.parseInt(currentPage), 10,uphone);
		Gson g=new Gson();
		String json=g.toJson(orderList);
		response.getWriter().println(json);
	}

}
