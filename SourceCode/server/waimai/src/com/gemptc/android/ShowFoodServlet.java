package com.gemptc.android;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.google.gson.Gson;
/**
 * Servlet implementation class ShowFood
 */
@WebServlet("/ShowFoodServlet")
public class ShowFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L; 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain;charset=utf-8");
		Integer rid = Integer.valueOf(request.getParameter("rid"));
		List<Food> food = new ArrayList<Food>();
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		food = us.searchFoodListByRid(rid);
		
//		food.add(new Food(1,1,"炸酱面","面食",15.0));
//		food.add(new Food(2,1,"肉酱面","面食",15.0));
//		food.add(new Food(3,1,"拉面","面食",15.0));
//		food.add(new Food(4,1,"红烧牛肉面","面食",15.0));
		Gson g = new Gson();
		String gson = g.toJson(food);
		response.getWriter().println(gson);
	}

}
