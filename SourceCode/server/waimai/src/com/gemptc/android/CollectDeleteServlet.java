package com.gemptc.android;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.impl.SellerServiceImpl;

/**
 * Servlet implementation class CollectDeleteServlet
 */
@WebServlet("/CollectDeleteServlet")
public class CollectDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain;charset=utf-8");
		long usephone = Long.valueOf(request.getParameter("usephone"));
		int rid = Integer.valueOf((request.getParameter("rid")));
		SellerService ss = new SellerServiceImpl(new SellerDaoImplMysql());
        ss.deleteCollect(usephone, rid);
	}

}
