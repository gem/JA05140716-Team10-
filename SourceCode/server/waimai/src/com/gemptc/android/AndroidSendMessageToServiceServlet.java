package com.gemptc.android;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AndroidSendMessageToServiceServlet
 */
@WebServlet("/AndroidSendMessageToServiceServlet")
public class AndroidSendMessageToServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		int count1=us.searchResturantCount();//第一次查
		int count2=us.searchResturantCount();//第二次查
		if(count2>count1){
			response.getWriter().print("true");
		}
	}

}
