package com.gemptc.android;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.impl.SellerServiceImpl;

/**
 * Servlet implementation class WriteCommentServlets
 */
@WebServlet("/WriteCommentServlet")
public class WriteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain;charset=utf-8");
		long usephone = Long.valueOf(request.getParameter("usephone"));
		Integer rid = Integer.valueOf(request.getParameter("rid"));
		System.out.println("bbbbbbbbbb="+rid);
		String date = request.getParameter("date");
		String cinfo = request.getParameter("cinfo");
		cinfo = URLDecoder.decode(cinfo, "utf-8");
		Content c = new Content(usephone,rid,cinfo,date);
		//初始化对象
		SellerService ss = new SellerServiceImpl(new SellerDaoImplMysql());
		//调用方法
		boolean flag = ss.addCinfo(c);
		response.getWriter().println(flag);
	
	}

}
