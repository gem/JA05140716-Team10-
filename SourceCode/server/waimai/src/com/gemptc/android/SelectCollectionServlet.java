package com.gemptc.android;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.impl.SellerServiceImpl;

/**
 * Servlet implementation class SelectCollectionServlet
 */
@WebServlet("/SelectCollectionServlet")
public class SelectCollectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain;charset=utf-8");
		Integer rid = Integer.valueOf(request.getParameter("rid"));
		long uphone = Long.valueOf(request.getParameter("usephone"));
		String name = URLDecoder.decode(request.getParameter("rname"),"utf-8"); 
		SellerService ss = new SellerServiceImpl(new SellerDaoImplMysql());
		System.out.println(rid+uphone+name);
		String rname = ss.selectcollection(rid, uphone);
		if(name.equals(rname)){
			response.getWriter().println("true");
		}else{
			response.getWriter().println("false");
		}
	}

}
