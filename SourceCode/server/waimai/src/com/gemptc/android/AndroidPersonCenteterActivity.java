package com.gemptc.android;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.CollectionPage;
import com.gemptc.waimai.entity.SearchCollectionPage;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.google.gson.Gson;

/**
 * Servlet implementation class AndroidPersonCenteterActivity
 */
@WebServlet("/AndroidPersonCenteterActivity")
public class AndroidPersonCenteterActivity extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String phone = request.getParameter("phone");
		String currentPage = request.getParameter("currentPage");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		SearchCollectionPage pager = us.searchCollectionByPage(
				Long.valueOf(phone), Integer.parseInt(currentPage), 10);
		if (pager != null) {
			List<Collection> data = pager.getData();
			if (data != null && !data.isEmpty()) {
				Gson g = new Gson();
				String json = g.toJson(data);
				response.getWriter().println(json);
			}
		}

	}

}
