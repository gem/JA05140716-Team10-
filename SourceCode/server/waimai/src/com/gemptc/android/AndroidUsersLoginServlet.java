package com.gemptc.android;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AndroidUsersLoginServlet
 */
@WebServlet("/AndroidUsersLoginServlet")
public class AndroidUsersLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String phone=request.getParameter("phone");
		String password=request.getParameter("password");
		String pwd=URLDecoder.decode(password,"utf-8");
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		UserPeople user=us.isRegister(Long.valueOf(phone));
		UserPeople up=us.getUserByNameAndPassword(Long.valueOf(phone), pwd);
		if(user!=null){
			if(up!=null){
				response.getWriter().print("true");
			}else{
				response.getWriter().print("false");
			}
		}else{
			response.getWriter().print("no");
		}
	}

}
