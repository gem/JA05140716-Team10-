package com.gemptc.android;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.impl.SellerServiceImpl;
import com.google.gson.Gson;

/**
 * Servlet implementation class ShowCommentServlet
 */
@WebServlet("/ShowCommentServlet")
public class ShowCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain;charset=utf-8");
		Integer rid = Integer.valueOf(request.getParameter("rid"));
		System.out.println("ShowCommentServlet:rid="+rid);
		List<Content> comment = new ArrayList<Content>();
		SellerService ss = new SellerServiceImpl(new SellerDaoImplMysql());
		comment = ss.getContentByRid(rid);
		if(comment!=null&&!comment.isEmpty()){
			Gson g = new Gson();
			String gson = g.toJson(comment);
			response.getWriter().println(gson);
		}
		
	
	}
		
	//ת��
	public String encoding(String s,HttpServletRequest request){
			if(s==null) return s;
			if("GET".equals(request.getMethod())){
				try {
					return new String(s.getBytes("iso8859-1"),"utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}else{
				try {
					return URLDecoder.decode(s,"utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			return s;
		}

}
