package com.gemptc.android;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AndroidUserCollectionCountServlet
 */
@WebServlet("/AndroidUserCollectionCountServlet")
public class AndroidUserCollectionCountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String phone=request.getParameter("phone");
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		int count=us.searchCollectionCountByPhone(Long.valueOf(phone));
		if(count!=0){
			response.getWriter().print(String.valueOf(count));
		}else{
			response.getWriter().print(String.valueOf("0"));
		}
		
	}

}
