package com.gemptc.waimai.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Buyer;
import com.gemptc.waimai.entity.RestaurantPage;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class SellerLoginServlet
 */
@WebServlet("/BuyerLoginServlet")
public class BuyerLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String name = request.getParameter("username");
		String password = request.getParameter("password");
		String yzm=request.getParameter("yzm");
		String rand=(String) session.getAttribute("rand");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		Buyer buyer = us.getBuyerByNameAndPassword(name, password);
		System.out.println(name+password);
		if(rand.equalsIgnoreCase(yzm)){
			session.removeAttribute("rand");
		if (buyer == null) {
			PrintWriter pw=response.getWriter();
			pw.print("<script language='javascript'>alert('�˺Ż��������');window.location.href='/waimai/login.jsp';</script>");
//			response.setHeader("refresh", "3;url=" + request.getContextPath()
//					+ "/login.jsp");
//			request.setAttribute("msg", "��¼ʧ�ܣ�3�����ת����¼ҳ��");
//			request.getRequestDispatcher("/fail.jsp")
//					.forward(request, response);
		} else {
			request.getSession().setAttribute("buyer", buyer);
			RestaurantPage pager = us.searchRestaurantByPage(1, 10);
			if (pager != null)
				request.setAttribute("pager", pager);
			request.getRequestDispatcher("/showrestaurant/restaurantlist.jsp").forward(
					request, response);}
		}else{
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}
}
