package com.gemptc.waimai.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String checkphoneandpwd = request.getParameter("checkphoneandpwd");
		HttpSession session = request.getSession();
		String phone = request.getParameter("phone");
		String password = request.getParameter("password");
		UserService uu = new UserServiceImpl(new UserDaoImplMysql());
		UserPeople user = uu.isRegister(Long.valueOf(phone));
		UserPeople up = uu.getUserByNameAndPassword(Long.valueOf(phone),
				password);
		PrintWriter pw = response.getWriter();
		if (user != null) {
			if (up != null) {
				session.setAttribute("up", up);
				if (checkphoneandpwd!=null&&checkphoneandpwd.equals("1")) {
					Cookie c = new Cookie("login", phone + ":" + password);
					c.setMaxAge(7 * 24 * 60 * 60);
					c.setPath(request.getServletContext().getContextPath());
					response.addCookie(c);			
				}

				request.getRequestDispatcher("/user/UserloginShow.jsp").forward(
						request, response);
			} else {
				pw.print("<script language='javascript'>alert('�������');window.location.href='/waimai/login.jsp';</script>");
			}
		} else {
			pw.print("<script language='javascript'>alert('���û���δע�ᣬ��ע�ᣡ');window.location.href='/waimai/login.jsp';</script>");
		}

	}

}

