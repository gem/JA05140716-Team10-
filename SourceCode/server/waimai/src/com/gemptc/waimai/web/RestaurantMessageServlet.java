package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.service.SellerService;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.SellerServiceImpl;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.gemptc.waimai.entity.*;

/**
 * Servlet implementation class RestaurantMessageServlet
 */
@WebServlet("/RestaurantMessageServlet")
public class RestaurantMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String rid = request.getParameter("id");
		int id = Integer.parseInt(rid);
		request.getSession().setAttribute("RID", id);
		//System.out.println(id);
		// int id=1;
		// SellerService ss = new SellerServiceImpl(new SellerDaoImplMysql());
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		List<Food> food = us.searchFoodListByRid(id);
		request.getSession().setAttribute("food", food);
		String[] a = new String[food.size()];
		for (int i = 0; i < food.size(); i++) {
			Food c = food.get(i);
			String cinfo = c.getFtype();
			a[i] = cinfo;
		}

		for (int i = 0; i < food.size(); i++) {
			for (int j = i + 1; j < food.size(); j++) {
				if (a[i].equals(a[j])) {
					a[j] = "";
				}
			}
		}
		List<String> con = new ArrayList<String>();
		for (int i = 0; i < a.length; i++) {
			if (a[i] != "") {
				con.add(a[i]);
			}
		}
		request.getSession().setAttribute("fod", con);
		Restaurant r = us.getRestaurantById(id);
		request.getSession().setAttribute("id", id);
		request.getSession().setAttribute("restaurant", r);
		List<Content> content = us.getContentByRid(id);
		request.getSession().setAttribute("content", content);
		// response.setHeader("refresh",
		// "0;url="+request.getContextPath()+"/seller/showmenu.jsp");
		// request.getRequestDispatcher("/seller/showmenu.jsp").forward(request,
		// response);
		// request.getRequestDispatcher("/seller/list.jsp").forward(request,
		// response);
		request.getRequestDispatcher("/seller/showmain.jsp").forward(request,
				response);

	}
}
