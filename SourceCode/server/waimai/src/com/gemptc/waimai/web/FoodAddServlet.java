package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class FoodAddServlet
 */
@WebServlet("/FoodAddServlet")
public class FoodAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		int id=(int) request.getAttribute("id");
		HttpSession session=request.getSession();
		int id=(int) session.getAttribute("id");
		String fname=request.getParameter("fname");
		String ftype=request.getParameter("ftype");
		Double fprice= Double.parseDouble(request.getParameter("fprice"));
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		Food food =new Food(id,fname,ftype,fprice);
		boolean flag=us.addFood(food);
		if(flag){
			request.getSession().removeAttribute("foods");
			List<Food> foods=us.searchFoodListByRid(id);
			request.getSession().setAttribute("foods", foods);
			request.getRequestDispatcher("/seller/show.jsp").forward(request, response);
//			request.setAttribute("msg", "恭喜你添加成功，2秒后为你转至主界面！");
//			response.setHeader("refresh", " 2;url="+request.getContextPath()+"/seller/menu.jsp");
//			request.getRequestDispatcher("/success.jsp").forward(request, response);
		}else{request.setAttribute("msg", "对不起添加失败，2秒后为你转至主界面！");
			response.setHeader("refresh", " 2;url="+request.getContextPath()+"/menu.jsp");
			request.getRequestDispatcher("/fail.jsp").forward(request, response);
			List<Food> foods=us.searchFoodListByRid(id);
			request.getSession().setAttribute("foods", foods);
			request.setAttribute("msg", "对不起添加失败，2秒后为你转至主界面！");
			response.setHeader("refresh", " 2;url="+request.getContextPath()+"/menu.jsp");
			request.getRequestDispatcher("/fail.jsp").forward(request, response);
		}
	}

}
