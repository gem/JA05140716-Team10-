package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class SellLoginServlet
 */
@WebServlet("/SellLoginServlet")
public class SellLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("sellname");
		String pwd=request.getParameter("sellpwd");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		Restaurant R=us.getSellererByNameAndPassword(name, pwd);
		if(R!=null){
			int id=R.getRid();
//			request.setAttribute("id", id);
			HttpSession session=request.getSession();
			session.setAttribute("id", id);
			request.setAttribute("sellname", name);
			List<Food> foods=us.searchFoodListByRid(id);
			request.getSession().setAttribute("foods", foods);
			request.getRequestDispatcher("/seller/show.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "对不起登录失败，2秒后为你转至登录界面！");
			response.setHeader("refresh", " 2;url="+request.getContextPath()+"/Seller/Login.jsp");
			request.getRequestDispatcher("/fail.jsp").forward(request, response);
		}
	}

}
