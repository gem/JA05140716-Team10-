package com.gemptc.waimai.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.Request;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		PrintWriter pw=response.getWriter();
		String phone=request.getParameter("phone");
		String password=request.getParameter("password");
		String username=request.getParameter("username");
		String pid=request.getParameter("problem");
		String answer=request.getParameter("answer");
		String id=request.getParameter("id");
		String UUid=(String) session.getAttribute("id");
		UserPeople up=new UserPeople(username, password, Long.valueOf(phone), Integer.valueOf(pid), answer);
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		UserPeople user=us.isRegister(Long.valueOf(phone));
		if(user==null){
			boolean flag=us.addUserPeople(up);
			if(id.equals(UUid)){
				session.removeAttribute("id");
				if(flag){
					pw.print("<script language='javascript'>alert('恭喜您注册成功，去登陆');window.location.href='/waimai/login.jsp';</script>");
				}
			}else{
				pw.print("<script language='javascript'>alert('该用户已存在，请不要重复提交！');window.location.href='/waimai/user/register.jsp';</script>");
			}	
		}else{
			pw.print("<script language='javascript'>alert('该手机号已注册！');window.location.href='/waimai/user/register.jsp';</script>");
		}
	
	}
}
