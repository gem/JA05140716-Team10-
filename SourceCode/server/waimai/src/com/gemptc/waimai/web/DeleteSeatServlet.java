package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.Seat;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class DeleteFoodServlet
 */
@WebServlet("/DeleteSeatServlet")
public class DeleteSeatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("seatid"));
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
//		System.out.println(id);
		Seat seat =us.getseatinfoByseatid(id);
//		System.out.println(food.getRid());
		boolean flag=us.deleteseatByseatId(id);
		if(flag){
			request.getSession().removeAttribute("seat");
			List<Seat> seats=us.getseatinfoByRid(seat.getRid());
			request.getSession().setAttribute("seat", seats);
//			for(Food f:foods){
//				System.out.println(f.getFname());
//			}
			request.getRequestDispatcher("/seller/show.jsp").forward(request, response);
//			response.setHeader("refresh", "3;url="+request.getContextPath()+"/seller/menu.jsp");
//			request.setAttribute("msg", "删除成功，3秒后跳转到主页");
//			request.getRequestDispatcher("/success.jsp").forward(request, response);
		}else{
			List<Food> foods=us.searchFoodListByRid(id);
			request.setAttribute("foods", foods);
			response.setHeader("refresh", "3;url="+request.getContextPath()+"/seller/menu.jsp");
			request.setAttribute("msg", "删除失败，3秒后跳转到主页");
			request.getRequestDispatcher("/fail.jsp").forward(request, response);
		}
	}

}
