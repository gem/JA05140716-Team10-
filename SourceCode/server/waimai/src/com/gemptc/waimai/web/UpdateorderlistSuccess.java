package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class UpdateSuccessServlet
 */
@WebServlet("/UpdateorderlistSuccess")
public class UpdateorderlistSuccess extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int oid=Integer.valueOf(request.getParameter("oid"));
		int rid=Integer.valueOf(request.getParameter("rid"));
		String info=request.getParameter("info");
		String uphone=request.getParameter("uphone");
		String bname=request.getParameter("bname");
		String site=request.getParameter("site");
		Double total=Double.valueOf(request.getParameter("total"));
		long phone=Long.valueOf(request.getParameter("phone"));
		int state=Integer.valueOf(request.getParameter("state"));
		OrderList orderList=new OrderList(oid,rid,uphone,bname,info,site,phone,total,state);
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		boolean flag=us.updateorder(orderList);
		if(flag){
//			request.getSession().removeAttribute("foods");
//			List<Food> foods=us.searchFoodListByRid(rid);
//			request.getSession().setAttribute("foods", foods);
			request.getRequestDispatcher("/seller/show.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("/seller/show.jsp").forward(request, response);
		}
	}

}
