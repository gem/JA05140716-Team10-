package com.gemptc.waimai.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class UserUpdatePad
 */
@WebServlet("/UserUpdatePad")
public class UserUpdatePad extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		String phone=(String) request.getSession().getAttribute("phone");
		String pwd=request.getParameter("password");
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		UserPeople up=us.isRegister(Long.valueOf(phone));
		UserPeople user=new UserPeople(up.getName(), pwd, Long.valueOf(phone), up.getpid(), up.getAnswer());
		boolean flag=us.updatePwd(user);
		if(flag){
			pw.print("<script language='javascript'>alert('��ϲ���������޸ĳɹ���ȥ��½��^-^');window.location.href='/waimai/login.jsp';</script>");
		}
		
	
	}

}
