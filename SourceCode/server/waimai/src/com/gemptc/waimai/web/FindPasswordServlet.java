package com.gemptc.waimai.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class FindPasswordServlet
 */
@WebServlet("/FindPasswordServlet")
public class FindPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		String phone=request.getParameter("phone");
		String pid=request.getParameter("problem");
		String answer=request.getParameter("answer");
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		UserPeople user=us.isRegister(Long.valueOf(phone));
		UserPeople up=us.getUserPwd(Long.valueOf(phone), Integer.valueOf(pid), answer);
		if(user!=null){
			if(up!=null){
				request.getSession().setAttribute("phone", phone);
				request.setAttribute("msg", up.getPassword());
				request.getRequestDispatcher("/WEB-INF/success.jsp").forward(request, response);	
				
			}else{
				pw.print("<script language='javascript'>alert('对不起，您的输入有误，密码不能找回！');window.location.href='/waimai/user/forgetpossword.jsp';</script>");
			}
		}else{
			pw.print("<script language='javascript'>alert('对不起，该手机号尚未注册！');window.location.href='/waimai/user/forgetpossword.jsp';</script>");
		}
		
	}

}
