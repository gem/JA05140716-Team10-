package com.gemptc.waimai.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class UserAdviceServlet
 */
@WebServlet("/UserAdviceServlet")
public class UserAdviceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		PrintWriter pw=response.getWriter();
		String info=request.getParameter("message");
		String rid=request.getParameter("id");
		String name=request.getParameter("name");
		String sid=(String) session.getAttribute("id");
		UserService us=new UserServiceImpl(new UserDaoImplMysql());
		if(name!=null&&!name.equals("")){
			UserPeople userPeople=(UserPeople) session.getAttribute("up");
			long phone=userPeople.getPhone();
			boolean flag=us.addAdvice(phone, info);
			if(sid.equals(rid)){
				session.removeAttribute("id");
				if(flag){
					pw.print("<script language='javascript'>alert('恭喜您，意见已发表成功，我们将会做的更好！');</script>");
				}
			}else{
				pw.print("<script language='javascript'>alert('意见已发表成功，请不要重复提交！');</script>");
			}
		}else{
			pw.print("<script language='javascript'>alert('对不起，您尚未登陆，无法发表意见，请登录！');window.location.href='/waimai/login.jsp';</script>");
		}
		
		
	}

}
