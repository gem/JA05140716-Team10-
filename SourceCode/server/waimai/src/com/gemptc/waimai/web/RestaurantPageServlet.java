package com.gemptc.waimai.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.RestaurantPage;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class StudPageServlet
 */
@WebServlet("/RestaurantPageServlet")
public class RestaurantPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String currentPage = request.getParameter("currentPage");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		RestaurantPage pager = us.searchRestaurantByPage(Integer.parseInt(currentPage), 10);
		if(pager != null){
			request.setAttribute("pager", pager);
		}
		request.getRequestDispatcher("/showrestaurant/restaurantlist.jsp").forward(request, response);
	}
}
