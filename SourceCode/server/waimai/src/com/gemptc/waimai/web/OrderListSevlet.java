package com.gemptc.waimai.web;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.ShopCar;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.Shoppingcar;

/**
 * Servlet implementation class OrderListSevlet
 */
@WebServlet("/OrderListSevlet")
public class OrderListSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList buyList=(ArrayList) request.getSession().getAttribute("buylist");
		String s2="";
		Double totalprice=0.0;
		for(int i=0;i<buyList.size();i++){
			Shoppingcar food=(Shoppingcar) buyList.get(i);
			String price=String .valueOf(food.getFprice());
			String num=String .valueOf(food.getNumber());
			String s1=food.getFname()+","+price+"Ԫ,"+num+"��";
			s2+=s1+";";
			totalprice+=food.getFprice();
		}
		//Food food=(Food) request.getSession().getAttribute("food");
		int rid=(int)request.getSession().getAttribute("RID");
		String  time=new Date(System.currentTimeMillis()).toLocaleString();
		request.getSession().setAttribute("s2", s2);
		request.getSession().setAttribute("rid", rid);
		request.getSession().setAttribute("time",time);
		request.getSession().setAttribute("total", totalprice);
		request.getRequestDispatcher("/orderlist/orderlist.jsp").forward(request, response);
	}

}
