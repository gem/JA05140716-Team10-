package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class UpdateSuccessServlet
 */
@WebServlet("/UpdateSuccessServlet")
public class UpdateSuccessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("id"));
		int rid=Integer.parseInt(request.getParameter("rid"));
		String fname=request.getParameter("fname");
		String ftype=request.getParameter("ftype");
		Double fprice=Double.valueOf(request.getParameter("fprice"));
		Food food=new Food(id,rid,fname, ftype, fprice);
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		boolean flag=us.updateFood(food);
		if(flag){
			request.getSession().removeAttribute("foods");
			List<Food> foods=us.searchFoodListByRid(rid);
			request.getSession().setAttribute("foods", foods);
			request.getRequestDispatcher("/seller/show.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("/seller/show.jsp").forward(request, response);
		}
	}

}
