package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.entity.Seat;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class ShowOrderList
 */
@WebServlet("/ShowSeat")
public class ShowSeat extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=(int) request.getSession().getAttribute("id");
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		 List<Seat> seatinfo=us.getseatinfoByRid(id);
//		 for(OrderList o:orderLists){
//			 System.out.println(o.getBname()+o.getOtime());
//		 }
		 request.getSession().setAttribute("seatinfo", seatinfo);
		 request.getRequestDispatcher("/seller/showseat.jsp").forward(request, response);
	}

}
