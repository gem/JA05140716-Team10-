package com.gemptc.waimai.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;
import com.sun.xml.internal.bind.v2.model.core.ID;

/**
 * Servlet implementation class SellerAddSuccessServlet
 */
@WebServlet("/SellerAddSuccessServlet")
@MultipartConfig
public class SellerAddSuccessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String sellname = request.getParameter("sellname");
		String sellpwd = request.getParameter("sellpwd");
		String telephone = request.getParameter("phone");
		String email = request.getParameter("email");
		String area = request.getParameter("areaname");
		String place = request.getParameter("placename");
		String type=request.getParameter("typename");
		String rname = request.getParameter("rname");
		String address = request.getParameter("address");
		String info = request.getParameter("info");
		PrintWriter pw = response.getWriter();
		Part part = request.getPart("myfile");
		// String path = getServletContext().getRealPath("/file");
		String path = "D:\\tomcat\\apache-tomcat-7.0.56\\webapps\\SellerImages";
		String path2="http:\\10.203.1.44:8080\\SellerImages\\";
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdir();
		}
		File file = new File(dir, sellname + ".jpg");
		part.write(file.getPath());// 保存文件
		String jpgurl = path2+sellname+".jpg";
		String areaname = null;
		String placename = null;
		if ("1".equals(area)) {
			areaname = "平江区";
			if ("0".equals(place)) {
				placename = "观前街道";
			} else if ("1".equals(place)) {
				placename = "平江路街道";
			} else if ("2".equals(place)) {
				placename = "苏锦街道";
			} else if ("3".equals(place)) {
				placename = "娄门街道";
			} else if ("4".equals(place)) {
				placename = "城北街道";
			} else if ("5".equals(place)) {
				placename = "桃花坞";
			}
		} else if ("2".equals(area)) {
			areaname = "工业园区";
			if ("0".equals(place)) {
				placename = "唯亭街道";
			} else if ("1".equals(place)) {
				placename = "斜塘街道";
			} else if ("2".equals(place)) {
				placename = "娄葑街道";
			} else if ("3".equals(place)) {
				placename = "胜浦街道";
			} else if ("4".equals(place)) {
				placename = "独墅湖科教创新区";
			}
		} else if ("3".equals(area)) {
			areaname = "沧浪区";
			if ("0".equals(place)) {
				placename = "公园街道";
			} else if ("1".equals(place)) {
				placename = "府前街道";
			} else if ("2".equals(place)) {
				placename = "南门街道";
			} else if ("3".equals(place)) {
				placename = "吴门桥街道";
			} else if ("4".equals(place)) {
				placename = "葑门街道";
			} else if ("5".equals(place)) {
				placename = "双塔街";
			} else if ("6".equals(place)) {
				placename = "友新街道";
			} else if ("7".equals(place)) {
				placename = "胥江街道";
			}

		} else if ("4".equals(area)) {
			areaname = "吴中区";
			if ("0".equals(place)) {
				placename = "长桥镇";
			} else if ("1".equals(place)) {
				placename = "胥口镇";
			} else if ("2".equals(place)) {
				placename = "木渎镇";
			} else if ("3".equals(place)) {
				placename = "横泾镇";
			} else if ("4".equals(place)) {
				placename = "浦庄镇";
			} else if ("5".equals(place)) {
				placename = "渡村镇";
			} else if ("6".equals(place)) {
				placename = "东山镇";
			} else if ("7".equals(place)) {
				placename = "西山镇";
			} else if ("8".equals(place)) {
				placename = "藏书镇";
			} else if ("9".equals(place)) {
				placename = "光福镇";
			} else if ("10".equals(place)) {
				placename = "镇湖镇";
			} else if ("11".equals(place)) {
				placename = "东渚镇";
			} else if ("12".equals(place)) {
				placename = "角直镇";
			} else if ("13".equals(place)) {
				placename = "车坊镇";
			} else if ("14".equals(place)) {
				placename = "郭巷镇";
			}
		} else if ("5".equals(area)) {
			areaname = "金阊区";
			if ("0".equals(place)) {
				placename = "太平街道";
			} else if ("1".equals(place)) {
				placename = "北桥街道";
			} else if ("2".equals(place)) {
				placename = "元河街道";
			} else if ("3".equals(place)) {
				placename = "黄桥街道";
			}
		} else if ("6".equals(area)) {
			areaname = "相城区";
			if ("0".equals(place)) {
				placename = "石路街道";
			} else if ("1".equals(place)) {
				placename = "彩香街道";
			} else if ("2".equals(place)) {
				placename = "留园街道";
			} else if ("3".equals(place)) {
				placename = "虎丘街道";
			} else if ("4".equals(place)) {
				placename = "白洋湾街道";
			}
		} else if ("7".equals(area)) {
			areaname = "虎丘区";
			if ("0".equals(place)) {
				placename = "横塘街道";
			} else if ("1".equals(place)) {
				placename = "狮山街道";
			} else if ("2".equals(place)) {
				placename = "枫桥街道";
			} else if ("3".equals(place)) {
				placename = "镇湖街道";
			}
		}
		// String city=area+place;
		UserService us = new UserServiceImpl(new UserDaoImplMysql());
		Restaurant R = new Restaurant(sellname, sellpwd, rname,
				Long.valueOf(telephone), email, areaname, placename, address,
				info, jpgurl,type);
		boolean flag = us.addRestaurant(R);
		if (flag) {
			request.setAttribute("msg", "恭喜你注册成功，2秒后为你转至主界面！");
			response.setHeader("refresh", " 2;url=" + request.getContextPath()
					+ "/main/main.jsp");
			request.getRequestDispatcher("/success.jsp").forward(request,
					response);
		} else {
			request.setAttribute("msg", "对不起商户入驻失败，请重新注册，2秒后为你转至注册界面！");
			response.setHeader("refresh", " 2;url=" + request.getContextPath()
					+ "/addseller/regist.jsp");
			request.getRequestDispatcher("/fail.jsp")
					.forward(request, response);
		}
	}

}
