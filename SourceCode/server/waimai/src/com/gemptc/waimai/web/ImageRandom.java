package com.gemptc.waimai.web;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet("/ImageRandom")
public class ImageRandom extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setHeader("content-type", "image/jpeg");
		// Expires: -1//3�ֽ�ֹ�����ͷ�ֶ�
		// Cache-Control: no-cache
		// Pragma: no-cache
		// ��Ҫ���������ͼƬ
		resp.setHeader("cache-control", "no-cache");
		resp.setHeader("pragma", "no-cache");
		resp.setHeader("expires", "-1");
		BufferedImage image = new BufferedImage(60, 20,
				BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();
		String chars1 = "0123456789";  
		String chars2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";  
        char[] rands1 = new char[2]; 
        char[] rands2 = new char[2];
        for (int i = 0; i < 2; i++) {  
            int rand1 = (int) (Math.random() * 10);  
            rands1[i] = chars1.charAt(rand1);  
        } 
        for (int i = 0; i < 2; i++) {  
            int rand2 = (int) (Math.random() * 52);  
            rands2[i] = chars2.charAt(rand2);  
        }  
        String a=""+rands1[0];
        String b=""+rands2[0];
        String c=""+rands1[1];
        String d=""+rands2[1];
        g.drawString("" + rands1[0], 1, 17);  
        g.drawString("" + rands2[0], 16, 15);  
        g.drawString("" + rands1[1], 31, 18);  
        g.drawString("" + rands2[1], 46, 16);  
        String s=a+b+c+d;
        HttpSession session=req.getSession();
        session.setAttribute("rand", s);
		// ���ͼƬ
		ImageIO.write(image, "jpg", resp.getOutputStream());
	}
}
