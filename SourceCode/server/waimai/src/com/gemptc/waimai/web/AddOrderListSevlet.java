package com.gemptc.waimai.web;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AddOrderListSevlet
 */
@WebServlet("/AddOrderListSevlet")
public class AddOrderListSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		int rid=Integer.parseInt(request.getParameter("rid"));
//		String info=request.getParameter("info");
		String bname=request.getParameter("bname");
		String site=request.getParameter("site");
//		String total=request.getParameter("total");
		String phone=request.getParameter("phone");
//		String uphone=request.getParameter("uphone");
		int rid=(int) request.getSession().getAttribute("rid");
		String info=(String) request.getSession().getAttribute("s2");
		String uphone=(String) request.getSession().getAttribute("uphone");
		Double total=(Double) request.getSession().getAttribute("total");
//		int state=Integer.parseInt(request.getParameter("state"));
		int state=0;
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"+"H:mm:s");
		String times=sdf.format(date);
		Date datetimeDate;
		try {
			datetimeDate = sdf.parse(times);
			java.sql.Timestamp timeDate=new java.sql.Timestamp(datetimeDate.getTime());
			UserService us = new UserServiceImpl(new UserDaoImplMysql());
			OrderList orderList=new OrderList(rid, bname, info, site, timeDate, Long.valueOf(phone), Double.valueOf(total),state,uphone);
			boolean flag=us.addorderlist(orderList);
			if(flag){
				request.setAttribute("msg", "生成订单成功，2秒后为你转至浏览界面！");
				response.setHeader("refresh", " 2;url="+request.getContextPath()+"/RestaurantPageServlet?currentPage=1");
				request.getRequestDispatcher("/success1.jsp").forward(request, response);
			}
			else{
				request.setAttribute("msg", "对不起添加失败，2秒后为你转至购物车界面！");
				response.setHeader("refresh", " 2;url="+request.getContextPath()+"/menu.jsp");
				request.getRequestDispatcher("/fail.jsp").forward(request, response);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
