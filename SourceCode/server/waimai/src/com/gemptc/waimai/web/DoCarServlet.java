package com.gemptc.waimai.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;






import com.gemptc.waimai.dao.impl.ShopCar;
import com.gemptc.waimai.dao.impl.UserDaoImplMysql;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.Shoppingcar;
import com.gemptc.waimai.service.UserService;
import com.gemptc.waimai.service.impl.UserServiceImpl;

/**
 * Servlet implementation class DoCarServlet
 */
@WebServlet("/DoCarServlet")
public class DoCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
	    if(action.equals("add")){
	    	HttpSession session = request.getSession();
		    ArrayList buylist = (ArrayList)session.getAttribute("buylist");
	    	if(buylist==null){
	    		buylist=new ArrayList();
	    		}
	    	ShopCar sc = new ShopCar();
	    	sc.setBuylist(buylist);//将buylist对象赋值给ShopCar类实例中的属性
	    	
			String strId = request.getParameter("id");
			int id = Integer.parseInt(strId);
			
			String fname = request.getParameter("name");
			fname=new String(fname.getBytes("ISO-8859-1"),"utf-8");
	
			String price = request.getParameter("price");
			Double fprice = Double.valueOf(price);
			int num = 1;
			
			Shoppingcar food= new Shoppingcar(id,fname,fprice,num);
			
			//调用ShopCar类中addItem()方法实现食物添加操作
			boolean flag = sc.addFood(food);
			
			session.setAttribute("buylist",buylist);
			System.out.println(flag);
			if(flag){
				//request.getSession().removeAttribute("fod");
				UserService us = new UserServiceImpl(new UserDaoImplMysql());
				int  id1=(int) request.getSession().getAttribute("id");
				List<Food> foods=us.searchFoodListByRid(id1);
				request.getSession().setAttribute("food", foods);
				for(Food f:foods){
					System.out.println(f.getFname());
				}
				request.getRequestDispatcher("/seller/showmain.jsp").forward(request, response);
			//response.setHeader("refresh", "0;url="+request.getContextPath()+"/seller/showmain.jsp");
			}else{
				response.getWriter().println("添加失败，请重选添加");
			}
		}
	    
	    if(action.equals("remove")){
	    	HttpSession session = request.getSession();
		    ArrayList buylist = (ArrayList)session.getAttribute("buylist");
			String id = request.getParameter("id");
		    int fid = Integer.parseInt(id);
			ShopCar sc = new ShopCar();
			sc.setBuylist(buylist);
			boolean flag = sc.removeFood(fid);
			System.out.println(flag);
			if(flag){
				//request.getRequestDispatcher("/waimai/").forward(request, response);
				response.setHeader("refresh", "0.1;url="+request.getContextPath()+"/LoginShopCar");
			}else{
				response.getWriter().println("移除失败");
			}
		}
	    
		if(action.equals("clear")){
			HttpSession session = request.getSession();
	        ArrayList buylist = (ArrayList)session.getAttribute("buylist");
			ShopCar sc = new ShopCar();
			sc.setBuylist(buylist);
			boolean flag = sc.clearFood();
				if(flag){
					response.setHeader("refresh", "0.1;url="+request.getContextPath()+"/LoginShopCar");
				}else{
					response.getWriter().println("购物已经为空");
				}
		}
	}
}
