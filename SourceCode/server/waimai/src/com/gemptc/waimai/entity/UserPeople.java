package com.gemptc.waimai.entity;

public class UserPeople {
private int id;
private String name;
private String password;
private Long phone;
private int pid;
private String answer;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public Long getPhone() {
	return phone;
}
public void setPhone(Long phone) {
	this.phone = phone;
}
public int getpid() {
	return pid;
}
public void setProgrem(int pid) {
	this.pid = pid;
}
public String getAnswer() {
	return answer;
}
public void setAnswer(String answer) {
	this.answer = answer;
}
public UserPeople(int id, String name, String password, Long phone,
		int pid, String answer) {
	super();
	this.id = id;
	this.name = name;
	this.password = password;
	this.phone = phone;
	this.pid = pid;
	this.answer = answer;
}
public UserPeople(String name, String password, Long phone, int pid,
		String answer) {
	super();
	this.name = name;
	this.password = password;
	this.phone = phone;
	this.pid = pid;
	this.answer = answer;
}

}
