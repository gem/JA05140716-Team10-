package com.gemptc.waimai.entity;

public class showHeadPhoto {
	private String result;
	private String phpath;
	public showHeadPhoto(String result, String phpath) {
		super();
		this.result = result;
		this.phpath = phpath;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getPhpath() {
		return phpath;
	}
	public void setPhpath(String phpath) {
		this.phpath = phpath;
	}

}
