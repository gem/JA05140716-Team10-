package com.gemptc.waimai.entity;

import java.util.List;

public class SearchCollectionPage {
	private List<Collection> data;
	private long phone;
	private int currentPage;
	private int totalPage;
	public List<Collection> getData() {
		return data;
	}
	public void setData(List<Collection> data) {
		this.data = data;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public SearchCollectionPage(List<Collection> data, long phone,
			int currentPage, int totalPage) {
		super();
		this.data = data;
		this.phone = phone;
		this.currentPage = currentPage;
		this.totalPage = totalPage;
	}
	public SearchCollectionPage() {
		super();
	}
	
}
