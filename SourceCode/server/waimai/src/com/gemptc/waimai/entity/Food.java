package com.gemptc.waimai.entity;

public class Food {
	private int fid;
	private int rid;
	private String fname;
	private String ftype;
	private double fprice;
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getFtype() {
		return ftype;
	}
	public void setFtype(String ftype) {
		this.ftype = ftype;
	}
	public double getFprice() {
		return fprice;
	}
	public void setFprice(double fprice) {
		this.fprice = fprice;
	}
	public Food(int rid, String fname, String ftype, double fprice) {
		super();
		this.rid = rid;
		this.fname = fname;
		this.ftype = ftype;
		this.fprice = fprice;
	}
	public Food(String fname, String ftype, double fprice) {
		super();
		this.fname = fname;
		this.ftype = ftype;
		this.fprice = fprice;
	}
	public Food() {
		super();
	}
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public Food(int fid, int rid, String fname, String ftype, double fprice) {
		super();
		this.fid = fid;
		this.rid = rid;
		this.fname = fname;
		this.ftype = ftype;
		this.fprice = fprice;
	}
	
}
