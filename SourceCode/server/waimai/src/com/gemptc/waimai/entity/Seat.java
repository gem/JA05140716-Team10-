package com.gemptc.waimai.entity;

public class Seat {
	private int seatid;
	private int rid;
	private int seatnum;
	private String arrivetime;
	private int booleanbj;
	private Long uphoneLong;
	public int getSeatid() {
		return seatid;
	}
	public void setSeatid(int seatid) {
		this.seatid = seatid;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public int getSeatnum() {
		return seatnum;
	}
	public void setSeatnum(int seatnum) {
		this.seatnum = seatnum;
	}
	public String getArrivetime() {
		return arrivetime;
	}
	public void setArrivetime(String arrivetime) {
		this.arrivetime = arrivetime;
	}
	public int getBooleanbj() {
		return booleanbj;
	}
	public void setBooleanbj(int booleanbj) {
		this.booleanbj = booleanbj;
	}
	public Long getUphoneLong() {
		return uphoneLong;
	}
	public void setUphoneLong(Long uphoneLong) {
		this.uphoneLong = uphoneLong;
	}
	public Seat(int seatid, int rid, int seatnum, String arrivetime,
			int booleanbj, Long uphoneLong) {
		super();
		this.seatid = seatid;
		this.rid = rid;
		this.seatnum = seatnum;
		this.arrivetime = arrivetime;
		this.booleanbj = booleanbj;
		this.uphoneLong = uphoneLong;
	}
	public Seat(int rid, int seatnum, String arrivetime, int booleanbj,
			Long uphoneLong) {
		super();
		this.rid = rid;
		this.seatnum = seatnum;
		this.arrivetime = arrivetime;
		this.booleanbj = booleanbj;
		this.uphoneLong = uphoneLong;
	}
	public Seat() {
		super();
	}
}
