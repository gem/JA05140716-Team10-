package com.gemptc.waimai.entity;

public class Advice {
	private int Aid; 
	private long aphone;
	private String Ainfo;
	public int getAid() {
		return Aid;
	}
	public void setAid(int aid) {
		Aid = aid;
	}
	public long getAphone() {
		return aphone;
	}
	public void setAphone(long aphone) {
		this.aphone = aphone;
	}
	public String getAinfo() {
		return Ainfo;
	}
	public void setAinfo(String ainfo) {
		Ainfo = ainfo;
	}
	public Advice(int aid, long aphone, String ainfo) {
		super();
		Aid = aid;
		this.aphone = aphone;
		Ainfo = ainfo;
	}
	public Advice() {
		super();
	}
}
