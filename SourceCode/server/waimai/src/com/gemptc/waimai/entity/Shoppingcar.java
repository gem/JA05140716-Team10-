package com.gemptc.waimai.entity;

public class Shoppingcar {
   private int fid;
   private String fname;
   private Double fprice;
   private int number;
   
public int getFid() {
	return fid;
}
public void setFid(int fid) {
	this.fid = fid;
}
public String getFname() {
	return fname;
}
public void setFname(String fname) {
	this.fname = fname;
}
public Double getFprice() {
	return fprice;
}
public void setFprice(Double fprice) {
	this.fprice = fprice;
}
public int getNumber() {
	return number;
}
public void setNumber(int number) {
	this.number = number;
}
public Shoppingcar(int fid, String fname, Double fprice, int number) {
	super();
	this.fid = fid;
	this.fname = fname;
	this.fprice = fprice;
	this.number = number;
}
public Shoppingcar( String fname, Double fprice, int number) {
	super();
	this.fname = fname;
	this.fprice = fprice;
	this.number = number;
}
public Shoppingcar() {
	super();
}
   
}
