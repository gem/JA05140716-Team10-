package com.gemptc.waimai.entity;

public class ShopCar {
  public String fname;
  private Double fprice;
  private int number;
public String getFname() {
	return fname;
}
public void setFname(String fname) {
	this.fname = fname;
}
public Double getFprice() {
	return fprice;
}
public void setFprice(Double fprice) {
	this.fprice = fprice;
}
public int getNumber() {
	return number;
}
public void setNumber(int number) {
	this.number = number;
}
public ShopCar(String fname, Double fprice, int number) {
	super();
	this.fname = fname;
	this.fprice = fprice;
	this.number = number;
}
public ShopCar() {
	super();
	// TODO Auto-generated constructor stub
}
  
  
}
