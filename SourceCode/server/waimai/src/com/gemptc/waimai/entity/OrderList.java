package com.gemptc.waimai.entity;

import java.sql.Date;
import java.sql.Timestamp;

public class OrderList {
	private int oid;
	private int rid;
	private String bname;
	private String foodinfo;
	private String osite;
	private Timestamp otime;
	private long  ophone;
	private double totalprice;
	private int state;
	private String uphone;
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getFoodinfo() {
		return foodinfo;
	}
	public void setFoodinfo(String foodinfo) {
		this.foodinfo = foodinfo;
	}
	public String getOsite() {
		return osite;
	}
	public void setOsite(String osite) {
		this.osite = osite;
	}
	public Timestamp getOtime() {
		return otime;
	}
	public void setOtime(Timestamp otime) {
		this.otime = otime;
	}
	public long getOphone() {
		return ophone;
	}
	public void setOphone(long ophone) {
		this.ophone = ophone;
	}
	public double getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(double totalprice) {
		this.totalprice = totalprice;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public OrderList(int oid, int rid, String bname, String foodinfo,
			String osite, Timestamp otime, long ophone, double totalprice,int state,String uphone) {
		super();
		this.oid = oid;
		this.rid = rid;
		this.bname = bname;
		this.foodinfo = foodinfo;
		this.osite = osite;
		this.otime = otime;
		this.ophone = ophone;
		this.totalprice = totalprice;
		this.state=state;
		this.uphone=uphone;
	}
	public OrderList(int rid, String bname, String foodinfo, String osite,
			Timestamp otime, long ophone, double totalprice,int state,String uphone) {
		super();
		this.rid = rid;
		this.bname = bname;
		this.foodinfo = foodinfo;
		this.osite = osite;
		this.otime = otime;
		this.ophone = ophone;
		this.totalprice = totalprice;
		this.state=state;
		this.uphone=uphone;
	}
	public OrderList() {
		super();
	}
	public String getUphone() {
		return uphone;
	}
	public void setUphone(String uphone) {
		this.uphone = uphone;
	}
	public OrderList(int rid, String bname, String foodinfo, String osite,
			Timestamp otime, long ophone, double totalprice) {
		super();
		this.rid = rid;
		this.bname = bname;
		this.foodinfo = foodinfo;
		this.osite = osite;
		this.otime = otime;
		this.ophone = ophone;
		this.totalprice = totalprice;
	}
	public OrderList(int rid,String usephone, String bname, String foodinfo, String osite,
			Timestamp otime, long ophone, double totalprice) {
		super();
		this.rid = rid;
		this.uphone = usephone;
		this.bname = bname;
		this.foodinfo = foodinfo;
		this.osite = osite;
		this.otime = otime;
		this.ophone = ophone;
		this.totalprice = totalprice;
	}
	public OrderList(int oid,int rid,String usephone, String bname, String foodinfo, String osite,
			 long ophone, double totalprice,int state) {
		super();
		this.oid=oid;
		this.rid = rid;
		this.uphone = usephone;
		this.bname = bname;
		this.foodinfo = foodinfo;
		this.osite = osite;
		this.ophone = ophone;
		this.totalprice = totalprice;
		this.state=state;
	}
}
