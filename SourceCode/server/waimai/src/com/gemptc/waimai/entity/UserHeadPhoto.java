package com.gemptc.waimai.entity;

public class UserHeadPhoto {
private int id;
private long phone;
private String headpath;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public long getPhone() {
	return phone;
}
public void setPhone(long phone) {
	this.phone = phone;
}
public String getHeadpath() {
	return headpath;
}
public void setHeadpath(String headpath) {
	this.headpath = headpath;
}
public UserHeadPhoto(int id, long phone, String headpath) {
	super();
	this.id = id;
	this.phone = phone;
	this.headpath = headpath;
}
public UserHeadPhoto(long phone, String headpath) {
	super();
	this.phone = phone;
	this.headpath = headpath;
}
public UserHeadPhoto() {
	super();
}

}
