package com.gemptc.waimai.entity;

public class Restaurant {
	private int rid;
	private String sname;
	private String spwd;
	private String rname;
	private long telephone;
	private String email;
	private String city;
	private String stress;
	private String type;
	private String rsite;
	private String info;
	private String jpgurl;
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getSpwd() {
		return spwd;
	}
	public void setSpwd(String spwd) {
		this.spwd = spwd;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public long getTelephone() {
		return telephone;
	}
	public void setTelephone(long telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRsite() {
		return rsite;
	}
	public void setRsite(String rsite) {
		this.rsite = rsite;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getJpgurl() {
		return jpgurl;
	}
	public void setJpgurl(String jpgurl) {
		this.jpgurl = jpgurl;
	}
	public Restaurant(int rid, String sname, String spwd, String rname,
			long telephone, String email, String city,String stress, String rsite,
			String info, String jpgurl,String type) {
		super();
		this.rid = rid;
		this.sname = sname;
		this.spwd = spwd;
		this.rname = rname;
		this.telephone = telephone;
		this.email = email;
		this.city = city;
		this.stress=stress;
		this.rsite = rsite;
		this.info = info;
		this.jpgurl = jpgurl;
		this.type=type;
	}
	public Restaurant(String sname, String spwd, String rname, long telephone,
			String email, String city,String stress,String rsite, String info, String jpgurl,String type) {
		super();
		this.sname = sname;
		this.spwd = spwd;
		this.rname = rname;
		this.telephone = telephone;
		this.email = email;
		this.city = city;
		this.stress=stress;
		this.rsite = rsite;
		this.info = info;
		this.jpgurl = jpgurl;
		this.type=type;
	}
	public Restaurant() {
		super();
	}
	public String getStress() {
		return stress;
	}
	public void setStress(String stress) {
		this.stress = stress;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
