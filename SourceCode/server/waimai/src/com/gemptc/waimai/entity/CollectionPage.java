package com.gemptc.waimai.entity;

import java.util.List;

public class CollectionPage {
	private List<Collection> data;
	private int currentPage;
	private int totalPage;
	public List<Collection> getData() {
		return data;
	}
	public void setData(List<Collection> data) {
		this.data = data;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public CollectionPage(List<Collection> data, int currentPage, int totalPage) {
		super();
		this.data = data;
		this.currentPage = currentPage;
		this.totalPage = totalPage;
	}
	public CollectionPage() {
		super();
	}
	
}
