package com.gemptc.waimai.entity;

public class Content {
	/* Cid int primary key auto_increment,
		      Sid int references Seller(Sid),
		      Rid int references Restaurant(Rid),
		      Cinfo varchar(200)      */
	private int cid;
	private long uphone;
	private int rid;
	private String date;
	private String cinfo;
	
	
	public long getUphone() {
		return uphone;
	}
	public void setUsephone(long uphone) {
		this.uphone = uphone;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getCinfo() {
		return cinfo;
	}
	public void setCinfo(String cinfo) {
		this.cinfo = cinfo;
	}
	public Content(int cid,int rid,long uphone, String cinfo,String date) {
		super();
		this.cid = cid;
		this.rid = rid;
		this.uphone=uphone;
		this.cinfo = cinfo;
		this.date = date;
	}
	public Content(int rid, String cinfo,String date) {
		super();
		this.rid = rid;
		this.cinfo = cinfo;
		this.date = date;
	}
	public Content( String cinfo,String date) {
		super();
		this.date =date;
		this.cinfo = cinfo;
	}
	
	public Content( Long uphone,int rid,String cinfo,String date) {
		super();
		this.uphone = uphone;
		this.rid = rid;
		this.date =date;
		this.cinfo = cinfo;
	}	
	public Content() {
		super();
	}
	public Content(String cinfo) {
		super();
		this.cinfo = cinfo;
	}
	
}
