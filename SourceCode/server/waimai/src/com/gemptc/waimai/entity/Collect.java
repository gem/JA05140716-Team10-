package com.gemptc.waimai.entity;

public class Collect {
  private String rname;
  private String info;
  private long telephone;
  private long uphone;
  public String getRname() {
	return rname;
}
public void setRname(String rname) {
	this.rname = rname;
}
public String getInfo() {
	return info;
}
public void setInfo(String info) {
	this.info = info;
}
public long getTelephone() {
	return telephone;
}
public void setTelephone(long telephone) {
	this.telephone = telephone;
}
public long getUphone() {
	return uphone;
}
public void setUphone(long uphone) {
	this.uphone = uphone;
}
public Collect(String rname, String info, long telephone, long uphone) {
	super();
	this.rname = rname;
	this.info = info;
	this.telephone = telephone;
	this.uphone = uphone;
}
public Collect() {
	super();
	// TODO Auto-generated constructor stub
}
  
}
