package com.gemptc.waimai.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JDBCUtil {
	private static Properties prop;
	static{
		prop = new Properties();
		InputStream is = JDBCUtil.class.getResourceAsStream("/db.properties");
		try {
			prop.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void saveStudId(long id){
		Connection conn = getConn();
		Statement st  = null;
		try {
			st = conn.createStatement();
			st.executeUpdate("update studids set ids="+id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			close(conn,st,null);
		}
	}
	public static long getStudId(){
		Connection conn = getConn();
		Statement st  = null;
		ResultSet rs = null;
		try {
			st = conn.createStatement();
			rs = st.executeQuery("select ids from studids");
			if(rs.next()){
				return rs.getLong("ids");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			close(conn,st,null);
		}
		return 0;
	}
	
	public static void close(Connection conn,Statement st,ResultSet rs){
		if(conn != null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(st != null){
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(rs != null){
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Connection getConn(){
		String user = prop.getProperty("user");
		String password = prop.getProperty("password");
		String url = prop.getProperty("url");
		String driver = prop.getProperty("driver");
		try {
			Class.forName(driver);
			return DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void main(String[] args) {
		Connection conn = getConn();
		if(conn != null){
			System.out.println("good");
		}else{
			System.out.println("bad");
		}
	}
}
