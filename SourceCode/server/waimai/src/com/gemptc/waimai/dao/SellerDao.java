package com.gemptc.waimai.dao;

import java.util.List;

import com.gemptc.waimai.entity.Collect;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.entity.OrderList;

public interface SellerDao {
    public boolean addCinfo(Content c);
    public List<Content> getContentByRid(int rid);
    public boolean addCollect(Collection cc);
    public boolean deleteCollect(long usephone,int rid);
    public boolean addOrderList(OrderList or);
    public String selectcollection(int rid,long usephone);
}
