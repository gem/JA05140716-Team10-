package com.gemptc.waimai.dao;

import java.util.List;

import com.gemptc.waimai.entity.Buyer;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.CollectionPage;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.entity.RestaurantPage;
import com.gemptc.waimai.entity.SearchCollectionPage;
import com.gemptc.waimai.entity.Seat;
import com.gemptc.waimai.entity.UserHeadPhoto;
import com.gemptc.waimai.entity.UserPeople;



public interface UserDao {
	public RestaurantPage searchRestaurantByPage(int currentPage,int pageSize);
	public Buyer getBuyerByNameAndPassword(String name,String password);
	public boolean addFood(Food f);
	public boolean addRestaurant(Restaurant R);
	public Restaurant getRestaurantIdByName(String name);
	public boolean createTableFoofListByNmae(String name);
	public Restaurant getSellererByNameAndPassword(String name,String password);
	public List<Food> searchFoodListByRid(int id);
	public boolean deleteFoodById(int id);
	public Food searchRidByFid(int id);
	public List<Food> searchFoodByFname(String name);
	public boolean updateFood(Food f);
	public boolean updateorder(OrderList o);
	public boolean addorderlist(OrderList o);
	public Restaurant getRestaurantById(int rid);
	public List<Content> getContentByRid(int rid);
	public List<OrderList> searchOrderListByRid(int id);
	public OrderList searchOrderListByOid(int id);
	public UserPeople getUserByNameAndPassword(long phone,String password);
	public UserPeople getUserPwd(long phone,int pid,String answer);
	public boolean addUserPeople(UserPeople up);
	public UserPeople isRegister(long phone);
	public List<Restaurant> getRestaurantsByStress(String stress);
	public RestaurantPage searchRestaurantPageByStress(int currentPage,int pageSize,String stress);
	public RestaurantPage searchRestaurantPageByCity(int currentPage,int pageSize,String city);
	public RestaurantPage searchRestaurantPageByStressAndType(int currentPage,int pageSize,String city,String type);
	public RestaurantPage searchRestaurantPageByType(int currentPage,int pageSize,String type);
	public List<OrderList> getOrderlistByPhone(int currentPage,int pageSize,String uphone);
	public List<OrderList> getOrderlistByPhoneAndstate(int currentPage,int pageSize,String uphone,int state);
	public boolean updatePwd(UserPeople up);
	public boolean updateName(UserPeople up);
	public boolean addAdvice(long phone,String advice);
	public CollectionPage searchCollectionByPage(int currentPage,int pageSize);
	public List<Collection> searchCollectionByPhone(Long phone);
	public int  searchCollectionCountByPhone(Long phone);
	public int  searchContentCountByPhone(Long phone);
	public List<Seat> getseatinfoByRid(int id);
	public boolean deleteseatByseatId(int id);
	public Seat getseatinfoByseatid(int id);
	public boolean deleteorderByoOid(int id);
	public OrderList getorderByOid(int id);
	public boolean addUserHeadPhoto(Long phone,String path);
	public UserHeadPhoto seachUserHeadPhoto(long phone);
	public int searchResturantCount();
	public List<Restaurant> serchRestaurantsById(int id);
	public boolean addSeat(Seat s);
	public SearchCollectionPage searchCollectionByPage(long phone, int currentPage,int pageSize);

}
