package com.gemptc.waimai.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gemptc.waimai.dao.SellerDao;
import com.gemptc.waimai.util.JDBCUtil;
import com.gemptc.waimai.entity.Collect;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.entity.OrderList;

public class SellerDaoImplMysql implements SellerDao {

	//添加评论
	@Override
	public boolean addCinfo(Content c) {
		Connection conn=JDBCUtil.getConn();
		String sql = "insert into Content(cid,uphone,rid,cinfo,date) values(?,?,?,?,?)";
		PreparedStatement pst = null;
		try {
		    pst = conn.prepareStatement(sql);
		    pst.setObject(1,c.getCid());
		    pst.setObject(2,c.getUphone());
		    pst.setObject(3, c.getRid());
		    pst.setObject(4, c.getCinfo());
		    pst.setObject(5, c.getDate());
		    int count=pst.executeUpdate();
			if(count>=1) return true;
			else return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	//得到评论
	@Override
	public List<Content> getContentByRid(int rid) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		String sql = "select * from content where rid = ?";
		ResultSet rs = null;
		List<Content> c = new ArrayList<Content>();
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, rid);
			rs = pst.executeQuery();
			while(rs.next()){
			Integer cid = rs.getInt("cid");
			String uphone=rs.getString("uphone");
			String cinfo = rs.getString("cinfo");
			String date = rs.getString("date");
			Content con = new Content(cid,rid,Long.valueOf(uphone),cinfo,date);
			c.add(con);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			JDBCUtil.close(conn, pst, rs);
		}
		return c;
	}

	//添加收藏
	@Override
	public boolean addCollect(Collection cc) {
		Connection conn=JDBCUtil.getConn();
		String sql = "insert into Collection(jpgurl,rname,info,telephone,uphone,rsite,rid,sname) values(?,?,?,?,?,?,?,?)";
		PreparedStatement pst = null;
		try {
		    pst = conn.prepareStatement(sql);
		    pst.setObject(1,cc.getJpgurl());
		    pst.setObject(2,cc.getRname());
		    pst.setObject(3,cc.getInfo() );
		    pst.setObject(4,cc.getTelephone());
		    pst.setObject(5,cc.getUphone() );
		    pst.setObject(6,cc.getRsite());
		    pst.setObject(7,cc.getRid());
		    pst.setObject(8,cc.getSname());
		    int count=pst.executeUpdate();
			if(count>=1) return true;
			else return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	//取消收藏
	@Override
	public boolean deleteCollect(long usephone, int rid) {
		Connection conn = JDBCUtil.getConn();
		String sql = "delete from Collection where uphone = ? and Rid = ?";
		PreparedStatement pst = null;
		try {
		    pst = conn.prepareStatement(sql);
		    pst.setObject(1,usephone);
		    pst.setObject(2,rid);  
		    int count=pst.executeUpdate();
			if(count>=1) return true;
			else return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	//添加订单
	@Override
	public boolean addOrderList(OrderList or) {
		Connection conn=JDBCUtil.getConn();
		String sql = "insert into Orderlistinfomation(rid,usephone,foodinfo,bname,osite,otime,ophone,ototalprice) values(?,?,?,?,?,?,?,?)";
		PreparedStatement pst = null;
		try {
		    pst = conn.prepareStatement(sql);
		    pst.setObject(1,or.getRid());
		    pst.setObject(2,or.getUphone()); 
		    pst.setObject(3,or.getFoodinfo()); 
		    pst.setObject(4,or.getBname()); 
		    pst.setObject(5,or.getOsite()); 
		    pst.setObject(6,or.getOtime()); 
		    pst.setObject(7,or.getOphone()); 
		    pst.setObject(8,or.getTotalprice()); 
		    int count=pst.executeUpdate();
			if(count>=1) return true;
			else return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public String selectcollection(int rid, long usephone) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		String sql = "select rname from collection where rid = ? and uphone = ?";
		ResultSet rs = null;
		String rname = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, rid);
			pst.setObject(2, usephone);
			rs = pst.executeQuery();
			if (rs.next()) {
				rname = rs.getString("rname");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			JDBCUtil.close(conn, pst, rs);
		}
		return rname;
	}
    
	
}
