package com.gemptc.waimai.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import com.gemptc.waimai.dao.UserDao;
import com.gemptc.waimai.entity.Buyer;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.CollectionPage;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.entity.RestaurantPage;
import com.gemptc.waimai.entity.SearchCollectionPage;
import com.gemptc.waimai.entity.Seat;
import com.gemptc.waimai.entity.UserHeadPhoto;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.util.JDBCUtil;
import com.sun.jndi.url.rmi.rmiURLContext;
import com.sun.org.apache.regexp.internal.recompile;
import com.sun.xml.internal.bind.v2.model.core.ID;

public class UserDaoImplMysql implements UserDao {

	public int getTotalRecord() {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		int total = 0;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("select count(*) from Restaurant");
			rs = pst.executeQuery();
			if (rs.next()) {
				total = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return total;
	}

	public int getTotalRecord1() {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		int total = 0;
		ResultSet rs = null;
		try {
			pst = conn
					.prepareStatement("select count(*) from Orderlistinfomation");
			rs = pst.executeQuery();
			if (rs.next()) {
				total = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return total;
	}

	@Override
	public RestaurantPage searchRestaurantByPage(int currentPage, int pageSize) {
		int total = getTotalRecord();
		// total = 51 pageSize = 10;
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Restaurant> data = new ArrayList<Restaurant>();
		try {
			pst = conn.prepareStatement("select * from Restaurant limit ?,?");
			pst.setObject(1, (currentPage - 1) * pageSize);
			pst.setObject(2, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int rid = rs.getInt("Rid");
				String sname = rs.getString("Sname");
				String spwd = rs.getString("Spwd");
				String rname = rs.getString("Rname");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String stress = rs.getString("stress");
				String rsite = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type = rs.getString("type");
				Restaurant s = new Restaurant(rid, sname, spwd, rname,
						telephone, email, city, stress, rsite, info, jpgurl,
						type);
				data.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return new RestaurantPage(data, currentPage, totalPage);
	}

	@Override
	public Buyer getBuyerByNameAndPassword(String name, String password) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		Buyer b = null;
		try {
			pst = conn
					.prepareStatement("select * from Buyer where Bname=? and Bpwd=?");
			pst.setObject(1, name);
			pst.setObject(2, password);
			rs = pst.executeQuery();
			if (rs.next()) {
				int bid = rs.getInt("Bid");
				String bname = rs.getString("Bname");
				String bpwd = rs.getString("Bpwd");
				b = new Buyer(bid, bname, bpwd);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return b;
	}

	@Override
	public boolean addFood(Food f) {
		Connection conn = JDBCUtil.getConn();
		String sql = "insert into Foodlist(Rid,Fname,Ftype,Fprice) values(?,?,?,?)";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, f.getRid());
			pst.setObject(2, f.getFname());
			pst.setObject(3, f.getFtype());
			pst.setObject(4, f.getFprice());
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public boolean addRestaurant(Restaurant R) {
		Connection conn = JDBCUtil.getConn();
		String sql = "insert into Restaurant(Sname,Spwd,Rname,telephone,email,city,Rsite,info,jpgurl,stress,type) values(?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, R.getSname());
			pst.setObject(2, R.getSpwd());
			pst.setObject(3, R.getRname());
			pst.setObject(4, R.getTelephone());
			pst.setObject(5, R.getEmail());
			pst.setObject(6, R.getCity());
			pst.setObject(7, R.getRsite());
			pst.setObject(8, R.getInfo());
			pst.setObject(9, R.getJpgurl());
			pst.setObject(10, R.getStress());
			pst.setObject(11, R.getType());
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public Restaurant getRestaurantIdByName(String name) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		Restaurant R = null;

		try {
			pst = conn
					.prepareStatement("select * from Restaurant where Rname=?");
			pst.setObject(1, name);
			rs = pst.executeQuery();
			System.out.println(rs);
			if (rs.next()) {
				int rid = rs.getInt("Rid");
				String sellname = rs.getString("Sname");
				String sellpwd = rs.getString("Spwd");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String stress = rs.getString("stress");
				String rname = rs.getString("rname");
				String address = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type = rs.getString("type");
				R = new Restaurant(rid, sellname, sellpwd, rname, telephone,
						email, city, stress, address, info, jpgurl, type);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return R;
	}

	@Override
	public boolean createTableFoofListByNmae(String name) {
		// Connection conn=JDBCUtil.getConn();
		// String sql = "create table "+name+""
		// + "(Fname varchae(100),"
		// + "Fprice double,"
		// + "Ftype varchar(50))";
		// Statement stmt=null;
		// try {
		// //pst=conn.prepareStatement(sql);
		// //pst.setObject(1, name);
		// stmt =conn.createStatement();
		// int count=stmt.executeUpdate(sql);
		// if(count>=1) return true;
		// else return false;
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }finally{
		// JDBCUtil.close(conn, stmt, null);
		// }
		return false;
	}

	@Override
	public Restaurant getSellererByNameAndPassword(String name, String password) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		Restaurant R = null;
		try {
			pst = conn
					.prepareStatement("select * from Restaurant where Sname=? and Spwd=?");
			pst.setObject(1, name);
			pst.setObject(2, password);
			rs = pst.executeQuery();
			if (rs.next()) {
				int rid = rs.getInt("Rid");
				String sellname = rs.getString("Sname");
				String sellpwd = rs.getString("Spwd");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String stress = rs.getString("stress");
				String rname = rs.getString("rname");
				String address = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type = rs.getString("type");
				R = new Restaurant(rid, sellname, sellpwd, rname, telephone,
						email, city, stress, address, info, jpgurl, type);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return R;
	}

	@Override
	public List<Food> searchFoodListByRid(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Food> f = new ArrayList<Food>();
		try {
			pst = conn.prepareStatement("select * from Foodlist where Rid=?");
			pst.setObject(1, id);

			rs = pst.executeQuery();
			while (rs.next()) {
				int fid = rs.getInt("Fid");
				int rid = rs.getInt("Rid");
				String fname = rs.getString("Fname");
				String ftype = rs.getString("ftype");
				Double fprice = rs.getDouble("Fprice");
				Food food = new Food(fid, rid, fname, ftype, fprice);
				f.add(food);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return f;
	}

	@Override
	public boolean deleteFoodById(int id) {
		Connection conn = JDBCUtil.getConn();
		String sql = "delete from Foodlist where Fid=?";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, id);
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public Food searchRidByFid(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		Food f = null;
		try {
			pst = conn.prepareStatement("select * from Foodlist where Fid=?");
			pst.setObject(1, id);
			rs = pst.executeQuery();
			if (rs.next()) {
				int fid = rs.getInt("Fid");
				int rid = rs.getInt("Rid");
				String fname = rs.getString("Fname");
				String ftype = rs.getString("ftype");
				Double fprice = rs.getDouble("Fprice");
				f = new Food(fid, rid, fname, ftype, fprice);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return f;
	}

	@Override
	public boolean updateFood(Food f) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn
					.prepareStatement("update Foodlist set Fname=?,Ftype=?,Fprice=? where Fid=?");
			pst.setObject(1, f.getFname());
			pst.setObject(2, f.getFtype());
			pst.setObject(3, f.getFprice());
			pst.setObject(4, f.getFid());
			int count = pst.executeUpdate();
			if (count >= 1) {
				return true;
			} else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return false;
	}

	@Override
	public List<Food> searchFoodByFname(String name) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Food> f = new ArrayList<Food>();
		try {
			pst = conn.prepareStatement("select * from Foodlist where Fname=?");
			pst.setObject(1, name);

			rs = pst.executeQuery();
			while (rs.next()) {
				int fid = rs.getInt("Fid");
				int rid = rs.getInt("Rid");
				String fname = rs.getString("Fname");
				String ftype = rs.getString("ftype");
				Double fprice = rs.getDouble("Fprice");
				Food food = new Food(fid, rid, fname, ftype, fprice);
				f.add(food);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return f;
	}

	@Override
	public boolean addorderlist(OrderList o) {
		Connection conn = JDBCUtil.getConn();
		String sql = "insert into Orderlistinfomation(Rid,Bname,FoodInfo,Ophone,Osite,Otime,Ototalprice,state,uphone) values(?,?,?,?,?,?,?,?,?)";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, o.getRid());
			pst.setObject(2, o.getBname());
			pst.setObject(3, o.getFoodinfo());
			pst.setObject(4, o.getOphone());
			pst.setObject(5, o.getOsite());
			pst.setObject(6, o.getOtime());
			pst.setObject(7, o.getTotalprice());
			pst.setObject(8, o.getState());
			pst.setObject(9, o.getUphone());
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public Restaurant getRestaurantById(int rid) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		String sql = "select * from Restaurant where Rid = ?";
		ResultSet rs = null;
		Restaurant R = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, rid);
			rs = pst.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("Rid");
				String sellname = rs.getString("Sname");
				String sellpwd = rs.getString("Spwd");
				int telephone = rs.getInt("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String stress = rs.getString("stress");
				String rname = rs.getString("rname");
				String address = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type = rs.getString("type");
				R = new Restaurant(id, sellname, sellpwd, rname, telephone,
						email, city, stress, address, info, jpgurl, type);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return R;

	}

	@Override
	public List<Content> getContentByRid(int rid) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		String sql = "Select cinfo from Content where Rid=?";
		ResultSet rs = null;
		List<Content> c = new ArrayList<Content>();
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, rid);
			rs = pst.executeQuery();
			while (rs.next()) {
				String cinfo = rs.getString("Cinfo");
				Content con = new Content(cinfo);
				c.add(con);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return c;

	}

	@Override
	public List<OrderList> searchOrderListByRid(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<OrderList> o = new ArrayList<OrderList>();
		try {
			pst = conn
					.prepareStatement("select * from Orderlistinfomation where Rid=?");
			pst.setObject(1, id);

			rs = pst.executeQuery();
			while (rs.next()) {
				int oid = rs.getInt("Oid");
				int rid = rs.getInt("Rid");
				String bname = rs.getString("Bname");
				String foodinfo = rs.getString("FoodInfo");
				Double total = rs.getDouble("Ototalprice");
				long ophone = rs.getLong("Ophone");
				String osite = rs.getString("Osite");
				Timestamp otime = rs.getTimestamp("Otime");
				int state = rs.getInt("state");
				String uphone=rs.getString("uphone");
				OrderList orderList = new OrderList(oid, rid, bname, foodinfo,
						osite, otime, ophone, total, state,uphone);
				o.add(orderList);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return o;
	}

	@Override
	public UserPeople getUserByNameAndPassword(long phone, String password) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		UserPeople userp = null;
		try {
			pst = conn
					.prepareStatement("select * from users where Uphone=? and userpassword=?");
			pst.setObject(1, phone);
			pst.setObject(2, password);
			rs = pst.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("Uid");
				String username = rs.getString("username");
				String userpassword = rs.getString("userpassword");
				Long userphone = rs.getLong("Uphone");
				int pid = rs.getInt("Pid");
				String answer = rs.getString("answer");
				userp = new UserPeople(username, userpassword, userphone, pid,
						answer);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return userp;
	}

	@Override
	public UserPeople getUserPwd(long phone, int pid, String answer) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		UserPeople userp = null;
		try {
			pst = conn
					.prepareStatement("select * from users where Uphone=? and Pid=? and answer=?");
			pst.setObject(1, phone);
			pst.setObject(2, pid);
			pst.setObject(3, answer);
			rs = pst.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("Uid");
				String username = rs.getString("username");
				String userpassword = rs.getString("userpassword");
				Long userphone = rs.getLong("Uphone");
				int pids = rs.getInt("Pid");
				String answers = rs.getString("answer");
				userp = new UserPeople(username, userpassword, userphone, pids,
						answers);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return userp;
	}

	@Override
	public boolean addUserPeople(UserPeople up) {
		Connection conn = JDBCUtil.getConn();
		String sql = "insert into users(username,userpassword,Uphone,Pid,answer) values(?,?,?,?,?)";
		PreparedStatement prep = null;
		try {
			prep = conn.prepareStatement(sql);
			prep.setObject(1, up.getName());
			prep.setObject(2, up.getPassword());
			prep.setObject(3, up.getPhone());
			prep.setObject(4, up.getpid());
			prep.setObject(5, up.getAnswer());
			int count = prep.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, prep, null);
		}

		return false;
	}

	@Override
	public UserPeople isRegister(long phone) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		UserPeople userp = null;
		try {
			pst = conn.prepareStatement("select * from users where Uphone=? ");
			pst.setObject(1, phone);
			rs = pst.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("Uid");
				String username = rs.getString("username");
				String userpassword = rs.getString("userpassword");
				Long userphone = rs.getLong("Uphone");
				int pid = rs.getInt("Pid");
				String answer = rs.getString("answer");
				userp = new UserPeople(username, userpassword, userphone, pid,
						answer);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return userp;
	}

	@Override
	public List<Restaurant> getRestaurantsByStress(String stress) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Restaurant> data = new ArrayList<Restaurant>();
		try {
			pst = conn
					.prepareStatement("select * from Restaurant where stress=?");
			pst.setObject(1, stress);
			rs = pst.executeQuery();
			while (rs.next()) {
				int rid = rs.getInt("Rid");
				String sname = rs.getString("Sname");
				String spwd = rs.getString("Spwd");
				String rname = rs.getString("Rname");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String stressname = rs.getString("stress");
				String rsite = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type = rs.getString("type");
				Restaurant s = new Restaurant(rid, sname, spwd, rname,
						telephone, email, city, stressname, rsite, info,
						jpgurl, type);
				data.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return data;
	}

	@Override
	public RestaurantPage searchRestaurantPageByStress(int currentPage,
			int pageSize, String stress) {
		int total = getTotalRecord();
		// total = 51 pageSize = 10;
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Restaurant> data = new ArrayList<Restaurant>();
		try {
			pst = conn
					.prepareStatement("select * from Restaurant where stress=? limit ?,?");
			pst.setObject(1, stress);
			pst.setObject(2, (currentPage - 1) * pageSize);
			pst.setObject(3, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int rid = rs.getInt("Rid");
				String sname = rs.getString("Sname");
				String spwd = rs.getString("Spwd");
				String rname = rs.getString("Rname");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String stressname = rs.getString("stress");
				String rsite = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type = rs.getString("type");
				Restaurant s = new Restaurant(rid, sname, spwd, rname,
						telephone, email, city, stressname, rsite, info,
						jpgurl, type);
				data.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return new RestaurantPage(data, currentPage, totalPage);
	}

	@Override
	public List<OrderList> getOrderlistByPhone(int currentPage, int pageSize,
			String uphone) {
		int total = getTotalRecord1();
		// total = 51 pageSize = 10;
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<OrderList> data = new ArrayList<OrderList>();
		try {
			pst = conn
					.prepareStatement("select * from Orderlistinfomation where uphone=? limit ?,?");
			pst.setObject(1, uphone);
			pst.setObject(2, (currentPage - 1) * pageSize);
			pst.setObject(3, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int oid = rs.getInt("Oid");
				int rid = rs.getInt("Rid");
				String bname = rs.getString("bname");
				String foodinfo = rs.getString("FoodInfo");
				long phone = rs.getLong("Ophone");
				String osite = rs.getString("Osite");
				Timestamp otime = rs.getTimestamp("Otime");
				Double totalprice = rs.getDouble("Ototalprice");
				int state = rs.getInt("state");
				String uphone1=rs.getString("uphone");
				OrderList o = new OrderList(oid, rid, bname, foodinfo, osite,
						otime, phone, totalprice, state,uphone1);
				data.add(o);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return data;
	}

	@Override
	public List<OrderList> getOrderlistByPhoneAndstate(int currentPage,
			int pageSize, String uphone, int state) {
		int total = getTotalRecord1();
		// total = 51 pageSize = 10;
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<OrderList> data = new ArrayList<OrderList>();
		try {
			pst = conn
					.prepareStatement("select * from Orderlistinfomation where uphone=? and state=? limit ?,?");

			pst.setObject(1, uphone);
			pst.setObject(2, state);
			pst.setObject(3, (currentPage - 1) * pageSize);
			pst.setObject(4, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int oid = rs.getInt("Oid");
				int rid = rs.getInt("Rid");
				String bname = rs.getString("bname");
				String foodinfo = rs.getString("FoodInfo");
				long phone = rs.getLong("Ophone");
				String osite = rs.getString("Osite");
				Timestamp otime = rs.getTimestamp("Otime");
				Double totalprice = rs.getDouble("Ototalprice");
				int state1 = rs.getInt("state");
				String uphone1=rs.getString("uphone");
				OrderList o = new OrderList(oid, rid, bname, foodinfo, osite,
						otime, phone, totalprice, state1,uphone1);
				data.add(o);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return data;
	}

	@Override
	public boolean updatePwd(UserPeople up) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		try {
			pst = conn
					.prepareStatement("update users set userpassword=? where Uphone=?");
			pst.setObject(1, up.getPassword());
			pst.setObject(2, up.getPhone());
			int count = pst.executeUpdate();
			if (count >= 1) {
				return true;
			} else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public boolean updateName(UserPeople up) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		try {
			pst = conn
					.prepareStatement("update users set username=? where Uphone=?");
			pst.setObject(1, up.getName());
			pst.setObject(2, up.getPhone());
			int count = pst.executeUpdate();
			if (count >= 1) {
				return true;
			} else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public boolean addAdvice(long phone, String advice) {
		Connection conn = JDBCUtil.getConn();
		String sql = "insert into Advice(Aphone,Ainfo) values(?,?)";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, phone);
			pst.setObject(2, advice);
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	public int getTotalCollection() {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		int total = 0;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("select count(*) from Collection");
			rs = pst.executeQuery();
			if (rs.next()) {
				total = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return total;
	}

	@Override
	public CollectionPage searchCollectionByPage(int currentPage, int pageSize) {
		int total = getTotalCollection();
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Collection> data = new ArrayList<Collection>();
		try {
			pst = conn.prepareStatement("select * from Collection limit ?,?");
			pst.setObject(1, (currentPage - 1) * pageSize);
			pst.setObject(2, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int coid = rs.getInt("Coid");
				String jpgurl = rs.getString("jpgurl");
				String rname = rs.getString("rname");
				String info = rs.getString("info");
				long telephone = rs.getLong("telephone");
				long userphone = rs.getLong("uphone");
				String rsite=rs.getString("rsite");
				int rid=rs.getInt("rid");
				String sname=rs.getString("sname");
				Collection collection = new Collection(coid, jpgurl, rname,
						info, telephone, userphone,rsite,rid,sname);
				data.add(collection);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return new CollectionPage(data, currentPage, totalPage);
	}

	@Override
	public List<Collection> searchCollectionByPhone(Long phone) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Collection> data = new ArrayList<Collection>();
		try {
			pst = conn
					.prepareStatement("select * from Collection where uphone=?");
			pst.setObject(1, phone);

			rs = pst.executeQuery();
			while (rs.next()) {
				int coid = rs.getInt("Coid");
				String jpgurl = rs.getString("jpgurl");
				String rname = rs.getString("rname");
				String info = rs.getString("info");
				Long rphone = rs.getLong("telephone");
				Long uphone = rs.getLong("uphone");
				String rsite=rs.getString("rsite");
				int rid=rs.getInt("rid");
				String sname=rs.getString("sname");
				Collection collection = new Collection(coid, jpgurl, rname,
						info, rphone, uphone,rsite,rid,sname);
				data.add(collection);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return data;
	}

	@Override
	public int searchCollectionCountByPhone(Long phone) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		int total = 0;
		ResultSet rs = null;
		try {
			pst = conn
					.prepareStatement("select count(*) from Collection where uphone=?");
			pst.setObject(1, phone);
			rs = pst.executeQuery();
			if (rs.next()) {
				total = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return total;
	}

	@Override
	public int searchContentCountByPhone(Long phone) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		int total = 0;
		ResultSet rs = null;
		try {
			pst = conn
					.prepareStatement("select count(*) from Content where uphone=?");
			pst.setObject(1, phone);
			rs = pst.executeQuery();
			if (rs.next()) {
				total = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return total;
	}

	@Override
	public List<Seat> getseatinfoByRid(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Seat> s = new ArrayList<Seat>();
		try {
			pst = conn.prepareStatement("select * from seat where Rid=?");
			pst.setObject(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				int seatid = rs.getInt("seatid");
				int rid = rs.getInt("Rid");
				int seatnum = rs.getInt("seatnum");
				String arrivetime = rs.getString("arrivetime");
				int booleanbj = rs.getInt("booleanbj");
				Long uphone = rs.getLong("uphone");
				Seat seat = new Seat(seatid, rid, seatnum, arrivetime,
						booleanbj, uphone);
				s.add(seat);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return s;
	}

	@Override
	public boolean deleteseatByseatId(int id) {
		Connection conn = JDBCUtil.getConn();
		String sql = "delete from seat where seatid=?";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, id);
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public Seat getseatinfoByseatid(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		Seat seat = null;
		try {
			pst = conn.prepareStatement("select * from seat where seatid=?");
			pst.setObject(1, id);
			rs = pst.executeQuery();
			if (rs.next()) {
				int seatid = rs.getInt("seatid");
				int rid = rs.getInt("Rid");
				int seatnum = rs.getInt("seatnum");
				String arrivetime = rs.getString("arrivetime");
				int booleanbj = rs.getInt("booleanbj");
				Long uphone = rs.getLong("uphone");
				seat = new Seat(seatid, rid, seatnum, arrivetime, booleanbj,
						uphone);
				;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return seat;
	}

	@Override
	public boolean deleteorderByoOid(int id) {
		Connection conn = JDBCUtil.getConn();
		String sql = "delete from Orderlistinfomation where Oid=?";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, id);
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public OrderList getorderByOid(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		OrderList order = null;
		try {
			pst = conn.prepareStatement("select * from Orderlistinfomation where Oid=?");
			pst.setObject(1, id);
			rs = pst.executeQuery();
			if (rs.next()) {
				int oid = rs.getInt("Oid");
				int rid = rs.getInt("Rid");
				String bname = rs.getString("Bname");
				String foodinfo = rs.getString("FoodInfo");
				Double total = rs.getDouble("Ototalprice");
				long ophone = rs.getLong("Ophone");
				String osite = rs.getString("Osite");
				Timestamp otime = rs.getTimestamp("Otime");
				int state = rs.getInt("state");
				String uphone=rs.getString("uphone");
			  order = new OrderList(oid, rid, bname, foodinfo,
						osite, otime, ophone, total, state,uphone);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return order;
	}

	@Override
	public RestaurantPage searchRestaurantPageByType(int currentPage,
			int pageSize, String type) {
		int total = getTotalRecord();
		// total = 51 pageSize = 10;
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Restaurant> data = new ArrayList<Restaurant>();
		try {
			pst = conn
					.prepareStatement("select * from Restaurant where type=? limit ?,?");
			pst.setObject(1, type);
			pst.setObject(2, (currentPage - 1) * pageSize);
			pst.setObject(3, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int rid = rs.getInt("Rid");
				String sname = rs.getString("Sname");
				String spwd = rs.getString("Spwd");
				String rname = rs.getString("Rname");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String stressname = rs.getString("stress");
				String rsite = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type1 = rs.getString("type");
				Restaurant s = new Restaurant(rid, sname, spwd, rname,
						telephone, email, city, stressname, rsite, info,
						jpgurl, type1);
				data.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return new RestaurantPage(data, currentPage, totalPage);
	}

	@Override
	public RestaurantPage searchRestaurantPageByStressAndType(int currentPage,
			int pageSize, String city, String type) {
		int total = getTotalRecord();
		// total = 51 pageSize = 10;
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Restaurant> data = new ArrayList<Restaurant>();
		try {
			pst = conn
					.prepareStatement("select * from Restaurant where type=? and city=? limit ?,?");
			pst.setObject(1, type);
			pst.setObject(2, city);
			pst.setObject(3, (currentPage - 1) * pageSize);
			pst.setObject(4, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int rid = rs.getInt("Rid");
				String sname = rs.getString("Sname");
				String spwd = rs.getString("Spwd");
				String rname = rs.getString("Rname");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city1 = rs.getString("city");
				String stressname = rs.getString("stress");
				String rsite = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type1 = rs.getString("type");
				Restaurant s = new Restaurant(rid, sname, spwd, rname,
						telephone, email, city1, stressname, rsite, info,
						jpgurl, type1);
				data.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return new RestaurantPage(data, currentPage, totalPage);
	}

	@Override
	public RestaurantPage searchRestaurantPageByCity(int currentPage,
			int pageSize, String city) {
		int total = getTotalRecord();
		// total = 51 pageSize = 10;
		int totalPage = total % pageSize == 0 ? total / pageSize : total
				/ pageSize + 1;
		if (currentPage > totalPage)
			return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Restaurant> data = new ArrayList<Restaurant>();
		try {
			pst = conn
					.prepareStatement("select * from Restaurant where city=? limit ?,?");
			pst.setObject(1, city);
			pst.setObject(2, (currentPage - 1) * pageSize);
			pst.setObject(3, pageSize);
			rs = pst.executeQuery();
			while (rs.next()) {
				int rid = rs.getInt("Rid");
				String sname = rs.getString("Sname");
				String spwd = rs.getString("Spwd");
				String rname = rs.getString("Rname");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city1 = rs.getString("city");
				String stressname = rs.getString("stress");
				String rsite = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String type = rs.getString("type");
				Restaurant s = new Restaurant(rid, sname, spwd, rname,
						telephone, email, city1, stressname, rsite, info,
						jpgurl, type);
				data.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return new RestaurantPage(data, currentPage, totalPage);
	}

	@Override
	public boolean addUserHeadPhoto(Long phone, String path) {
		Connection conn = JDBCUtil.getConn();
		String sql = "insert into UserHead(uphone,path) values(?,?)";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, phone);
			pst.setObject(2, path);
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}

	@Override
	public UserHeadPhoto seachUserHeadPhoto(long phone) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		UserHeadPhoto uhp = null;
		try {
			pst = conn
					.prepareStatement("select * from UserHead where uphone=?");
			pst.setObject(1, phone);
			rs = pst.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("UHid");
				Long userphone = rs.getLong("uphone");
				String photopath = rs.getString("path");
				uhp = new UserHeadPhoto(userphone, photopath);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return uhp;
	}

	@Override
	public int searchResturantCount() {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		int total = 0;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("select count(*) from Restaurant");
			rs = pst.executeQuery();
			if (rs.next()) {
				total = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return total;
	}

	@Override
	public List<Restaurant> serchRestaurantsById(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = "select * from Restaurant where Rid = ?";
		List<Restaurant> data = new ArrayList<Restaurant>();
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				int rid = rs.getInt("Rid");
				String sellname = rs.getString("Sname");
				String sellpwd = rs.getString("Spwd");
				long telephone = rs.getLong("telephone");
				String email = rs.getString("email");
				String city = rs.getString("city");
				String rname = rs.getString("rname");
				String address = rs.getString("Rsite");
				String info = rs.getString("info");
				String jpgurl = rs.getString("jpgurl");
				String stress = rs.getString("stress");
				String type = rs.getString("type");
				Restaurant rt = new Restaurant(rid, sellname, sellpwd, rname,
						telephone, email, city, stress, address, info, jpgurl,
						type);
				data.add(rt);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return data;
	}

	@Override
	public boolean addSeat(Seat s) {
		Connection conn = JDBCUtil.getConn();
		String sql = "insert into seat(Rid,seatnum,arrivetime,booleanbj,uphone) values(?,?,?,?,?)";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			pst.setObject(1, s.getRid());
			pst.setObject(2, s.getSeatnum());
			pst.setObject(3, s.getArrivetime());
			pst.setObject(4, s.getBooleanbj());
			pst.setObject(5, s.getUphoneLong());
			int count = pst.executeUpdate();
			if (count >= 1)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, null);
		}
		return false;
	}
	
	@Override
	public SearchCollectionPage searchCollectionByPage(long phone, int currentPage,
			int pageSize) {
		int total = searchCollectionCountByPhone(phone);
		int totalPage = total % pageSize == 0?  total / pageSize :  total / pageSize + 1;
		if(currentPage > totalPage) return null;
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Collection> data  = new ArrayList<Collection>();
		try {
			pst = conn.prepareStatement("select * from Collection where uphone=?  limit ?,?;");
			pst.setObject(1, phone);
			pst.setObject(2, (currentPage - 1) * pageSize);
			pst.setObject(3, pageSize);
			rs = pst.executeQuery();
			while(rs.next()){
				int coid = rs.getInt("Coid");
				String jpgurl = rs.getString("jpgurl");
				String rname=rs.getString("rname");
				String info = rs.getString("info");
				long telephone = rs.getLong("telephone");
				long userphone = rs.getLong("uphone");
				String rsite=rs.getString("rsite");
				int rid=rs.getInt("rid");
				String sname=rs.getString("sname");
				Collection collection=new Collection(coid,jpgurl,rname,info,telephone,userphone,rsite,rid,sname);
				data.add(collection);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			JDBCUtil.close(conn, pst, rs);
		}
		return new SearchCollectionPage(data,phone, currentPage, totalPage);
	}

	@Override
	public OrderList searchOrderListByOid(int id) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		OrderList o = null;
		try {
			pst = conn
					.prepareStatement("select * from Orderlistinfomation where oid=?");
			pst.setObject(1, id);

			rs = pst.executeQuery();
			if (rs.next()) {
				int oid = rs.getInt("Oid");
				int rid = rs.getInt("Rid");
				String bname = rs.getString("Bname");
				String foodinfo = rs.getString("FoodInfo");
				Double total = rs.getDouble("Ototalprice");
				long ophone = rs.getLong("Ophone");
				String osite = rs.getString("Osite");
				Timestamp otime = rs.getTimestamp("Otime");
				int state = rs.getInt("state");
				String uphone=rs.getString("uphone");
				o = new OrderList(oid, rid, bname, foodinfo,
						osite, otime, ophone, total, state,uphone);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}

		return o;
	}

	@Override
	public boolean updateorder(OrderList o) {
		Connection conn = JDBCUtil.getConn();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn
					.prepareStatement("update orderlistinfomation set state=? where oid=?");
			pst.setObject(1, o.getState());
			pst.setObject(2, o.getOid());
			int count = pst.executeUpdate();
			if (count >= 1) {
				return true;
			} else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JDBCUtil.close(conn, pst, rs);
		}
		return false;
	}
}
