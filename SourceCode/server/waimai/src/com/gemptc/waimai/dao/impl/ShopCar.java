package com.gemptc.waimai.dao.impl;

import java.util.ArrayList;

import com.gemptc.waimai.entity.Shoppingcar;

public class ShopCar{
	  //用来存储购买的食物
	   private ArrayList buylist = new ArrayList();
	   
	   public void setBuylist(ArrayList buylist){
		   this.buylist = buylist;
	   }
	   
	 public ArrayList getBuylist() {
		return buylist;
	 }

	//向购物车中添加食物
	public boolean addFood(Shoppingcar food) {
		 if(food != null){
			   //购物车中没有食物
			   if(buylist.size()==0){
				 Shoppingcar f = new Shoppingcar();
				 f.setFname(food.getFname());
				 f.setFprice(food.getFprice());
				 f.setNumber(food.getNumber());
				 buylist.add(f);
				 return true;
			    }else{    //购物车中有食物
				  //遍历购物车，查看是否有已经存在当前要添加的食物
				   int i=0;
				   for(;i<buylist.size();i++){
					  //获得buylist集合中的元素
					   Shoppingcar f = (Shoppingcar)buylist.get(i);
					  
					  //判断buylist集合中获得当前食物名称是否与要添加的食物的名称相同
					  //如果相同，说明已经已经有了该食物，只需要将食物的数量加1
					  if(f.getFname().equals(food.getFname())){
						  f.setNumber(f.getNumber()+1);
						  return true;
					  }
				  }
				  if(i>=buylist.size()){//说明buylist中不存在要添加的商品
					  Shoppingcar f = new Shoppingcar();
					  f.setFname(food.getFname());
					  f.setFprice(food.getFprice());
					  f.setNumber(food.getNumber());
					  buylist.add(f);//存储商品到buylist集合中
					  return true;
				  }
			   }
		   }
		 return false;
	}

	//从购物车中移除指定编号的食物
	public boolean removeFood(int fid) {
		//遍历购物车，找到指定编号的食物
		   for(int i = 0;i<buylist.size();i++){
			   Shoppingcar f = (Shoppingcar)buylist.get(i);
			   if(f.getFid()==fid){
				   if(f.getNumber()>1){//如果该食物的购买数量大于1
					 f.setNumber(f.getNumber()-1);
				      return true;
				   }else{
					   buylist.remove(i);
						return true;
				   }
			   }
		   }
		   return false;
	}
	//清空购物车
	public boolean clearFood(){
		if(buylist.size() !=0 ){
			buylist.clear();
		    return true;
		}else{
			return false;
		}
	}
}
