package com.gemptc.waimai.service;

import java.util.List;

import com.gemptc.waimai.entity.Collect;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.entity.OrderList;

public interface SellerService {
	  public boolean addCinfo(Content c);
	  public List<Content> getContentByRid(int rid);
	  public boolean addCollect(Collection cc);
	  public boolean deleteCollect(long usephone,int rid);
	  public boolean addOrderList(OrderList or);
	  public String selectcollection(int rid,long usephone);
}
