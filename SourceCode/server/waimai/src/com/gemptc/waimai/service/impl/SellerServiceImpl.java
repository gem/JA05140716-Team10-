package com.gemptc.waimai.service.impl;

import java.util.List;

import com.gemptc.waimai.dao.SellerDao;
import com.gemptc.waimai.entity.Collect;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.dao.impl.SellerDaoImplMysql;
import com.gemptc.waimai.service.SellerService;

public class SellerServiceImpl implements SellerService {
	 private SellerDao dao;
	 
	 
	public SellerServiceImpl(SellerDao dao) {
		super();
		this.dao = dao;
	}


	@Override
	public boolean addCinfo(Content c) {
		return dao.addCinfo(c);
	}


	@Override
	public List<Content> getContentByRid(int rid) {
		return dao.getContentByRid(rid);
	}


	@Override
	public boolean addCollect(Collection cc) {
		return dao.addCollect(cc);
	}


	@Override
	public boolean deleteCollect(long usephone, int rid) {
				return dao.deleteCollect(usephone, rid);
	}


	@Override
	public boolean addOrderList(OrderList or) {
		return dao.addOrderList(or);
	}


	@Override
	public String selectcollection(int rid, long usephone) {
		return dao.selectcollection(rid, usephone);
	}


}
