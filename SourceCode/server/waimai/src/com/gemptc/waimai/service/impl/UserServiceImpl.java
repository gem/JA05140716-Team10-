package com.gemptc.waimai.service.impl;

import java.util.List;

import com.gemptc.waimai.dao.UserDao;
import com.gemptc.waimai.entity.Buyer;
import com.gemptc.waimai.entity.Collection;
import com.gemptc.waimai.entity.CollectionPage;
import com.gemptc.waimai.entity.Content;
import com.gemptc.waimai.entity.Food;
import com.gemptc.waimai.entity.OrderList;
import com.gemptc.waimai.entity.Restaurant;
import com.gemptc.waimai.entity.RestaurantPage;
import com.gemptc.waimai.entity.SearchCollectionPage;
import com.gemptc.waimai.entity.Seat;
import com.gemptc.waimai.entity.UserHeadPhoto;
import com.gemptc.waimai.entity.UserPeople;
import com.gemptc.waimai.service.UserService;

public class UserServiceImpl implements UserService {
	 private UserDao dao;
	 
	public UserServiceImpl(UserDao dao) {
		super();
		this.dao = dao;
	}

	@Override
	public RestaurantPage searchRestaurantByPage(int currentPage, int pageSize) {
		return dao.searchRestaurantByPage(currentPage, pageSize);
	}

	@Override
	public Buyer getBuyerByNameAndPassword(String name, String password) {
		// TODO Auto-generated method stub
		return dao.getBuyerByNameAndPassword(name, password);
	}

	@Override
	public boolean addFood(Food f) {
		// TODO Auto-generated method stub
		return dao.addFood(f);
	}

	@Override
	public boolean addRestaurant(Restaurant R) {
		// TODO Auto-generated method stub
		return dao.addRestaurant(R);
	}

	@Override
	public Restaurant getRestaurantIdByName(String name) {
		// TODO Auto-generated method stub
		return dao.getRestaurantIdByName(name);
	}

	@Override
	public boolean createTableFoofListByNmae(String name) {
		// TODO Auto-generated method stub
		return dao.createTableFoofListByNmae(name);
	}

	@Override
	public Restaurant getSellererByNameAndPassword(String name, String password) {
		// TODO Auto-generated method stub
		return dao.getSellererByNameAndPassword(name, password);
	}

	@Override
	public	List<Food> searchFoodListByRid(int id) {
		// TODO Auto-generated method stub
		return dao.searchFoodListByRid(id);
	}

	@Override
	public boolean deleteFoodById(int id) {
		// TODO Auto-generated method stub
		return dao.deleteFoodById(id);
	}

	@Override
	public Food searchRidByFid(int id) {
		// TODO Auto-generated method stub
		return dao.searchRidByFid(id);
	}

	@Override
	public boolean updateFood(Food f) {
		// TODO Auto-generated method stub
		return dao.updateFood(f);
	}

	@Override
	public List<Food> searchFoodByFname(String name) {
		// TODO Auto-generated method stub
		return dao.searchFoodByFname(name);
	}

	@Override
	public boolean addorderlist(OrderList o) {
		// TODO Auto-generated method stub
		return dao.addorderlist(o);
	}

	@Override
	public Restaurant getRestaurantById(int rid) {
		// TODO Auto-generated method stub
		return dao.getRestaurantById(rid);
	}

	@Override
	public List<Content> getContentByRid(int rid) {
		// TODO Auto-generated method stub
		return dao.getContentByRid(rid);
	}

	@Override
	public List<OrderList> searchOrderListByRid(int id) {
		// TODO Auto-generated method stub
		return dao.searchOrderListByRid(id);
	}

	@Override
	public UserPeople getUserByNameAndPassword(long phone, String password) {
		// TODO Auto-generated method stub
		return dao.getUserByNameAndPassword(phone, password);
	}

	@Override
	public UserPeople getUserPwd(long phone, int pid, String answer) {
		// TODO Auto-generated method stub
		return dao.getUserPwd(phone, pid, answer);
	}

	@Override
	public boolean addUserPeople(UserPeople up) {
		// TODO Auto-generated method stub
		return dao.addUserPeople(up);
	}

	@Override
	public UserPeople isRegister(long phone) {
		// TODO Auto-generated method stub
		return dao.isRegister(phone);
	}

	@Override
	public List<Restaurant> getRestaurantsByStress(String stress) {
		// TODO Auto-generated method stub
		return dao.getRestaurantsByStress(stress);
	}

	@Override
	public RestaurantPage searchRestaurantPageByStress(int currentPage,
			int pageSize, String stress) {
		// TODO Auto-generated method stub
		return dao.searchRestaurantPageByStress(currentPage, pageSize, stress);
	}

	@Override
	public List<OrderList> getOrderlistByPhone(int currentPage, int pageSize,
			String uphone) {
		// TODO Auto-generated method stub
		return dao.getOrderlistByPhone(currentPage, pageSize, uphone);
	}

	@Override
	public List<OrderList> getOrderlistByPhoneAndstate(int currentPage,
			int pageSize, String uphone, int state) {
		// TODO Auto-generated method stub
		return dao.getOrderlistByPhoneAndstate(currentPage, pageSize, uphone, state);
	}
	
	@Override
	public boolean updatePwd(UserPeople up) {
		// TODO Auto-generated method stub
		return dao.updatePwd(up);
	}

	@Override
	public boolean updateName(UserPeople up) {
		// TODO Auto-generated method stub
		return dao.updateName(up);
	}

	@Override
	public boolean addAdvice(long phone, String advice) {
		// TODO Auto-generated method stub
		return dao.addAdvice(phone, advice);
	}

	@Override
	public CollectionPage searchCollectionByPage(int currentPage, int pageSize) {
		// TODO Auto-generated method stub
		return dao.searchCollectionByPage(currentPage, pageSize);
	}

	@Override
	public List<Collection> searchCollectionByPhone(Long phone) {
		// TODO Auto-generated method stub
		return dao.searchCollectionByPhone(phone);
	}

	@Override
	public int searchCollectionCountByPhone(Long phone) {
		// TODO Auto-generated method stub
		return dao.searchCollectionCountByPhone(phone);
	}

	@Override
	public int searchContentCountByPhone(Long phone) {
		// TODO Auto-generated method stub
		return dao.searchContentCountByPhone(phone);
	}

	@Override
	public List<Seat> getseatinfoByRid(int id) {
		// TODO Auto-generated method stub
		return dao.getseatinfoByRid(id);
	}

	@Override
	public boolean deleteseatByseatId(int id) {
		// TODO Auto-generated method stub
		return dao.deleteFoodById(id);
	}

	@Override
	public Seat getseatinfoByseatid(int id) {
		// TODO Auto-generated method stub
		return dao.getseatinfoByseatid(id);
	}

	@Override
	public boolean deleteorderByoOid(int id) {
		// TODO Auto-generated method stub
		return dao.deleteorderByoOid(id);
	}

	@Override
	public OrderList getorderByOid(int id) {
		// TODO Auto-generated method stub
		return dao.getorderByOid(id);
	}

	@Override
	public RestaurantPage searchRestaurantPageByType(int currentPage,
			int pageSize, String type) {
		// TODO Auto-generated method stub
		return dao.searchRestaurantPageByType(currentPage, pageSize, type);
	}

	@Override
	public RestaurantPage searchRestaurantPageByStressAndType(int currentPage,
			int pageSize, String city, String type) {
		// TODO Auto-generated method stub
		return dao.searchRestaurantPageByStressAndType(currentPage, pageSize, city, type);
	}

	@Override
	public RestaurantPage searchRestaurantPageByCity(int currentPage,
			int pageSize, String city) {
		// TODO Auto-generated method stub
		return dao.searchRestaurantPageByCity(currentPage, pageSize, city);
	}
	
	@Override
	public boolean addUserHeadPhoto(Long phone, String path) {
		// TODO Auto-generated method stub
		return dao.addUserHeadPhoto(phone, path);
	}

	@Override
	public UserHeadPhoto seachUserHeadPhoto(long phone) {
		// TODO Auto-generated method stub
		return dao.seachUserHeadPhoto(phone);
	}

	@Override
	public int searchResturantCount() {
		// TODO Auto-generated method stub
		return dao.searchResturantCount();
	}

	@Override
	public List<Restaurant> serchRestaurantsById(int id) {
		// TODO Auto-generated method stub
		return dao.serchRestaurantsById(id);
	}

	@Override
	public boolean addSeat(Seat s) {
		// TODO Auto-generated method stub
		return dao.addSeat(s);
	}

	@Override
	public SearchCollectionPage searchCollectionByPage(long phone,
			int currentPage, int pageSize) {
		// TODO Auto-generated method stub
		return dao.searchCollectionByPage(phone, currentPage, pageSize);
	}

	@Override
	public OrderList searchOrderListByOid(int id) {
		// TODO Auto-generated method stub
		return dao.searchOrderListByOid(id) ;
	}

	@Override
	public boolean updateorder(OrderList o) {
		// TODO Auto-generated method stub
		return dao.updateorder(o);
	}

	
	

}
